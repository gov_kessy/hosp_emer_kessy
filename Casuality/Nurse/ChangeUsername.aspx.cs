﻿//Change username
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Nurse_ChangeUsername : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            lbl_title.Text = "Change UserName - " + Session["id"];
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        }
    }
    protected void btn_usr_Click(object sender, EventArgs e)
    {
        c.getCon();

        string name = Session["id"].ToString();

        SqlCommand cmd111 = new SqlCommand("select * from UserTable where username= '" + txt_usr.Text + "' ", c.Con);
        SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
        DataTable dt111 = new DataTable();
        sda111.Fill(dt111);

        int k = cmd111.ExecuteNonQuery();

        if (dt111.Rows.Count > 0)
        {
            Response.Write("<script>alert('Username Already Exists');</script>");
        }
        else
        {
            SqlCommand cmds = new SqlCommand("select * from UserTable where username= '" + name + "' and password='" + txt_pass.Text + "'", c.Con);
            SqlDataAdapter sdas = new SqlDataAdapter(cmds);
            DataTable dts = new DataTable();
            sdas.Fill(dts);

            int ks = cmds.ExecuteNonQuery();

            if (dts.Rows.Count > 0)
            {
                String s = "update UserTable set username='" + txt_usr.Text + "' where username='" + name + "'";
                SqlCommand cmd = new SqlCommand(s, c.Con);
                cmd.ExecuteNonQuery();
                Response.Write("<script>alert('Username Changed Successfully');</script>");

                SqlCommand cmd11 = new SqlCommand("select * from UserTable where username= '" + txt_usr.Text + "' ", c.Con);
                SqlDataAdapter sda11 = new SqlDataAdapter(cmd11);
                DataTable dt11 = new DataTable();
                sda11.Fill(dt11);

                int k1 = cmd11.ExecuteNonQuery();

                if (dt11.Rows.Count > 0)
                {
                    DataRow row11 = dt11.Rows[dt11.Rows.Count - 1];
                    string user = Convert.ToString(row11[1]);
                    lbl_title.Text = "New Username - " + user;
                    txt_usr.Text = " ";
                    Session["id"] = user;
                }
            }
            else
                Response.Write("<script>alert('Incorrect Password');</script>");
        }
        txt_pass.Text = " ";
        txt_usr.Text = " ";

        c.Con.Close();
        Response.Redirect("Homepageheadnurse.aspx");


    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepagenurse.aspx");
    }
}