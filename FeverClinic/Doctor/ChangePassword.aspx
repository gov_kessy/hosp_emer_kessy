﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Doctor/FeverClinicDocMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="FeverClinic_Doctor_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lbl_title" runat="server" Font-Bold="False"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Enter Old Password:</td>
                <td>
                    <asp:TextBox ID="txt_old" CssClass="twitter" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:Label ID="lbl_old" runat="server" style="font-size: medium"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_old" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Enter New Password:</td>
                <td>
                    <asp:TextBox ID="txt_new" runat="server" CssClass="twitter" TextMode="Password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="#CC0000" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&amp;])[A-Za-z\d$@$!%*#?&amp;]{8,}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Retype New Password:</td>
                <td>
                    <asp:TextBox ID="txt_retype" runat="server" CssClass="twitter" TextMode="Password"></asp:TextBox>
                    <asp:Label ID="lbl_retype" runat="server" style="font-size: medium"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_chg" runat="server" OnClick="btn_chg_Click" Text="Change Password" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click" Text="Cancel" CssClass="btn" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
        </table>
    <div>
    
    </div>
    </form>

</asp:Content>

