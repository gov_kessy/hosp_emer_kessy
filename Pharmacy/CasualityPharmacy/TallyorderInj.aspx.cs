﻿//tally injection orders
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Pharmacy_CasualityPharmacy_TallyorderInj : System.Web.UI.Page
{
        conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLabels();
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
    }
     public void BindLabels()
    {
        c.getCon();
        int orderno = Convert.ToInt32(Request.QueryString["OrderNo"].ToString());
        lbl_orderid.Text = orderno.ToString();
        SqlCommand cmd = new SqlCommand("select m.injectionname,o.orderamt,o.dateoforder,m.injectionid from CasualityInjectionStock s inner join InjectionMaster m on s.injecid=m.injectionid inner join CasualityInjectionOrder o on o.injectionid=s.injecid where o.orderid=@ono and status=@stat order by orderid", c.Con);
        cmd.Parameters.AddWithValue("@ono", orderno);
        cmd.Parameters.AddWithValue("@stat", "Pending");
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_injid.Text = dt.Rows[0][3].ToString();
            lbl_medname.Text = dt.Rows[0][0].ToString();
            lbl_orderamt.Text = dt.Rows[0][1].ToString();
            lbl_orderdate.Text = dt.Rows[0][2].ToString();
        }
        c.Con.Close();
    }
    protected void btn_tally_Click(object sender, EventArgs e)
    {
        c.getCon();
        int orderno = Convert.ToInt32(Request.QueryString["OrderNo"].ToString());
        string insert = "insert into BatchCasuality values('"+txtDate.Text+"')Select @@Identity";
            SqlCommand cmd = new SqlCommand(insert, c.Con);
           Object ob= cmd.ExecuteScalar();
        int batchid=Convert.ToInt32(ob);
          
       
            string ins = "insert into CasualityInjectionStock values('" + Convert.ToInt32(lbl_injid.Text) + "','" +  Convert.ToInt32(txt_amtrec.Text) + "','"+batchid+"')";
            SqlCommand cmdup = new SqlCommand(ins, c.Con);
            cmdup.ExecuteNonQuery();
            string upd = "update CasualityInjectionOrder set status='Fullfilled',dateoffulfil='" + DateTime.Today + "' where orderid='" + orderno + "'";
            SqlCommand cmds = new SqlCommand(upd, c.Con);
            cmds.ExecuteNonQuery();
            
            c.Con.Close();
            Response.Redirect("UpdateStockInj.aspx");
        
    }
}


 
        