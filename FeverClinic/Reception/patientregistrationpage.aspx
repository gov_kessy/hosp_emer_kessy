﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Reception/FeverClinicReceptionMaster.master" AutoEventWireup="true" CodeFile="patientregistrationpage.aspx.cs" Inherits="FeverClinic_Reception_patientregistrationpage" %>

 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
  
            <link href="style.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <center>
        <table>
            <tr>
                <td ><ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
                <td class="auto-style12" >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Patient Name</td>
                <td>
                    <asp:TextBox ID="txt_pname" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_pname" ErrorMessage="*name can contain only letters and white spaces" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Contact Number</td>
                <td>
                    <asp:TextBox ID="txt_pcon" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_pcon" ErrorMessage="*Invalid Phone Number-please enter 10 digit number" ForeColor="#CC0000" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">Address Line 1</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_add1" runat="server" CssClass="twitter"></asp:TextBox>
           
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txt_add1" ErrorMessage="*Invalid Address Line" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">Address Line 2</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_add2" runat="server" CssClass="twitter"></asp:TextBox>
           
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_add2" ErrorMessage="*Invalid Address Line" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Place</td>
                <td>
                    <asp:TextBox ID="txt_place" runat="server" CssClass="twitter"></asp:TextBox>
           
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txt_place" ErrorMessage="*Invalid Place" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Pin</td>
                <td>
                    <asp:TextBox ID="txt_pin" runat="server" CssClass="twitter"></asp:TextBox>
            
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txt_pin" ErrorMessage="*Invalid Pin" ForeColor="#CC0000" ValidationExpression="\d{6}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
           <tr>
                <td class="auto-style3">Date of Birth ( mm/dd/yyyy)</td>
                <td>
                    <asp:TextBox ID="txt_dob" runat="server" CssClass="twitter"></asp:TextBox><ajax:calendarextender ID="CalendarExtender1" TargetControlID="txt_dob" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_dob" ErrorMessage="*Enter Date of Birth" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               
                </td>
            </tr>
           <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Guardian Name</td>
                <td>
                                        <asp:TextBox ID="txt_gname" runat="server" CssClass="twitter"></asp:TextBox>

                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txt_gname" ErrorMessage="*name can contain only letters and white spaces" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                                        &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Relation of Guardian</td>
                <td>
                    <asp:DropDownList ID="ddl_rel" runat="server" ForeColor="Gray" CssClass="twitter">
                        <asp:ListItem>[Select]</asp:ListItem>
                        <asp:ListItem>Father</asp:ListItem>
                        <asp:ListItem>Mother</asp:ListItem>
                        <asp:ListItem>Son</asp:ListItem>
                        <asp:ListItem>Daughter</asp:ListItem>
                        <asp:ListItem>Husband</asp:ListItem>
                        <asp:ListItem>Wife</asp:ListItem>
                    </asp:DropDownList>
           
                    <asp:Label ID="lbl_rel" runat="server" ForeColor="#CC0000" Text="*Select Relation"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Guardian Contact Number</td>
                <td>
                                        <asp:TextBox ID="txt_gcon" runat="server" CssClass="twitter"></asp:TextBox>

                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txt_gcon" ErrorMessage="*Invalid Phone Number" ForeColor="#CC0000" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                                        &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Blood Group</td>
                <td>
                    <asp:DropDownList ID="ddl_blood" runat="server" ForeColor="Gray" CssClass="twitter" >
                    </asp:DropDownList>
           
                    <asp:Label ID="lbl_blood" runat="server" ForeColor="#CC0000" Text="*Select Blood Group"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style5">
                    Gender</td>
                <td class="auto-style6">
                    <asp:RadioButtonList ID="rbl_gen" runat="server" CssClass="twitter" >
                        <asp:ListItem Selected="True">Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style6">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_reg" runat="server" Text="Register!" OnClick="btn_reg_Click1"  CssClass="btn" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    <div>
    
    </div></center>
    </form>
</asp:Content>
