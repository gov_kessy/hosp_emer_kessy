﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Superintendent_loginpage : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        lbl_incorrect.Visible = false;

    }
    protected void btn_sub_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from UserTable where username='" + txt_user.Text + "' and password='" + txt_pswd.Text + "'", c.Con);
        // cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();

        if (dt.Rows.Count > 0)
        {
            DataRow row1 = dt.Rows[dt.Rows.Count - 1];
            string log = "insert into LogTable values('" + txt_user.Text + "','" + DateTime.Now + "')";
            SqlCommand cmds = new SqlCommand(log, c.Con);
            cmds.ExecuteNonQuery();
            Session["id"] = row1[1];
            Session["docid"] = row1[3];
            Session["uid"] = row1[3];
            int type = Convert.ToInt32(row1["emptypeid"]);
            string mod = Convert.ToString(row1["module"]);

            if (String.Compare(mod, "Superintendent") == 0)
                Response.Redirect("homepageSuper.aspx");
           
            txt_pswd.Text = " ";
            txt_user.Text = " ";
        }
        else
        {
            lbl_incorrect.Visible = true;
            lbl_incorrect.Text = "You're username or password is incorrect";
            lbl_incorrect.ForeColor = System.Drawing.Color.Red;

        }

    }

}
