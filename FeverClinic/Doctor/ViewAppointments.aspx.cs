﻿//View todays appointments
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class FeverClinic_Doctor_ViewAppointments : System.Web.UI.Page
{
    conclass c = new conclass();
    int deptid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/FeverClinic/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getCon();
            lbl_cas.Visible = false;
            string user = Session["id"].ToString();
            SqlCommand cmd_usr = new SqlCommand("select * from UserTable where username= '" + user + "' ", c.Con);
            SqlDataAdapter sda_usr = new SqlDataAdapter(cmd_usr);
            DataTable dt_usr = new DataTable();
            sda_usr.Fill(dt_usr);
            int empid;
            int k = cmd_usr.ExecuteNonQuery();

            if (dt_usr.Rows.Count > 0)
            {
                DataRow row_usr = dt_usr.Rows[dt_usr.Rows.Count - 1];

                empid = Convert.ToInt32(row_usr[3]);
                Session["docid"] = empid;

                SqlCommand cmd_dept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%'", c.Con);
                SqlDataAdapter sda_dept = new SqlDataAdapter(cmd_dept);
                DataTable dt_dept = new DataTable();
                sda_dept.Fill(dt_dept);

                int d = cmd_dept.ExecuteNonQuery();

                if (dt_dept.Rows.Count > 0)
                {
                    DataRow row_dept = dt_dept.Rows[dt_dept.Rows.Count - 1];

                    deptid = Convert.ToInt32(row_dept[0]);
                    BindGridViewappoint();

                }

            }
        }
    }
    protected void gridview_pat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_pat.Rows[index];
            Response.Redirect("PatientConsult.aspx?PatNo=" + row.Cells[0].Text);
        }
    }

    private void BindGridViewappoint()
    {
        SqlCommand cmd = new SqlCommand("select * from patientdetails p inner Join Schedule s on p.patientid=s.patientid where (deptid=@dptno and scheduledate=@date and status=@status and empid=@empid) or  (deptid=@dptno and scheduledate=@date and status=@status and empid=@eid) order by s.scheduleid", c.Con);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@dptno", deptid);
        cmd.Parameters.AddWithValue("@status", "Confirmed");
        cmd.Parameters.AddWithValue("@empid", Session["docid"]);
        cmd.Parameters.AddWithValue("@eid", "0");
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
        }
        else
            lbl_cas.Visible = true;

    }
}