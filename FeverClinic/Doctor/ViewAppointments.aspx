﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Doctor/FeverClinicDocMaster.master" AutoEventWireup="true" CodeFile="ViewAppointments.aspx.cs" Inherits="FeverClinic_Doctor_ViewAppointments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 136px;
        }
        .auto-style3 {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server"><center>
        <table class="auto-style1">
            <tr>
                <td class="auto-style3"></td>
            </tr>
            <tr>
                <td>
                                                <asp:Label ID="lbl_cas" runat="server" Font-Size="Large" ForeColor="#FF99FF" Text="No Appointments"></asp:Label>
                                            </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:GridView ID="gridview_pat" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" runat="server" AutoGenerateColumns="False" OnRowCommand="gridview_pat_RowCommand" Width="498px" CellPadding="3" CssClass="auto-style2" DataKeyNames="patientid" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" HorizontalAlign="Center"  >  
                    <Columns>  
                        <asp:BoundField DataField="patientid" HeaderText="Patient Id" />  
                        <asp:BoundField DataField="pname" HeaderText="Patient Name" />  
                        <asp:BoundField DataField="scheduletime" HeaderText="Token Generated Time" />  
                     <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >

                        
                             
<ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>

                        
                             
                    </Columns>  
                       <FooterStyle BackColor="White" ForeColor="#000066" />

<HeaderStyle BackColor="#006699" ForeColor="White" Font-Bold="True"></HeaderStyle>
                       <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                       <RowStyle ForeColor="#000066" />
                       <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                       <SortedAscendingCellStyle BackColor="#F1F1F1" />
                       <SortedAscendingHeaderStyle BackColor="#007DBB" />
                       <SortedDescendingCellStyle BackColor="#CAC9C9" />
                       <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table></center>
    </form>
</asp:Content>

