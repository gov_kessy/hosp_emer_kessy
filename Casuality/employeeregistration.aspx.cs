﻿//Casuality employee registration
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
public partial class employeeregistration : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_desig.Visible = false;
            lbl_type.Visible = false;
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
             c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmpTypeMaster ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_emptype.DataSource = dt;
            ddl_emptype.DataValueField = "emptypeid";
            ddl_emptype.DataTextField = "emptypename";
            ddl_emptype.DataBind();
            ddl_emptype.Items.Insert(0, "[SELECT]");

        }
            //SqlCommand cmd2 = new SqlCommand("select * from Department ", c.Con);
                //SqlDataAdapter sda2 = new SqlDataAdapter(cmd2);
                //DataTable dt2 = new DataTable();
                //sda2.Fill(dt2);

                //int i2 = cmd2.ExecuteNonQuery();
                //int j2 = dt2.Rows.Count;

                //if (j2 > 0)
                //{
                //    ddl_dept.DataSource = dt2;
                //    ddl_dept.DataValueField = "deptid";
                //    ddl_dept.DataTextField = "deptname";
                //    ddl_dept.DataBind();
                //    ddl_dept.Items.Insert(0, "[SELECT]");

                //}
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        c.getCon();
        String image = "";
        String path = " ";
        if (FileUpload1.HasFile)
        {
            image = FileUpload1.FileName;
            if (Directory.Exists(Server.MapPath("~/OPD/image")))
                FileUpload1.SaveAs(Server.MapPath("~/OPD/image/" + image));
            else
            {
                Directory.CreateDirectory(Server.MapPath("~/OPD/image"));
                FileUpload1.SaveAs(Server.MapPath("~/OPD/image/" + image));
            }
            path = "~/OPD/image/" + image;
        }

        String stat = "Pending";
         if (ddl_emptype.SelectedIndex != 0)
        {
            if (ddl_desig.SelectedIndex != 0)
            {
                //if (ddl_dept.SelectedIndex != 0)
                //{
                SqlCommand cmd2 = new SqlCommand("select deptid from Department where deptname like '%Casuality%' and emptype='" + ddl_emptype.SelectedItem.Value + "' ", c.Con);
                SqlDataAdapter sda2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                sda2.Fill(dt2);

                int i2 = cmd2.ExecuteNonQuery();
                int j2 = dt2.Rows.Count;

                if (j2 > 0)
                {
        String s = "insert into CasualityEmployeeDetails values('" + ddl_emptype.SelectedItem.Value + "','" + txt_name.Text + "','" + txt_dob.Text + "','" + rbl_gen.SelectedItem.Text + "','" + DateTime.Today + "','" + txt_padd1.Text + "','" + txt_padd2.Text + "','" + txt_pplace.Text + "','" + txt_ppin.Text + "','" + txt_cadd1.Text + "','" + txt_cadd2.Text + "','" + txt_cplace.Text + "','" + txt_cpin.Text + "','" + txt_contact1.Text + "','" + txt_contact2.Text + "','" + dt2.Rows[0][0] + "','" + ddl_desig.SelectedItem.Value + "','" + txt_email.Text + "','" + stat + "','" + path + "')";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();
        Response.Write("<script>alert('Employee Registration successful');</script>");
        Response.Redirect("loginpage.aspx");
                }
                //else
                //{
                //    lbl_dept.Visible = true;
                //}
            }
            else
            {
                lbl_desig.Visible = true;
            }
        }
         else
         {
             lbl_type.Visible = true;
         }
        c.Con.Close();

        txt_name.Text = " ";
        txt_dob.Text = " ";
        txt_email.Text = " ";
        txt_contact1.Text = " ";
        txt_contact2.Text = " ";
        txt_cadd1.Text = " ";
        txt_cadd2.Text = " ";
        txt_cplace.Text = " ";
        txt_cpin.Text = " ";
        txt_padd1.Text = " ";
        txt_padd2.Text = " ";
        txt_pplace.Text = " ";
        txt_ppin.Text = " ";
        txt_cadd1.Text = " ";
        cbl_perm.ClearSelection();
       // ddl_dept.SelectedIndex = 0;
        ddl_desig.SelectedIndex = 0;
        ddl_emptype.SelectedIndex = 0;

    }
       
    protected void cbl_perm_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbl_perm.SelectedItem.Text == "[Check if same as current address]")
        {
            txt_padd1.Text = txt_cadd1.Text;
            txt_padd2.Text = txt_cadd2.Text;
            txt_pplace.Text = txt_cplace.Text;
            txt_ppin.Text = txt_cpin.Text;
        }


    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("loginpage.aspx");
    }
    protected void ddl_emptype_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd1 = new SqlCommand("select * from Designation d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like '" + ddl_emptype.SelectedItem.Text + "' ", c.Con);
        SqlDataAdapter sda1 = new SqlDataAdapter(cmd1);
        DataTable dt1 = new DataTable();
        sda1.Fill(dt1);

        int i1 = cmd1.ExecuteNonQuery();
        int j1 = dt1.Rows.Count;

        if (j1 > 0)
        {
            ddl_desig.DataSource = dt1;
            ddl_desig.DataValueField = "desigid";
            ddl_desig.DataTextField = "designame";
            ddl_desig.DataBind();
            ddl_desig.Items.Insert(0, "[SELECT]");

        }

        c.Con.Close();
    }
}