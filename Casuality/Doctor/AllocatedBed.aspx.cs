﻿//View details of patient on the bed and give treatment
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Casuality_Doctor_AllocatedBed : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            bindlabel();
            lbl_caslab.Visible = false;
            mulview_doctor.ActiveViewIndex = 0;
            BindGridView();
            BindGridView_Vitals();
            BindGridView_GridView_Obsv();
            GridView_caslab_Bind();
            Bind_meds_ddl();
        }
    }
    protected void GridView_CasLab_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_CasLab_Result.PageIndex = e.NewPageIndex;
        GridView_caslab_Bind();
    }
    public void GridView_caslab_Bind()
    {
        c.getCon();
      int  pno = Convert.ToInt32(lbl_patno.Text);


        SqlCommand cmd = new SqlCommand("select * from CasualityLabResult lr inner join CasualityLabRequest r on lr.labreqid=r.labreqid inner join CasualityEmployeeDetails e on e.empid=r.docid inner join LabSubTestMaster l on lr.labmasterid=l.testmasterid where patientid=@pno order by r.dateofreq desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_CasLab_Result.DataSource = dt;
            GridView_CasLab_Result.DataBind();
        }
        else
            lbl_caslab.Visible = true;
        c.Con.Close();

    }
    //public void Bind_meds_ddl()
    //{
    //    c.getCon();

    //    SqlCommand cmd_med = new SqlCommand("select * from MedMaster ", c.Con);
    //    SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
    //    DataTable dt_med = new DataTable();
    //    sda_med.Fill(dt_med);


    //    int k_med = cmd_med.ExecuteNonQuery();

    //    if (dt_med.Rows.Count > 0)
    //    {

    //        ddl_pres.DataSource = dt_med;
    //        ddl_pres.DataTextField = "MedName";
    //        ddl_pres.DataValueField = "MedId";
    //        ddl_pres.DataBind();
    //    }
    //    ddl_pres.Items.Insert(0, "[Select]");
    //    c.Con.Close();
    //}
    
    public void bindlabel()
    {
        c.getCon();
        int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

        SqlCommand cmd_pat = new SqlCommand("select p.patientid,p.pname,p.dob,p.gender,c.status,c.bedid from patientdetails p inner join CasualityBedAllocation c on p.patientid=c.patientid where c.bedid ='" + bid + "' and c.timeout=' ' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            String name = Convert.ToString(dt_pat.Rows[0][1]);
            String pid = Convert.ToString(dt_pat.Rows[0][0]);
            String bedid = Convert.ToString(dt_pat.Rows[0][5]);
            string status = Convert.ToString(dt_pat.Rows[0][4]);
            DateTime dob = Convert.ToDateTime(dt_pat.Rows[0][2]);
            String gender = Convert.ToString(dt_pat.Rows[0][3]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_age.Text = age.ToString();
            lbl_gen.Text = gender;
            lbl_patno.Text = pid;
            lbl_pname.Text = name;
        }
        c.Con.Close();
    }
    protected void link_inj_Click(object sender, EventArgs e)
    {
        mulview_doctor.ActiveViewIndex = 1;
    }
    private void BindGridView()
    {
        c.getCon();
        int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());
        SqlCommand cmd = new SqlCommand("select * from CasualityInjectionTable i inner Join InjectionMaster m on i.injectionid=m.injectionid inner join patientdetails p on p.patientid=i.patientid inner join CasualityInjectionResult r on i.injecreqid=r.injecreqid inner join CasualityEmployeeDetails e on r.nurseid=e.empid where (i.status=@stat and i.patientid=@pno ) or ( i.status=@status and i.patientid=@pno )  order by i.injecreqid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", lbl_patno.Text);
        cmd.Parameters.AddWithValue("@bid", bid);
        cmd.Parameters.AddWithValue("@stat", "Completed");
        cmd.Parameters.AddWithValue("@status", "In Progress");
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            lbl_inj.Visible = false;

            GridView_InjReq.DataSource = dt;
            GridView_InjReq.DataBind();
        }
        else
        {
            lbl_inj.Visible = true;
        }
        c.Con.Close();
    }
    protected void GridView_InjReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_InjReq.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    
    protected void link_observ_Click(object sender, EventArgs e)
    {
        mulview_doctor.ActiveViewIndex = 0;
    }
    
       protected void link_vit_Click(object sender, EventArgs e)
    {
        mulview_doctor.ActiveViewIndex = 2;
    }
    
    public void BindGridView_Vitals()
    {
        c.getCon();
        int pno = Convert.ToInt32(lbl_patno.Text);

        SqlCommand cmd = new SqlCommand("select * from VitalMaster vm inner join CasualityVitalTable v on vm.vitalmasterid=v.vitalmasterid inner join CasualityEmployeeDetails e on e.empid=v.nurseid  where patientid=@pno order by vitalid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_vit.Visible = false;
            GridView_Vitals.DataSource = dt;
            GridView_Vitals.DataBind();
        }
        else
        {
            lbl_vit.Visible = true;

        }
        c.Con.Close();

    }
    protected void GridView_Vitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Vitals.PageIndex = e.NewPageIndex;
        BindGridView_Vitals();
    }

    protected void link_dis_Click(object sender, EventArgs e)
    {
        c.getCon();
        int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

        String s = "update CasualityBedAllocation set status='Completed',docid='" + Session["uid"] + "' where bedid=@bid and patientid=@pid and timeout=@date";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.Parameters.AddWithValue("@pid", lbl_patno.Text);
        cmds.Parameters.AddWithValue("@bid", bid);
        cmds.Parameters.AddWithValue("@date", ' ');
        cmds.ExecuteNonQuery();
        c.Con.Close();
        Response.Redirect("CasualityHome.aspx");
    }
    protected void GridView_Obsv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Obsv.PageIndex = e.NewPageIndex;
        BindGridView_GridView_Obsv();
    }
    public void BindGridView_GridView_Obsv()
    {
        c.getCon();
        int pno = Convert.ToInt32(lbl_patno.Text);

        SqlCommand cmd = new SqlCommand("select * from CasualityObservations o inner join CasualityEmployeeDetails e on e.empid=o.observedby  where patientid=@pno order by observid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);


        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_observ.Visible = false;

            GridView_Obsv.DataSource = dt;
            GridView_Obsv.DataBind();
        }
        else
        {
            lbl_observ.Visible = true;

        }
        c.Con.Close();
    }
    protected void gridview_pres_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getCon();
        String id = gridview_pres.DataKeys[e.RowIndex].Values[0].ToString();
        String s = "delete Prescription where presid='" + Convert.ToInt32(id) + "'";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();
        gridview_pres.EditIndex = -1;
        c.Con.Close();
        BindGridView();

    }
    protected void gridview_pres_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gridview_pres.EditIndex = e.NewEditIndex;

        BindGridView();

    }
    public void Bind_meds_ddl()
    {
        c.getCon();

        SqlCommand cmd_med = new SqlCommand("select * from MedMaster ", c.Con);
        SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
        DataTable dt_med = new DataTable();
        sda_med.Fill(dt_med);


        int k_med = cmd_med.ExecuteNonQuery();

        if (dt_med.Rows.Count > 0)
        {

            ddl_pres.DataSource = dt_med;
            ddl_pres.DataTextField = "MedName";
            ddl_pres.DataValueField = "MedId";
            ddl_pres.DataBind();
        }
        ddl_pres.Items.Insert(0, "[Select]");
        c.Con.Close();
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from CasualityEmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);
            //SqlCommand cmd_med = new SqlCommand("select * from MedMaster where MedName='" + txt_dname.Text + "' ", c.Con);
            //SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
            //DataTable dt_med = new DataTable();
            //sda_med.Fill(dt_med);


            //int k_med = cmd_med.ExecuteNonQuery();

            //if (dt_med.Rows.Count > 0)
            //{
            //    DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

            //    int medid = Convert.ToInt32(row_med[0]);
                int pno = Convert.ToInt32(lbl_patno.Text);

                String s = "insert into Prescription values('" + pno + "','" + Session["docid"] + "','" + ddl_pres.SelectedItem.Value + "','" + txt_potency.Text + "','" + txt_dose.Text + "','" + txt_freq.Text + "','" + txt_days.Text + "','" + txt_remarks.Text + "','" + DateTime.Today + "','" + deptid + "','Prescribed')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();
                c.Con.Close();
                txt_remarks.Text = " ";
                txt_potency.Text = " ";
                txt_freq.Text = " ";
                txt_dose.Text = " ";
                //txt_dname.Text = " ";
                txt_days.Text = " ";
                ddl_pres.SelectedIndex = 0;
                BindGridViewpres();
            //}
        }
    }
    public void BindGridViewpres()
    {
        c.getCon();
        int pno = Convert.ToInt32(lbl_patno.Text);

        SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and docid=@docid order by presid", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", Session["docid"]);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pres.DataSource = dt;
            gridview_pres.DataBind();
        }
        c.Con.Close();
    }

    protected void btn_ref_Click(object sender, EventArgs e)
    {
        txt_remarks.Text = " ";
        txt_potency.Text = " ";
        txt_freq.Text = " ";
        txt_dose.Text = " ";
       // txt_dname.Text = " ";
        txt_days.Text = " ";
        c.getCon();
        int pno = Convert.ToInt32(lbl_patno.Text);

        SqlCommand cmd = new SqlCommand("select * from Prescription where patientid=@pno and dateofpres=@date and docid=@docid order by presid", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", Session["docid"]);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pres.DataSource = dt;
            gridview_pres.DataBind();
        }
        c.Con.Close();
    }
    protected void gridview_pres_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        c.getCon();
        string pid = gridview_pres.DataKeys[e.RowIndex].Values[0].ToString();
        string dname = ((TextBox)gridview_pres.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
        string potency = (gridview_pres.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text;
        string dosage = (gridview_pres.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text;
        string freq = (gridview_pres.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text;
        string noofdays = (gridview_pres.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text;
        string upd = "update Prescription set potency='" + potency + "',frequency='" + freq + "',dosage='" + dosage + "',no_of_days='" + noofdays + "' where presid='" + pid + "'";
        SqlCommand cmd = new SqlCommand(upd, c.Con);
        cmd.ExecuteNonQuery();
        gridview_pres.EditIndex = -1;
        c.Con.Close();
        BindGridViewpres();

    }
    protected void gridview_pres_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gridview_pres.EditIndex = -1;
        BindGridViewpres();
    }

    protected void link_pres_Click(object sender, EventArgs e)
    {
        mulview_doctor.ActiveViewIndex = 3;
    }
    protected void link_lab_Click(object sender, EventArgs e)
    {
        mulview_doctor.ActiveViewIndex = 4;
    }
}