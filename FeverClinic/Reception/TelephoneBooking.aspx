﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Reception/FeverClinicReceptionMaster.master" AutoEventWireup="true" CodeFile="TelephoneBooking.aspx.cs" Inherits="FeverClinic_Reception_TelephoneBooking" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <center>
    <table id="Table1"  runat="server">
        <tr>
            <td> <ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
         <tr>
                <td>Enter Patient ID</td>
                <td>
                    <asp:TextBox ID="txt_pid" runat="server" CssClass="twitter"></asp:TextBox>
                                        <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Search" CssClass="btn" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pid" ErrorMessage="*Enter Patient Card no" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td >Patient Name</td>
                <td>
                    <asp:TextBox ID="txt_pname" runat="server" ReadOnly="True" Enabled="False" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>Patient Date of Birth</td>
                <td>
                    <asp:TextBox ID="txt_dob" runat="server" Enabled="False" ReadOnly="True" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>
                    Date of Booking</td>
                <td>
                     <asp:TextBox ID="txtDate" runat="server" CssClass="twitter" />
<ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDate" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_generate" runat="server" OnClick="btn_generate_Click" Text="Book appointment" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
                    <td >Token No:</td>
                    <td >
                        <asp:Label ID="lbl_token" runat="server"></asp:Label></td>
                        </tr>
          
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table></center>
    </form>
</asp:Content>

