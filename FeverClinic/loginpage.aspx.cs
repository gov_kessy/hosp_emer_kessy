﻿//Login page for fever clinic employees
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class FeverClinic_loginpage : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        lbl_incorrect.Visible = false;

    }
    protected void btn_sub_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from UserTable where username='" + txt_user.Text + "' and password='" + txt_pswd.Text + "'", c.Con);
        // cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();

        if (dt.Rows.Count > 0)
        {
            DataRow row1 = dt.Rows[dt.Rows.Count - 1];
            string log = "insert into LogTable values('" + txt_user.Text + "','" + DateTime.Now + "')";
            SqlCommand cmdsd = new SqlCommand(log, c.Con);
            cmdsd.ExecuteNonQuery();
            Session["id"] = row1[1];
            Session["docid"] = row1[3];
            Session["uid"] = row1[3];
            int type = Convert.ToInt32(row1["emptypeid"]);
            string mod = Convert.ToString(row1["module"]);
            SqlCommand cmds = new SqlCommand("select * from EmpTypeMaster where emptypeid='" + type + "'", c.Con);
            // cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
            SqlDataAdapter sdas = new SqlDataAdapter(cmds);
            DataTable dts = new DataTable();
            sdas.Fill(dts);

            int j = cmds.ExecuteNonQuery();

            if (dts.Rows.Count > 0)
            {
                DataRow rows = dts.Rows[dts.Rows.Count - 1];
                string typename = Convert.ToString(rows[1]);

                if (String.Compare(typename, "Doctor") == 0 && String.Compare(mod, "Fever Clinic") == 0)
                    Response.Redirect("Doctor/homepagedoctor.aspx");
                else if (String.Compare(typename, "Receptionist") == 0 && String.Compare(mod, "Fever Clinic") == 0)
                    Response.Redirect("Reception/homepagereception.aspx");
                txt_pswd.Text = " ";
                txt_user.Text = " ";
            }
        }
        else
        {
            lbl_incorrect.Visible = true;
            lbl_incorrect.Text = "You're username or password is incorrect";
            lbl_incorrect.ForeColor = System.Drawing.Color.Red;

        }

    }

}
