﻿//view total stock of each medicine
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Data.SqlClient;
public partial class Pharmacy_CasualityPharmacy_ViewStock : System.Web.UI.Page
{
    conclass c = new conclass();
    int injid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //BindLabels();
            BindGridView();
            BindChart();
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }


    }
    private void BindGridView()
    {
        c.getCon();
        injid = Convert.ToInt32(Request.QueryString["InjreqNo"]);

        SqlCommand cmd = new SqlCommand("select m.injectionid,m.injectionname,s.stockvalue,b.batchexpdate from InjectionMaster m inner join CasualityInjectionStock s on s.injecid=m.injectionid inner join BatchCasuality b on b.batchid=s.batchid where m.injectionid='" + injid + "' order by injectionname", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            String iname = Convert.ToString(dt.Rows[0][1]);
            int id = Convert.ToInt32(dt.Rows[0][0]);
            lbl_injid.Text = id.ToString();
            lbl_injname.Text = iname;
        }

        c.Con.Close();
    }
    //public void BindChart()
    //{
    //    c.getCon();
    //    SqlCommand cmd_chart = new SqlCommand("select orderamt,dateoforder  from CasualityInjectionOrder where injectionid='" + lbl_injid.Text + "' and status='Fullfilled'", c.Con);
    //    SqlDataAdapter sda_chart = new SqlDataAdapter(cmd_chart);
    //    DataTable dt_chart = new DataTable();
    //    sda_chart.Fill(dt_chart);
    //    int k_pat = cmd_chart.ExecuteNonQuery();
    //    int amt = 0;
    //    if (dt_chart.Rows.Count > 0)
    //    {
    //        int[] x = new int[dt_chart.Rows.Count];
    //        int[] y = new int[dt_chart.Rows.Count];
    //        for (int i = 0; i < dt_chart.Rows.Count; i++)
    //        {

    //            int order = Convert.ToInt32(dt_chart.Rows[i][0]);
    //            DateTime date = Convert.ToDateTime(dt_chart.Rows[i][1]);
    //            int year = date.Year;
    //            amt = amt + order;
    //            x[i] = year;
    //            y[i] = order;
    //        }
    //        amt = amt / dt_chart.Rows.Count;
    //        chart_inj.Series[0].Points.DataBindXY(x, y);
    //        lbl_est.Text = amt.ToString();


    //    }


    //}
    //public void BindLabels()
    //{
    //    injid = Convert.ToInt32(Request.QueryString["InjreqNo"]);
    //    c.getCon();
    //    SqlCommand cmd_pat = new SqlCommand("select i.injectionid,i.injectionname,s.stockvalue  from InjectionMaster i inner join CasualityInjectionStock s on i.injectionid=s.injecid where s.injstockid= '" + injid + "' ", c.Con);
    //    SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
    //    DataTable dt_pat = new DataTable();
    //    sda_pat.Fill(dt_pat);
    //    int k_pat = cmd_pat.ExecuteNonQuery();
    //    if (dt_pat.Rows.Count > 0)
    //    {
    //        DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];
    //        String iname = Convert.ToString(row_pat[1]);
    //        int stock = Convert.ToInt32(row_pat[2]);
    //        int id = Convert.ToInt32(row_pat[0]);
    //        lbl_injid.Text = id.ToString();
    //        lbl_injname.Text = iname;
    //    }
    //    c.Con.Close();
    //}
    public void BindChart()
    {
        c.getCon();
        SqlCommand cmd_chart = new SqlCommand("select sum(orderamt),Year(dateoforder)  from CasualityInjectionOrder where injectionid='" + lbl_injid.Text + "' and status='Fullfilled' group by Year(dateoforder)", c.Con);
        SqlDataAdapter sda_chart = new SqlDataAdapter(cmd_chart);
        DataTable dt = new DataTable();
        sda_chart.Fill(dt);
        int k_pat = cmd_chart.ExecuteNonQuery();
        int amt = 0;
        if (dt.Rows.Count > 0)
        {
            string[] x = new string[dt.Rows.Count];
            decimal[] y = new decimal[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                int count = Convert.ToInt32(dt.Rows[i][0]);
                string date = Convert.ToString(dt.Rows[i][1]);
                amt = amt + count;

                x[i] = Convert.ToString(date);
                y[i] = count;
            }
            amt = amt / dt.Rows.Count;
            lbl_est.Text = amt.ToString();
            //LineChart1.Series[0].Points.DataBindXY(x, y);
            //lbl_est.Text = amt.ToString();
            LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
            LineChart1.CategoriesAxis = string.Join(",", x);
            LineChart1.ChartTitle = string.Format("{0} ", "Previous Orders");
            if (x.Length > 3)
            {
                LineChart1.ChartWidth = (x.Length * 75).ToString();
            }
            LineChart1.Visible = true;
        }

    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s_in = "insert into CasualityInjectionOrder values('" + lbl_injid.Text + "','" + txt_amount.Text + "','" + DateTime.Today + "','Pending',' ')";
        SqlCommand cmdsin = new SqlCommand(s_in, c.Con);
        
        cmdsin.ExecuteNonQuery();
        
        c.Con.Close();
        Response.Redirect("PlaceOrders.aspx");
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView();
    }

    protected void link_previous_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Show();
    }
}