﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/OPPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="ViewStockMeds.aspx.cs" Inherits="Pharmacy_OPPharmacy_ViewStockMeds" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form2" runat="server">
        <table class="auto-style1">
            <tr><td><cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager></td><td></td></tr>
            <tr>
                <td class="auto-style2">Medicine Id</td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_medid" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Medicine Name</td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_medname" runat="server"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td class="auto-style2">Current Stock Values</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="MedId" OnPageIndexChanging="GridView1_PageIndexChanging"  Width="800px" HorizontalAlign="Center">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                        <Columns>
                            <asp:BoundField DataField="stockvalue" HeaderStyle-HorizontalAlign="Center" HeaderText="Stock Value" ItemStyle-HorizontalAlign="Center" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="batchexpdate" HeaderStyle-HorizontalAlign="Center" HeaderText="Expiry date" ItemStyle-HorizontalAlign="Center" >
                            
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="link_previous" runat="server" OnClick="link_previous_Click">View Previous Usage Trend</asp:LinkButton>
                </td>
                <td>&nbsp;</td>
            </tr>
             <tr>
               <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopupExtender1" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopup" TargetControlID="link_previous"  BackgroundCssClass="modalBackground" CancelControlID = "btn_okay"></cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" >
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart1" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_okay" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                       
            <tr>
                <td colspan="2">
                    
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Estimated Value of Injection needed</td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_est" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Order Amount</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_amount" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_add" runat="server" Text="Add" OnClick="btn_add_Click" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>



