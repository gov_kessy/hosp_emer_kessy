﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/CasualityPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="TallyOrder.aspx.cs" Inherits="Pharmacy_CasualityPharmacy_TallyOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr><td><ajax:toolkitscriptmanager ID="ToolkitScriptManager1" runat="server">
</ajax:toolkitscriptmanager></td><td></td></tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Order Id</td>
                <td>
                    <asp:Label ID="lbl_orderid" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Medicine Id</td>
                <td>
                    <asp:Label ID="lbl_medid" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Medicine Name</td>
                <td>
                    <asp:Label ID="lbl_medname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Order Amount</td>
                <td>
                    <asp:Label ID="lbl_orderamt" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Order Date</td>
                <td>
                    <asp:Label ID="lbl_orderdate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Amount Received</td>
                <td>
                    <asp:TextBox ID="txt_amtrec" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Expiry Date of Medicine batch</td>
                <td>
                    <asp:TextBox ID="txtDate" runat="server" CssClass="twitter" />
<ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDate" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_tally" runat="server" OnClick="btn_tally_Click" Text="Tally Order" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

