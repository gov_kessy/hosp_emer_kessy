﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/CasualityPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="PlaceOrders.aspx.cs" Inherits="Pharmacy_CasualityPharmacy_PlaceOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <table><tr><td>&nbsp;</td></tr>
        <tr><td>
            <asp:LinkButton ID="link_print" runat="server" OnClick="link_print_Click">Print The Order</asp:LinkButton>
            </td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td><asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" PageSize="50" DataKeyNames="injectionid" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" Width="800px" HorizontalAlign="Center">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                         <Columns>  
                              <asp:BoundField DataField="injectionname" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Particulars" >  
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                              </asp:BoundField>
                     <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >

                        
                             
<ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>

                        
                             
                    </Columns>  
                    </asp:GridView></td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>
            &nbsp;</td></tr>
    </table>
        </form>
</asp:Content>

