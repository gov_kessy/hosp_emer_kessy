﻿//Generate token for patient appointments
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;

public partial class FeverClinic_Reception_generatetoken : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/FeverClinic/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

            tab_print.Visible = false;
            btn_print.Visible = false;
            img_logo.ImageUrl = "~/OPD/Registration/image/gov.png";
        }
    }

    protected void btn_generate_Click(object sender, EventArgs e)
    {
        int count;
        c.getCon();

        
            SqlCommand cmddept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%'", c.Con);
            SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
            DataTable dtdept = new DataTable();
            sdadept.Fill(dtdept);

            int idept = cmddept.ExecuteNonQuery();
            int j = dtdept.Rows.Count;
            if (j > 0)
            {
                DataRow row = dtdept.Rows[dtdept.Rows.Count - 1];

                int did = Convert.ToInt32(row[0]);

                SqlCommand cmd = new SqlCommand("select count(*) from Schedule where scheduledate='" + DateTime.Today + "' and deptid ='" + did + "'", c.Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                int r = cmd.ExecuteNonQuery();

                if (dt.Rows.Count > 0)
                {
                    DataRow ro = dt.Rows[dt.Rows.Count - 1];

                    count = Convert.ToInt32(ro[0]);
                    tab_print.Visible = true;
                    btn_print.Visible = true;

                    lbl_patno.Text = txt_pid.Text;
                    lbl_token.Text = Convert.ToString(count + 1);

                    lbl_date.Text = DateTime.Now.ToString();
                    lbl_pname.Text = txt_pname.Text;
                    SqlCommand cmd_post = new SqlCommand("select * from patientdetails where patientid= '" + txt_pid.Text + "' ", c.Con);
                    SqlDataAdapter sda_post = new SqlDataAdapter(cmd_post);
                    DataTable dt_post = new DataTable();
                    sda_post.Fill(dt_post);

                    int k = cmd_post.ExecuteNonQuery();

                    if (dt_post.Rows.Count > 0)
                    {
                        DataRow row11 = dt_post.Rows[dt_post.Rows.Count - 1];
                        DateTime todaydate = DateTime.Now;
                        string pname = Convert.ToString(row11[1]);
                        DateTime dob = Convert.ToDateTime(row11[7]);
                        int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                        lbl_age.Text = age.ToString();
                        lbl_pname.Text = pname;
                    }
                    string id = Session["uid"].ToString();
                    SqlCommand cmd_pos = new SqlCommand("select * from EmployeeDetails where empid= '" + id + "' ", c.Con);
                    SqlDataAdapter sda_pos = new SqlDataAdapter(cmd_pos);
                    DataTable dt_pos = new DataTable();
                    sda_pos.Fill(dt_pos);

                    int k_pos = cmd_pos.ExecuteNonQuery();

                    if (dt_pos.Rows.Count > 0)
                    {
                        DataRow row_pos = dt_pos.Rows[dt_pos.Rows.Count - 1];
                        string uname = Convert.ToString(row_pos[2]);
                        lbl_postedby.Text = uname;
                    }
                }

                String s = "insert into Schedule values('" + txt_pid.Text + "','" + DateTime.Today + "','" + DateTime.Now + "','" + did + "','Confirmed','0',' ')";
                SqlCommand cmd2 = new SqlCommand(s, c.Con);
                cmd2.ExecuteNonQuery();
                txt_dob.Text = " ";
                txt_pid.Text = " ";
                txt_pname.Text = " ";
            }      
        c.Con.Close();
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmdbk = new SqlCommand("select * from patientdetails where patientid= '" + txt_pid.Text + "' ", c.Con);
        SqlDataAdapter sdabk = new SqlDataAdapter(cmdbk);
        DataTable dtbk = new DataTable();
        sdabk.Fill(dtbk);

        int k = cmdbk.ExecuteNonQuery();

        if (dtbk.Rows.Count > 0)
        {
            DataRow rowbk = dtbk.Rows[dtbk.Rows.Count - 1];
            DateTime todaydate = DateTime.Now;
            string pname = Convert.ToString(rowbk[1]);
            String pdob = Convert.ToString(rowbk[7]);
            DateTime lrdate = Convert.ToDateTime(rowbk[9]);
            int y = todaydate.Year.CompareTo(lrdate.Year);
            if (y >= 1)
            {
                Response.Write("<script>alert('Renewal Needed');</script>");
            }

            txt_pname.Text = pname;
            txt_dob.Text = pdob;
        }
        c.Con.Close();
    }

protected void btn_print_Click(object sender, EventArgs e)
    {
    Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(2);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            table.SetWidths(new float[] { 0.3f, 0.7f });

            //Company Logo
            cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("Changanacherry,\n", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)));
            phrase.Add(new Chunk("Kottayam,\n", FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)));
            phrase.Add(new Chunk("Kerala-686101", FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)));
            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
            DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            //Name
            phrase = new Phrase();
            phrase.Add(new Chunk("Token Print\n", FontFactory.GetFont("Arial", 14, Font.BOLD, Color.BLACK)));
            //phrase.Add(new Chunk("(" + dr["Title"].ToString() + ")", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)));
            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            table.AddCell(cell);
            //document.Add(table);

            //DrawLine(writer, 160f, 80f, 160f, 690f, Color.BLACK);
            //DrawLine(writer, 115f, document.Top - 200f, document.PageSize.Width - 100f, document.Top - 200f, Color.BLACK);

            table = new PdfPTable(2);
            table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_RIGHT;
            document.Add(table);

            //Employee Id
            table.AddCell(PhraseCell(new Phrase("\n\n\nDate:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase( "\n\n\n"+lbl_date.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 10f;
            table.AddCell(cell);


            ////Address
            //table.AddCell(PhraseCell(new Phrase("Patient Name:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //phrase = new Phrase(new Chunk(dr["Address"] + "\n", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)));
            //phrase.Add(new Chunk(dr["City"] + "\n", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)));
            //phrase.Add(new Chunk(dr["Region"] + " " + dr["Country"] + " " + dr["PostalCode"], FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)));
            //table.AddCell(PhraseCell(phrase, PdfPCell.ALIGN_LEFT));
            //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            //cell.Colspan = 2;
            //cell.PaddingBottom = 10f;
            //table.AddCell(cell);

            //Date of Birth
            table.AddCell(PhraseCell(new Phrase("Patient Id:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase(lbl_patno.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 10f;
            table.AddCell(cell);

            //Phone
            table.AddCell(PhraseCell(new Phrase("Patient Name:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase(lbl_pname.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 10f;
            table.AddCell(cell);

            table.AddCell(PhraseCell(new Phrase("Age:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase(lbl_age.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 10f;
            table.AddCell(cell);

            table.AddCell(PhraseCell(new Phrase("Token Number:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase(lbl_token.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 10f;
            table.AddCell(cell);

            table.AddCell(PhraseCell(new Phrase("Posted By:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase(lbl_postedby.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 10f;
            table.AddCell(cell);
            document.Add(table);

            ////Addtional Information
            //table.AddCell(PhraseCell(new Phrase("Addtional Information:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table.AddCell(PhraseCell(new Phrase(dr["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_JUSTIFIED));
            //document.Add(table);
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=token.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
}
 
private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2, Color color)
{
    PdfContentByte contentByte = writer.DirectContent;
    contentByte.SetColorStroke(color);
    contentByte.MoveTo(x1, y1);
    contentByte.LineTo(x2, y2);
    contentByte.Stroke();
}
private static PdfPCell PhraseCell(Phrase phrase, int align)
{
    PdfPCell cell = new PdfPCell(phrase);
    cell.BorderColor = Color.WHITE;
    cell.VerticalAlignment = PdfCell.ALIGN_TOP;
    cell.HorizontalAlignment = align;
    cell.PaddingBottom = 2f;
    cell.PaddingTop = 0f;
    return cell;
}
private static PdfPCell ImageCell(string path, float scale, int align)
{
    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path));
    image.ScalePercent(scale);
    PdfPCell cell = new PdfPCell(image);
    cell.BorderColor = Color.WHITE;
    cell.VerticalAlignment = PdfCell.ALIGN_TOP;
    cell.HorizontalAlignment = align;
    cell.PaddingBottom = 0f;
    cell.PaddingTop = 0f;
    return cell;
}
}



