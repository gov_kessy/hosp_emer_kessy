﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/OPPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="PatPrescriptions.aspx.cs" Inherits="Pharmacy_OPPharmacy_PatPrescriptions" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Patient Id</td>
                <td>
                    <asp:Label ID="lbl_patno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lbl_pre" runat="server" ForeColor="#FF5050" Text="No Prescriptions Found"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                               <asp:GridView ID="gridview_pres" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="auto-style2" DataKeyNames="presid" Height="182px" 
                                    Width="1000px" OnRowCommand="gridview_pres_RowCommand" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" HorizontalAlign="Center">
                                   <Columns>
                                       <asp:BoundField DataField="MedName" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Medicine Name" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                       <asp:BoundField DataField="potency" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Potency" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                       <asp:BoundField DataField="dosage" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Dosage" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                        <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >
                                <ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>   
               </Columns>
               <FooterStyle BackColor="White" ForeColor="#000066" />
               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"></HeaderStyle>
               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               <RowStyle ForeColor="#000066" />
               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
               <sortedascendingcellstyle backcolor="#F1F1F1" />
               <sortedascendingheaderstyle backcolor="#007DBB" />
               <sorteddescendingcellstyle backcolor="#CAC9C9" />
               <sorteddescendingheaderstyle backcolor="#00547E" />
               </asp:GridView></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

