﻿//View patient prescription
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
public partial class Pharmacy_CasualityPharmacy_PatientPrescriptions : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            lbl_pre.Visible = false;
            link_print.Visible = false;
        }

    }
    protected void btn_srch_Click(object sender, EventArgs e)
    {
        BindGridView();
    }
    public void BindGridView()
    {
        c.getCon();
        string pno = txt_patno.Text;
        SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and status=@stat order by presid", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@stat", "Prescribed");
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pres.DataSource = dt;
            gridview_pres.DataBind(); 
            link_print.Visible = true;

        }
        else
            lbl_pre.Visible = true;
        c.Con.Close();
    }

        protected void gridview_pres_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("DrugPresPage.aspx?DrugNo=" +  gridview_pres.DataKeys[index].Value.ToString());
        }
    }

        public void btnExportPDF()
        {
            Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                Phrase phrase = null;
                PdfPCell cell = null;
                PdfPTable table = null;
                Color color = null;

                document.Open();

                //Header Table
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.3f, 0.7f });

                //Company Logo
                cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
                table.AddCell(cell);

                //Company Name and Address
                phrase = new Phrase();
                phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
                phrase.Add(new Chunk("Changanacherry,\n", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)));
                phrase.Add(new Chunk("Kottayam,\n", FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)));
                phrase.Add(new Chunk("Kerala-686101", FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)));
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
                cell.VerticalAlignment = PdfCell.ALIGN_TOP;
                table.AddCell(cell);

                //Separater Line
                color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
                DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
                DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
                document.Add(table);

                table = new PdfPTable(2);
                table.HorizontalAlignment = Element.ALIGN_LEFT;
                table.SetWidths(new float[] { 0.3f, 1f });
                table.SpacingBefore = 20f;

                table = new PdfPTable(2);
                table.SetWidths(new float[] { 0.5f, 2f });
                table.TotalWidth = 340f;
                table.LockedWidth = true;
                table.SpacingBefore = 20f;
                table.HorizontalAlignment = Element.ALIGN_RIGHT;
                document.Add(table);

                c.getCon();
                SqlCommand cmd_post = new SqlCommand("select * from patientdetails where patientid= '" + txt_patno.Text + "' ", c.Con);
                SqlDataAdapter sda_post = new SqlDataAdapter(cmd_post);
                DataTable dt_post = new DataTable();
                sda_post.Fill(dt_post);

                int k = cmd_post.ExecuteNonQuery();

                if (dt_post.Rows.Count > 0)
                {
                    DateTime todaydate = DateTime.Now;
                    string pname = Convert.ToString(dt_post.Rows[0][1]);
                    DateTime dob = Convert.ToDateTime(dt_post.Rows[0][7]);
                    int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

                    table.AddCell(PhraseCell(new Phrase("\n\n\nDate:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table.AddCell(PhraseCell(new Phrase("\n\n\n" + todaydate.ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 2;
                    cell.PaddingBottom = 10f;
                    table.AddCell(cell);

                    table.AddCell(PhraseCell(new Phrase("Patient Id:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table.AddCell(PhraseCell(new Phrase(txt_patno.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 2;
                    cell.PaddingBottom = 10f;
                    table.AddCell(cell);

                    table.AddCell(PhraseCell(new Phrase("Patient Name:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table.AddCell(PhraseCell(new Phrase(pname, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 2;
                    cell.PaddingBottom = 10f;
                    table.AddCell(cell);

                    table.AddCell(PhraseCell(new Phrase("Age:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table.AddCell(PhraseCell(new Phrase(age.ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 2;
                    cell.PaddingBottom = 10f;
                    table.AddCell(cell);
                }
                document.Add(table);

                c.Con.Close();
                PdfPTable table1 = new PdfPTable(5);
                table1.AddCell(PhraseCell(new Phrase("Medicine Name", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table1.AddCell(PhraseCell(new Phrase("Potency", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table1.AddCell(PhraseCell(new Phrase("Dosage", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table1.AddCell(PhraseCell(new Phrase("Frequency", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table1.AddCell(PhraseCell(new Phrase("Instructions", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                cell.Colspan = 5;
                cell.PaddingBottom = 10f;
                table1.AddCell(cell);
                c.getCon();
                string pno = txt_patno.Text;
                SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and status=@stat order by presid", c.Con);
                cmd.Parameters.AddWithValue("@pno", pno);
                cmd.Parameters.AddWithValue("@date", DateTime.Today);
                cmd.Parameters.AddWithValue("@stat", "Prescribed");
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);


                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][13].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][4].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][5].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][6].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][8].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                        cell.Colspan = 5;
                        cell.PaddingBottom = 10f;
                        table1.AddCell(cell);
                    }
                }
                document.Add(table1);

                ////Addtional Information
                //table.AddCell(PhraseCell(new Phrase("Addtional Information:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //table.AddCell(PhraseCell(new Phrase(dr["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_JUSTIFIED));
                //document.Add(table);
                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=prescription.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }
        }

        private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2, Color color)
        {
            PdfContentByte contentByte = writer.DirectContent;
            contentByte.SetColorStroke(color);
            contentByte.MoveTo(x1, y1);
            contentByte.LineTo(x2, y2);
            contentByte.Stroke();
        }
        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderColor = Color.WHITE;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 0f;
            return cell;
        }
        private static PdfPCell ImageCell(string path, float scale, int align)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path));
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            cell.BorderColor = Color.WHITE;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 0f;
            cell.PaddingTop = 0f;
            return cell;
        }

        protected void link_print_Click(object sender, EventArgs e)
        {
            btnExportPDF();
        }
}