﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Superintendent/SuperintendentHomeMaster.master" AutoEventWireup="true" CodeFile="ScheduleDocs.aspx.cs" Inherits="Superintendent_ScheduleDocs" %>

 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table class="nav-justified">
         <tr>
                <td ><ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
                <td class="auto-style12" >&nbsp;</td>
            </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div class="drop">
<ul class="drop_menu">
  <li><a href="#">View Schedule</a>
         <ul>
        <li>
            <asp:LinkButton ID="link_cas" runat="server" OnClick="link_cas_Click">Casuality Doctor Schedule</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_genop" runat="server" OnClick="link_genop_Click" >General OP Doctor Schedule</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_ncd" runat="server" OnClick="link_ncd_Click" >NCD Doctor Schedule</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_ward" runat="server" OnClick="link_ward_Click" >Ward Doctor Schedule</asp:LinkButton></li>
       
        </ul>
    </li>
    <li><a href="#">Create New Schedule</a>
         <ul>
        <li>
            <asp:LinkButton ID="link_ccas" runat="server" OnClick="link_ccas_Click">Casuality Doctor Schedule</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_cgenop" runat="server" OnClick="link_cgenop_Click" >General OP Doctor Schedule</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_cncd" runat="server" OnClick="link_cncd_Click" >NCD Doctor Schedule</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_cward" runat="server" OnClick="link_cward_Click" >Ward Doctor Schedule</asp:LinkButton></li>
       
        </ul>
    </li>
          
      </ul>
</div></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:MultiView ID="MultiView1" runat="server">
                    <table class="nav-justified">
                        <tr>
                            <td>
                                <asp:View ID="View1" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select From Date</td>
                                            <td><asp:TextBox ID="txt_casvfrom" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender2" TargetControlID="txt_casvfrom" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_casvfrom" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select To Date</td>
                                            <td><asp:TextBox ID="txt_casvto" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender3" TargetControlID="txt_casvto" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_casvto" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
          </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_vfind" runat="server" CssClass="btn" OnClick="btn_vfind_Click" Text="Find" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="grid_cas" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="dateofsch" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e1name" HeaderStyle-HorizontalAlign="Center" HeaderText="Morning Casuality" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e2name" HeaderStyle-HorizontalAlign="Center" HeaderText="Afternoon Casuality" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e3name" HeaderStyle-HorizontalAlign="Center" HeaderText="Night Casuality" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lbl_vcas" runat="server" ForeColor="#CC0000" Text="No Schedules Found" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_vcasprint" runat="server" CssClass="btn" OnClick="btn_vcasprint_Click" Text="Print Schedule" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View2" runat="server">
                                    <table class="nav-justified">
                                         <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select From Date</td>
                                            <td><asp:TextBox ID="txt_copfrom" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender5" TargetControlID="txt_copfrom" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_copfrom" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select To Date</td>
                                            <td><asp:TextBox ID="txt_copto" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender6" TargetControlID="txt_copto" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_copto" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
          </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_copfind" runat="server" CssClass="btn" OnClick="btn_copfind_Click" Text="Find" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="grid_cop" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="dateofsch" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e1name" HeaderStyle-HorizontalAlign="Center" HeaderText="Doctor 1" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e2name" HeaderStyle-HorizontalAlign="Center" HeaderText="Doctor 2" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                       
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lbl_copno" runat="server" ForeColor="#CC0000" Text="No Schedules Found" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_copprint" runat="server" CssClass="btn" OnClick="btn_copprint_Click" Text="Print Schedule" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    
                                    </table>
                                
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View3" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select From Date</td>
                                            <td><asp:TextBox ID="txt_wardfrom" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender8" TargetControlID="txt_wardfrom" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txt_wardfrom" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select To Date</td>
                                            <td><asp:TextBox ID="txt_wardto" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender9" TargetControlID="txt_wardto" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txt_wardto" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
          </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_wardfind" runat="server" CssClass="btn" OnClick="btn_wardfind_Click" Text="Find" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="grid_vward" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="dateofsch" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e1name" HeaderStyle-HorizontalAlign="Center" HeaderText="Day Ward Doctor " ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="e2name" HeaderStyle-HorizontalAlign="Center" HeaderText="Night Ward Doctor " ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                       
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lbl_wardno" runat="server" ForeColor="#CC0000" Text="No Schedules Found" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_wardprint" runat="server" CssClass="btn" OnClick="btn_wardprint_Click" Text="Print Schedule" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    
                                    </table>
                                
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View4" runat="server">
                                    <table class="nav-justified">
                                           <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select From Date</td>
                                            <td><asp:TextBox ID="txt_ncdfrom" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender10" TargetControlID="txt_ncdfrom" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txt_ncdfrom" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Select To Date</td>
                                            <td><asp:TextBox ID="txt_ncdto" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender11" TargetControlID="txt_ncdto" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txt_ncdto" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
          </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_ncdfind" runat="server" CssClass="btn" OnClick="btn_ncdfind_Click" Text="Find" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="grid_vncd" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="dateofsch" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="empname" HeaderStyle-HorizontalAlign="Center" HeaderText="Doctor " ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>                                                    
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lbl_ncdno" runat="server" ForeColor="#CC0000" Text="No Schedules Found" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_ncdprint" runat="server" CssClass="btn" OnClick="btn_ncdprint_Click" Text="Print Schedule" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    
                                    </table>
                                
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View5" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date </td>
                                            <td> 
                                                <asp:TextBox ID="txt_ccas" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender1" TargetControlID="txt_ccas" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_ccas" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Morning Duty Doctor</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_ccas_doc1" runat="server" AutoPostBack="True" CssClass="twitter" OnSelectedIndexChanged="ddl_ccas_duty_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_ccas_doc1" runat="server" ForeColor="Red" Text="*Select Doctor 1" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Afternoon Duty Doctor</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_ccas_doc2" runat="server" CssClass="twitter" AutoPostBack="True" OnSelectedIndexChanged="ddl_ccas_doc2_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_ccas_doc2" runat="server" ForeColor="Red" Text="*Select Doctor 2" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Night Duty Doctor&nbsp;</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_ccas_doc3" runat="server" CssClass="twitter">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_ccas_doc3" runat="server" ForeColor="Red" Text="*Select Doctor 3" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btn_cascan" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_cascan_Click" />
                                                &nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_casadd" runat="server" CssClass="btn" Text="Add Schedule" OnClick="btn_casadd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:GridView ID="grid_ccas" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                              <Columns>
                                       <asp:BoundField DataField="dateofsch" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="day" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="e1name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Morning Casuality" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="e2name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Afternoon Casuality" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                         <asp:BoundField DataField="e3name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Night Casuality" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        
               </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View6" runat="server">
                                    <table><tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date </td>
                                            <td> 
                                                <asp:TextBox ID="txt_cop" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender4" TargetControlID="txt_cop" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_cop" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Doctor 1</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_cop_doc1" runat="server" AutoPostBack="True" CssClass="twitter" OnSelectedIndexChanged="ddl_cop_doc1_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_cop_doc1" runat="server" ForeColor="Red" Text="*Select Doctor 1" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Doctor 2</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_cop_doc2" runat="server" CssClass="twitter" AutoPostBack="True" >
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_cop_doc2" runat="server" ForeColor="Red" Text="*Select Doctor 2" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btn_copcan" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_copcan_Click" />
                                                &nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_copadd" runat="server" CssClass="btn" Text="Add Schedule" OnClick="btn_copadd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:GridView ID="grid_ccop" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                              <Columns>
                                       <asp:BoundField DataField="dateofsch" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="day" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="e1name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Doctor 1" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="e2name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Doctor 2" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        
                                        
               </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr></table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View7" runat="server">
                                     <table><tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date </td>
                                            <td> 
                                                <asp:TextBox ID="txt_cwarddate" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:calendarextender ID="CalendarExtender7" TargetControlID="txt_cwarddate" Format="MM/dd/yyyy" runat="server">
</ajax:calendarextender> <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txt_cwarddate" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
               
               </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Day Ward Doctor</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_cdoc1" runat="server" AutoPostBack="True" CssClass="twitter" OnSelectedIndexChanged="ddl_cwarddoc1_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_cwarddoc1" runat="server" ForeColor="Red" Text="*Select Day Ward Doctor" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Night ward Doctor </td>
                                            <td>
                                                <asp:DropDownList ID="ddl_cdoc2" runat="server" CssClass="twitter" AutoPostBack="True" >
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_cwarddoc2" runat="server" ForeColor="Red" Text="*Select Night Ward Doctor" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btn_cwardcan" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_cwardcan_Click" />
                                                &nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_cwardadd" runat="server" CssClass="btn" Text="Add Schedule" OnClick="btn_cwardadd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:GridView ID="grid_cward" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                              <Columns>
                                       <asp:BoundField DataField="dateofsch" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="day" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="e1name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Day Ward" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        <asp:BoundField DataField="e2name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Night ward" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle><ItemStyle HorizontalAlign="Center"></ItemStyle></asp:BoundField>
                                        
                                        
               </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr></table>
                                
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:View ID="View8" runat="server">
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date </td>
                                            <td>
                                                <asp:TextBox ID="txt_cncddate" runat="server" CssClass="twitter"></asp:TextBox>
                                                <ajax:CalendarExtender ID="txt_cwarddate0_calendarextender" runat="server" Format="MM/dd/yyyy" TargetControlID="txt_cncddate">
                                                </ajax:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txt_cncddate" ErrorMessage="*Select Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Doctor</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_cncddoc" runat="server" AutoPostBack="True" CssClass="twitter" >
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_cncddoc" runat="server" ForeColor="Red" Text="*Select Doctor" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btn_cncdcan" runat="server" CssClass="btn" OnClick="btn_cncdcan_Click" Text="Cancel" />
                                                &nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_cncdadd" runat="server" CssClass="btn" OnClick="btn_cncdadd_Click" Text="Add Schedule" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:GridView ID="grid_cncd" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="dateofsch" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="empname" HeaderStyle-HorizontalAlign="Center" HeaderText="Doctor" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:View ID="View9" runat="server">
                                    <br /><br /><br />
                                    <div>
                                                       <ul id="sti-menu" class="sti-menu">
	        		<li data-hovercolor="#fff">
					<a href="#">
						<h4 data-type="mText" class="sti-item">View Schedules</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-eye-open sti-item"></span>
						<span data-type="icon" class="gly"></span>

					</a>
				</li>
                                                           <li data-hovercolor="#fff">
					<a href="#">
						<h4 data-type="mText" class="sti-item">Create Schedules</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-pencil sti-item"></span>
						<span data-type="icon" class="gly"></span>

					</a>
				</li>
                                                       </ul>
                                    </div>
                                </asp:View>
                            </td>
                        </tr>
                    </table>
                </asp:MultiView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
</asp:Content>

