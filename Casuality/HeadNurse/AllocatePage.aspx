﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="AllocatePage.aspx.cs" Inherits="AllocatePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5" colspan="2"><strong style="color: #0000CC">CASUALITY BEDS AND ALLOCATIONS</strong></td>
        </tr>
        <tr>
            <td class="auto-style8"></td>
            <td class="auto-style4">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style8">Patient Id</td>
            <td class="auto-style4">
                <asp:TextBox ID="txt_patid" runat="server" CssClass="twitter"></asp:TextBox>
                <asp:Button ID="btn_patfind" runat="server" OnClick="btn_patfind_Click" Text="Find Patient" CssClass="btn" />
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Patient Name</td>
            <td class="auto-style2">
                <asp:Label ID="lbl_name" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style10">Age</td>
            <td class="auto-style6">
                <asp:Label ID="lbl_age" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style10">&nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style8">Gender</td>
            <td class="auto-style4">
                <asp:Label ID="lbl_gen" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style8">&nbsp;</td>
            <td class="auto-style4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style8"></td>
            <td class="auto-style4"></td>
        </tr>
        <tr>
            <td class="auto-style11">
                <asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add" Width="85px" CssClass="btn" />
            </td>
            <td class="auto-style7"></td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        </table>
    </form>

</asp:Content>

