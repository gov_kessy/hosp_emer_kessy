﻿//Patient prescription page
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Pharmacy_CasualityPharmacy_PatPrescriptions : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridView();
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
    }
    public void BindGridView()
    {
        c.getCon();
        int pno = Convert.ToInt32(Request.QueryString["PatNo"].ToString());
        lbl_patno.Text = pno.ToString();
        SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and status=@stat order by presid", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@stat", "Prescribed");
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pres.DataSource = dt;
            gridview_pres.DataBind();
        }
        c.Con.Close();
    }

        protected void gridview_pres_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("DrugPresPage.aspx?DrugNo=" +  gridview_pres.DataKeys[index].Value.ToString());
        }
    
    }
}