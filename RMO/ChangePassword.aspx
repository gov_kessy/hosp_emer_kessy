﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOHomeMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="RMO_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
         <link href="../assets/css/flexslider.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table >
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_title" runat="server" ></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td >Enter Old Password:</td>
                <td>
                    <asp:TextBox ID="txt_old" runat="server" CssClass="twitter" TextMode="Password"></asp:TextBox>
                    <asp:Label ID="lbl_old" runat="server" style="font-size: medium"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_old" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td >&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td >Enter New Password:</td>
                <td>
                    <asp:TextBox ID="txt_new" CssClass="twitter" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="#CC0000" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&amp;])[A-Za-z\d$@$!%*#?&amp;]{8,}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>Retype New Password:</td>
                <td>
                    <asp:TextBox ID="txt_retype" CssClass="twitter" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:Label ID="lbl_retype" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_chg" runat="server" CssClass="btn" OnClick="btn_chg_Click" Text="Change Password" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" CssClass="btn" OnClick="btn_cancel_Click" Text="Cancel"  />
                </td>
            </tr>
           
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
           
        </table>
    <div>
    
    </div>
    </form>

</asp:Content>

