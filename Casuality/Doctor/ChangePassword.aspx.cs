﻿//change password
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Doctor_ChangePassword : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            lbl_title.Text = "Change Password - " + Session["id"];
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        }
    }

    protected void btn_chg_Click(object sender, EventArgs e)
    {
        c.getCon();
        string name = Session["id"].ToString();
        SqlCommand cmd111 = new SqlCommand("select * from UserTable where username= '" + name + "' ", c.Con);
        SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
        DataTable dt111 = new DataTable();
        sda111.Fill(dt111);

        int k = cmd111.ExecuteNonQuery();

        if (dt111.Rows.Count > 0)
        {
            DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];
            string pass = Convert.ToString(row11[2]);
            if (pass == txt_old.Text)
            {


                if (txt_new.Text == txt_retype.Text)
                {


                    String s = "update UserTable set password='" + txt_retype.Text + "' where username='" + name + "'";
                    SqlCommand cmd = new SqlCommand(s, c.Con);
                    cmd.ExecuteNonQuery();
                    Response.Write("<script>alert('Password Changed Successfully');</script>");
                    lbl_retype.Text = " ";
                    lbl_old.Text = " ";
                    c.Con.Close();
                }
                else
                {
                    lbl_retype.Text = "Retyped password not equal to new password";
                }
            }
            else
            {
                lbl_old.Text = "Password Incorrect";
            }
            txt_new.Text = " ";
            txt_old.Text = " ";
            txt_retype.Text = " ";
            lbl_old.Text = " ";
            lbl_retype.Text = " ";
            Response.Redirect("homepagedoctor.aspx");

        }
    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepagedoctor.aspx");
    }
}