﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/CasualityPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="UpdateStock.aspx.cs" Inherits="Pharmacy_CasualityPharmacy_UpdateStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="lbl_med" runat="server" Font-Size="Large" ForeColor="White"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="1000px" AllowPaging="True" AutoGenerateColumns="False" PageSize="50" DataKeyNames="orderid" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand1" style="margin-right: 43px" HorizontalAlign="Center" >
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                     <Columns>  

                        <asp:BoundField DataField="medname" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Medicine Name" >  
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25%"></ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="orderamt" ItemStyle-Width="25%" HeaderText="Order Amount" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >  
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25%"></ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="dateoforder" ItemStyle-Width="25%" HeaderText="Date Of Order" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">  
                    
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25%"></ItemStyle>
                         </asp:BoundField>
                    
                      <asp:ButtonField Text="View" ItemStyle-Width="25%" CommandName="Select"   >

                        
                             
<ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>

                             
                    </Columns>  

                </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

