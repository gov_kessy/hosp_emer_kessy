﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="MessagePage.aspx.cs" Inherits="Casuality_HeadNurse_MessagePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Date &amp; Time</td>
                <td>
                    <asp:Label ID="lbl_date" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>Subject</td>
                <td>
                    <asp:Label ID="lbl_sub" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>Message</td>
                <td>
                    <asp:Label ID="lbl_msg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="Back To Bulletin Board" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>


