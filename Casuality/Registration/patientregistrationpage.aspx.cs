﻿//Register new patient
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class patientregistrationpage : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            ddl_blood.Items.Add("[SELECT]");
            lbl_blood.Visible = false;
            lbl_rel.Visible = false;
                        ddl_blood.Items.Add("Not Sure");
            ddl_blood.Items.Add("A+");
            ddl_blood.Items.Add("A-");
            ddl_blood.Items.Add("B+");
            ddl_blood.Items.Add("B-");
            ddl_blood.Items.Add("O+");
            ddl_blood.Items.Add("O-");
            ddl_blood.Items.Add("AB+");
            ddl_blood.Items.Add("AB-");
        }

    }
        protected void btn_reg_Click1(object sender, EventArgs e)
    {
        c.getCon();
            if (ddl_blood.SelectedIndex != 0)
        {
            if (ddl_rel.SelectedIndex != 0)
            {
        String s = "insert into patientdetails values('" + txt_pname.Text + "','" + txt_pcon.Text + "','" + txt_add1.Text + "','" + txt_add2.Text + "','" + txt_place.Text + "','" + txt_pin.Text + "','" + txt_dob.Text + "','" +Convert.ToDateTime( DateTime.Today )+ "','" +Convert.ToDateTime( DateTime.Today) + "','" + txt_gname.Text + "','" + ddl_rel.SelectedItem.Text + "','" + txt_gcon.Text + "','"+ddl_blood.SelectedItem.Text+"','"+rbl_gen.SelectedItem.Text+"')select @@Identity";
        SqlCommand cmd = new SqlCommand(s, c.Con);
        object o = new object();
        o=cmd.ExecuteScalar();
        int id = Convert.ToInt32(o);
        c.Con.Close();
        Response.Write("<script>alert('Registration successful');</script>");
        txt_add1.Text = " ";
        txt_add2.Text = " ";
        txt_dob.Text = " ";
        txt_gcon.Text = " ";
        txt_gname.Text = " ";
        txt_pcon.Text = " ";
        txt_pin.Text = " ";
        txt_place.Text = " ";
        txt_pname.Text = " ";
        ddl_rel.SelectedIndex = 0;
        ddl_blood.SelectedIndex = 0;
        rbl_gen.SelectedIndex = 0;
        Response.Redirect("printregisteredpatients.aspx?PatientNo=" + Convert.ToInt32(o));
            }
            else
            {
                lbl_rel.Visible = true;
            }
        }
            else
            {
                lbl_blood.Visible = true;
            }
        
        
    }


       
}



   
      
        