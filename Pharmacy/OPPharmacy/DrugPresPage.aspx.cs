﻿//view prescription of each drug
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Pharmacy_OPPharmacy_DrugPresPage : System.Web.UI.Page
{
    conclass c = new conclass();
    int drugno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlabels();
            BindGridView();
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }

    }
    protected void btn_disp_Click(object sender, EventArgs e)
    {
        c.getCon();
        drugno = Convert.ToInt32(Request.QueryString["DrugNo"].ToString());

        SqlCommand cmd = new SqlCommand("select p.patientid from Prescription p  where presid='" + drugno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            //string medid = dt.Rows[0][0].ToString();
            //string stock = dt.Rows[0][1].ToString();
            string pno = dt.Rows[0][0].ToString();
            //int updstock = Convert.ToInt32(stock) - Convert.ToInt32(txt_disp.Text);
            //String s = "update CasualityStockMedicines set stockvalue='" + updstock + "' where medid='" + medid + "'";
            //SqlCommand cmds = new SqlCommand(s, c.Con);
            //cmds.ExecuteNonQuery();
            //String str = "update Prescription set status='Dispensed' where presid='" + drugno + "'";
            //SqlCommand cmd_stat = new SqlCommand(str, c.Con);
            //cmd_stat.ExecuteNonQuery();
            SqlCommand cmd_redirect = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and status=@stat order by presid", c.Con);
            cmd_redirect.Parameters.AddWithValue("@pno", pno);
            cmd_redirect.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_redirect.Parameters.AddWithValue("@stat", "Prescribed");
            DataTable dt_redirect = new DataTable();
            SqlDataAdapter da_redirect = new SqlDataAdapter(cmd_redirect);
            da_redirect.Fill(dt_redirect);


            if (dt_redirect.Rows.Count > 0)
            {
                Response.Redirect("PatPrescriptions.aspx?PatNo=" + pno);
            }
            else
            {
                Response.Redirect("PatientPrescriptions.aspx");
            }
            c.Con.Close();
        }
    }
    public void bindlabels()
    {
        drugno = Convert.ToInt32(Request.QueryString["DrugNo"].ToString());
        c.getCon();
        SqlCommand cmd = new SqlCommand("select pat.pname,m.medname,p.potency,p.dosage,p.frequency,p.no_of_days,p.remarks,p.dateofpres from Prescription p inner join MedMaster m on p.medid=m.MedId inner join patientdetails pat on pat.patientid=p.patientid  where presid='" + drugno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            lbl_patname.Text = dt.Rows[0][0].ToString();
            lbl_drug.Text = dt.Rows[0][1].ToString();
            lbl_pot.Text = dt.Rows[0][2].ToString();
            lbl_dos.Text = dt.Rows[0][3].ToString();
            lbl_freq.Text = dt.Rows[0][4].ToString();
            lbl_instr.Text = dt.Rows[0][6].ToString();
            lbl_noofdays.Text = dt.Rows[0][5].ToString();
            lbl_pres.Text = dt.Rows[0][7].ToString();
            lbl_tot.Text = (Convert.ToInt32(dt.Rows[0][4]) * Convert.ToInt32(dt.Rows[0][5]) * Convert.ToInt32(dt.Rows[0][3])).ToString();
        }
        c.Con.Close();
    }
    public void BindGridView()
    {
        c.getCon();
        drugno = Convert.ToInt32(Request.QueryString["DrugNo"].ToString());

        SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId inner join PharmacyStock s on s.medid=p.medid inner join BatchCasuality b on b.batchid=s.batchid where presid='"+drugno+"' and stockvalue>0", c.Con);
        
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pres.DataSource = dt;
            gridview_pres.DataBind();
        }
        c.Con.Close();
    }

    protected void gridview_pres_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            int id = Convert.ToInt32(gridview_pres.DataKeys[index].Value);
            int stock =Convert.ToInt32(gridview_pres.Rows[index].Cells[1].Text);
            c.getCon();
            drugno = Convert.ToInt32(Request.QueryString["DrugNo"].ToString());

            int updstock = Convert.ToInt32(stock) - Convert.ToInt32(txt_disp.Text);
            String s = "update PharmacyStock set stockvalue='" + updstock + "' where medstockid='" + id + "'";
            SqlCommand cmds = new SqlCommand(s, c.Con);
            cmds.ExecuteNonQuery();
            String str = "update Prescription set status='Dispensed' where presid='" + drugno + "'";
            SqlCommand cmd_stat = new SqlCommand(str, c.Con);
            cmd_stat.ExecuteNonQuery();
            
            c.Con.Close();
            BindGridView();


        }
    }
}