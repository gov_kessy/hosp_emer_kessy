﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Reception/FeverClinicReceptionMaster.master" AutoEventWireup="true" CodeFile="printregisteredpatients.aspx.cs" Inherits="FeverClinic_Reception_printregisteredpatients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <%-- <script language="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=100,toolbar=0,scrollbars=0,status=0,dir=ltr');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            prtContent.innerHTML = strOldOne;
        }
</script>--%>
    
                <link href="style.css" rel="stylesheet" />

    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form id="form1" runat="server">
    <div id="print">
    
    
        <table>
            
            
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            
            
            <tr class="auto-style2">
                <td>Patient Card No</td>
                <td>
                    <asp:Label ID="lbl_pid" runat="server"></asp:Label>
                </td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td>Patient Name</td>
                <td>
                    <asp:Label ID="lbl_name" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td>Date Of&nbsp; Birth</td>
                <td>
                    <asp:Label ID="lbl_dob" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td>Gender</td>
                <td>
                    <asp:Label ID="lbl_gen" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
          
            <tr class="auto-style2">
                <td>Address:</td>
                <td>&nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td class="auto-style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address Line1&nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="lbl_padd1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address Line2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <asp:Label ID="lbl_padd2" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Place</td>
                <td>
                    <asp:Label ID="lbl_pplace" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="auto-style2">
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pin&nbsp;</td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_ppin" runat="server" Text=""></asp:Label>
                </td>
            </tr>
           
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
           
            <tr class="auto-style2">
                <td>Contact Number</td>
                <td>
                    <asp:Label ID="lbl_con1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
           
            <tr class="auto-style2">
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
           
            </table></div>
        <table>
            <tr>
                <td class="auto-style1" >
                    </td>
               
            </tr>
            <tr>
                <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;<asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click" CssClass="btn" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
               
                <td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:button id="BtnPrint" runat="server" onclientclick="javascript:CallPrint('print');" text="Print" xmlns:asp="#unknown"  CssClass="btn" OnClick="BtnPrint_Click" />                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
               
               
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
               
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
               
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
               
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>


