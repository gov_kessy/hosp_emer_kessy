﻿//Casuality home page which has the beds and allocations to it
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class CasualityHome : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
            Bind();
        
    }
    public void Bind()
    {
        c.getCon();
        SqlCommand cmd_bed = new SqlCommand("select * from BedMaster ", c.Con);
        SqlDataAdapter sda_bed = new SqlDataAdapter(cmd_bed);
        DataTable dt_bed = new DataTable();
        sda_bed.Fill(dt_bed);


        int k_bed = cmd_bed.ExecuteNonQuery();

        if (dt_bed.Rows.Count > 0)
        {
            int bid = 0;
            ImageButton[] i = new ImageButton[dt_bed.Rows.Count];
            Label[] l = new Label[dt_bed.Rows.Count];
            for (int j = 0; j < dt_bed.Rows.Count; j++)
            {
                String bname = Convert.ToString(dt_bed.Rows[j][1]);
                bid = Convert.ToInt32(dt_bed.Rows[j][0]);
                i[j] = new ImageButton();
                i[j].AlternateText = bid.ToString();
                l[j] = new Label();
                i[j].Click += (s, args) =>
                {
                    this.GotoPage(s,args);
                };
                i[j].ID = "image_" + j;
                l[j].Text = bname;
                i[j].Height = 200;
                i[j].Width = 200;
                i[j].ImageUrl = "~/Casuality/image/bedgs.jpg";
                if (j % 2 == 0)
                {
                    Form.Controls.Add(new LiteralControl("<br /><br /><br /><br /><br />"));
                 
                }
                Form.Controls.Add(i[j]);
                Form.Controls.Add(l[j]);
                Form.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
            }
            for (int j = 0; j < dt_bed.Rows.Count; j++)
            {
                bid = Convert.ToInt32(dt_bed.Rows[j][0]);
                i[j] = new ImageButton();
                i[j].AlternateText = bid.ToString();
                i[j].Click += (s, args) =>
                {
                    this.GotoPage(s,args);
                };
            }
        SqlCommand cmd_pat = new SqlCommand("select p.patientid,p.pname,p.dob,p.gender,c.bedid from patientdetails p inner join CasualityBedAllocation c on p.patientid=c.patientid where timeout =' ' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            for (int j = 0; j < dt_pat.Rows.Count; j++)
           {

            String name = Convert.ToString(dt_pat.Rows[j][1]);
            String pid = Convert.ToString(dt_pat.Rows[j][0]);
            int id = Convert.ToInt32(dt_pat.Rows[j][4]);
            DateTime dob = Convert.ToDateTime(dt_pat.Rows[j][2]);
            String gender = Convert.ToString(dt_pat.Rows[j][3]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            i[id - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
            //    l[id-1].ForeColor = System.Drawing.ColorTranslator.FromHtml("#0000CC");

            l[id - 1].Text = l[id - 1].Text + " Patient Id- " + pid + " Name- " + name;
            l[id - 1].ForeColor = System.Drawing.Color.Red;
            }
        }

        }


        Form.Controls.Add(new LiteralControl("<br /><br /><br /><br />"));
        
        Page.Controls.Add(new LiteralControl("<br />"));
        Page.Controls.Add(new LiteralControl("<br />"));
       
        c.Con.Close();
    }
    public void GotoPage(object sender,ImageClickEventArgs e)
    {
        ImageButton clickedbutton = sender as ImageButton;

        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select * from CasualityBedAllocation where bedid='" + clickedbutton.AlternateText + "' and timeout =' ' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            Response.Redirect("AllocatedBed.aspx?Bedid=" + clickedbutton.AlternateText);
        }
        
        c.Con.Close();
              

        
    }
   
}