﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
public partial class Superintendent_ScheduleDocs : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Superintendent/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            bind_cas_duty_ddl();
            bind_cop_duty_ddl();
            BindGridView_gridvward();
            bind_cward_duty_ddl();
            BindGridView_gridcward();
            BindGridView_gridccas();
            BindGridView_gridcncd();
            BindGridView_gridvncd();
            BindGridView_gridccop();
            BindGridView_gridcop();
            BindGridView_gridcas();
            lbl_vcas.Visible = false;
            lbl_copno.Visible = false;
            btn_copprint.Visible = false;
            lbl_ncdno.Visible = false;
            btn_ncdprint.Visible = false;
            lbl_wardno.Visible = false;
            btn_wardprint.Visible = false;
            btn_vcasprint.Visible = false;
            bind_cncd_ddl();
            MultiView1.ActiveViewIndex = 8;

        }
    }
    public void bind_cop_duty_ddl()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_cop_doc1.DataSource = dt;
            ddl_cop_doc1.DataValueField = "empid";
            ddl_cop_doc1.DataTextField = "empname";
            ddl_cop_doc1.DataBind();
            ddl_cop_doc1.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();
    }
    public void bind_cncd_ddl()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from NCDEmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_cncddoc.DataSource = dt;
            ddl_cncddoc.DataValueField = "empid";
            ddl_cncddoc.DataTextField = "empname";
            ddl_cncddoc.DataBind();
            ddl_cncddoc.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();
    }
    public void bind_cward_duty_ddl()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {

            ddl_cdoc1.DataSource = dt;
            ddl_cdoc1.DataValueField = "empid";
            ddl_cdoc1.DataTextField = "empname";
            ddl_cdoc1.DataBind();
            ddl_cdoc1.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();
    }
    public void bind_cas_duty_ddl()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from CasualityEmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_ccas_doc1.DataSource = dt;
            ddl_ccas_doc1.DataValueField = "empid";
            ddl_ccas_doc1.DataTextField = "empname";
            ddl_ccas_doc1.DataBind();
            ddl_ccas_doc1.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();
    }
    protected void btn_cascan_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepageRMO.aspx");
    }
    protected void ddl_ccas_duty_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from CasualityEmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' and c.empid != '" + ddl_ccas_doc1.SelectedItem.Value + "'", c.Con);
        //where not exists (select * from CasualityEmployeeDetails ct inner join EmpTypeMaster et on et.emptypeid=ct.emptypeid inner join CasualityDocSchedule st on st.docid=ct.empid where st.dateofsch='" + txt_ccas.Text + "' and et.emptypename='Doctor') and
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_ccas_doc2.DataSource = dt;
            ddl_ccas_doc2.DataValueField = "empid";
            ddl_ccas_doc2.DataTextField = "empname";
            ddl_ccas_doc2.DataBind();
            ddl_ccas_doc2.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();
    }
    protected void btn_casadd_Click(object sender, EventArgs e)
    {
        c.getCon();
        if (ddl_ccas_doc1.SelectedIndex != 0)
        {
            if (ddl_ccas_doc2.SelectedIndex != 0)
            {
                if (ddl_ccas_doc3.SelectedIndex != 0)
                {
                    DateTime day = (Convert.ToDateTime(txt_ccas.Text));
                    SqlCommand cmd = new SqlCommand("select dayid from DayMaster where day='" + day.DayOfWeek + "'", c.Con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);

                    int i = cmd.ExecuteNonQuery();
                    int j = dt.Rows.Count;

                    if (j > 0)
                    {

                        String s = "insert into CasualityDocSchedule values('" + ddl_ccas_doc1.SelectedItem.Value + "','" + ddl_ccas_doc2.SelectedItem.Value + "','" + ddl_ccas_doc3.SelectedItem.Value + "','" + txt_ccas.Text + "','" + dt.Rows[0][0] + "','" + DateTime.Today + "')";
                        SqlCommand cmds = new SqlCommand(s, c.Con);
                        cmds.ExecuteNonQuery();
                    }
                }
                else
                    lbl_ccas_doc3.Visible = true;

            }
            else
                lbl_ccas_doc2.Visible = true;
        }
        else
            lbl_ccas_doc1.Visible = true;
        ddl_ccas_doc3.SelectedIndex = 0;

        ddl_ccas_doc2.SelectedIndex = 0;
        ddl_ccas_doc1.SelectedIndex = 0;
        txt_ccas.Text = " ";
        c.Con.Close();
        BindGridView_gridccas();
    }
    protected void link_ccas_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 4;
    }
    protected void ddl_ccas_doc2_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from CasualityEmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' and c.empid not in ( '" + ddl_ccas_doc1.SelectedItem.Value + "', '" + ddl_ccas_doc2.SelectedItem.Value + "')", c.Con);
        //where not exists (select * from CasualityEmployeeDetails ct inner join EmpTypeMaster et on et.emptypeid=ct.emptypeid inner join CasualityDocSchedule st on st.docid=ct.empid where st.dateofsch='" + txt_ccas.Text + "' and et.emptypename='Doctor') and
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_ccas_doc3.DataSource = dt;
            ddl_ccas_doc3.DataValueField = "empid";
            ddl_ccas_doc3.DataTextField = "empname";
            ddl_ccas_doc3.DataBind();
            ddl_ccas_doc3.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();
    }
    public void BindGridView_gridccas()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name,e3.empname as e3name from CasualityDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join CasualityEmployeeDetails e on s.doc1id=e.empid inner join CasualityEmployeeDetails e2 on s.doc2id=e2.empid inner join CasualityEmployeeDetails e3 on s.doc3id=e3.empid where s.date='" + DateTime.Today + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_ccas.DataSource = dt;
            grid_ccas.DataBind();
        }
        c.Con.Close();

    }
    public void BindGridView_gridcas()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name,e3.empname as e3name from CasualityDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join CasualityEmployeeDetails e on s.doc1id=e.empid inner join CasualityEmployeeDetails e2 on s.doc2id=e2.empid inner join CasualityEmployeeDetails e3 on s.doc3id=e3.empid where  s.dateofsch>='" + txt_casvfrom.Text + "' and s.dateofsch<='" + txt_casvto.Text + "' order by s.dateofsch", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_cas.DataSource = dt;
            grid_cas.DataBind();
            btn_vcasprint.Visible = true;
        }
        else
        {
            lbl_vcas.Visible = true;
            btn_vcasprint.Visible = false;
        }
        c.Con.Close();

    }
    protected void link_cas_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_vcasprint_Click(object sender, EventArgs e)
    {
        Print_Cas_Schedule();
    }
    public void Print_Cas_Schedule()
    {
        Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(2);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            table.SetWidths(new float[] { 0.3f, 0.7f });

            //Company Logo
            cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("Casuality Schedule from " + txt_casvfrom.Text + " to " + txt_casvto.Text + "\n\n", FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)));

            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
            DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            table = new PdfPTable(2);
            table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_RIGHT;
            document.Add(table);

            document.Add(table);
            PdfPTable table1 = new PdfPTable(5);
            table1.AddCell(PhraseCell(new Phrase("Date", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Day", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Morning Casuality", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Afternoon Casuality", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Night Casuality", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 5;
            cell.PaddingBottom = 10f;
            table1.AddCell(cell);
            c.getCon();
            SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name,e3.empname as e3name from CasualityDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join CasualityEmployeeDetails e on s.doc1id=e.empid inner join CasualityEmployeeDetails e2 on s.doc2id=e2.empid inner join CasualityEmployeeDetails e3 on s.doc3id=e3.empid where  s.dateofsch>='" + txt_casvfrom.Text + "' and s.dateofsch<='" + txt_casvto.Text + "' order by s.dateofsch", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][1].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][0].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][2].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][3].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][4].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 5;
                    cell.PaddingBottom = 10f;
                    table1.AddCell(cell);
                }
            }
            document.Add(table1);
            c.Con.Close();
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=CasSchedule.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
    }

    private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2, Color color)
    {
        PdfContentByte contentByte = writer.DirectContent;
        contentByte.SetColorStroke(color);
        contentByte.MoveTo(x1, y1);
        contentByte.LineTo(x2, y2);
        contentByte.Stroke();
    }
    private static PdfPCell PhraseCell(Phrase phrase, int align)
    {
        PdfPCell cell = new PdfPCell(phrase);
        cell.BorderColor = Color.WHITE;
        cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 2f;
        cell.PaddingTop = 0f;
        return cell;
    }
    private static PdfPCell ImageCell(string path, float scale, int align)
    {
        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path));
        image.ScalePercent(scale);
        PdfPCell cell = new PdfPCell(image);
        cell.BorderColor = Color.WHITE;
        cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 0f;
        cell.PaddingTop = 0f;
        return cell;
    }


    protected void btn_vfind_Click(object sender, EventArgs e)
    {
        BindGridView_gridcas();
    }
    protected void btn_copcan_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepageRMO.aspx");

    }
    protected void btn_copadd_Click(object sender, EventArgs e)
    {
        c.getCon();
        if (ddl_cop_doc1.SelectedIndex != 0)
        {
            if (ddl_cop_doc2.SelectedIndex != 0)
            {

                DateTime day = (Convert.ToDateTime(txt_cop.Text));
                SqlCommand cmd = new SqlCommand("select dayid from DayMaster where day='" + day.DayOfWeek + "'", c.Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                int i = cmd.ExecuteNonQuery();
                int j = dt.Rows.Count;

                if (j > 0)
                {

                    String s = "insert into GeneralOPDocSchedule values('" + txt_cop.Text + "','" + ddl_cop_doc1.SelectedItem.Value + "','" + ddl_cop_doc2.SelectedItem.Value + "','" + dt.Rows[0][0] + "','" + DateTime.Today + "')";
                    SqlCommand cmds = new SqlCommand(s, c.Con);
                    cmds.ExecuteNonQuery();
                }

            }
            else
                lbl_cop_doc2.Visible = true;
        }
        else
            lbl_cop_doc1.Visible = true;

        ddl_cop_doc2.SelectedIndex = 0;
        ddl_cop_doc1.SelectedIndex = 0;
        txt_cop.Text = " ";
        c.Con.Close();
        BindGridView_gridccop();

    }
    public void BindGridView_gridccop()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name from GeneralOPDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join EmployeeDetails e on s.doc1id=e.empid inner join EmployeeDetails e2 on s.doc2id=e2.empid where s.date='" + DateTime.Today + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_ccop.DataSource = dt;
            grid_ccop.DataBind();
        }
        c.Con.Close();
    }
    protected void btn_copfind_Click(object sender, EventArgs e)
    {
        BindGridView_gridcop();
    }
    public void BindGridView_gridcop()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name from GeneralOPDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join EmployeeDetails e on s.doc1id=e.empid inner join EmployeeDetails e2 on s.doc2id=e2.empid where  s.dateofsch>='" + txt_copfrom.Text + "' and s.dateofsch<='" + txt_copto.Text + "' order by s.dateofsch", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_cop.DataSource = dt;
            grid_cop.DataBind();
            btn_copprint.Visible = true;
        }
        else
        {
            lbl_copno.Visible = true;
            btn_copprint.Visible = false;
        }
        c.Con.Close();

    }
    protected void btn_copprint_Click(object sender, EventArgs e)
    {

        Print_OP_Schedule();
    }
    public void Print_OP_Schedule()
    {
        Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(2);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            table.SetWidths(new float[] { 0.3f, 0.7f });

            //Company Logo
            cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("General OP Schedule from " + txt_copfrom.Text + " to " + txt_copto.Text + "\n\n", FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)));

            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
            DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            table = new PdfPTable(2);
            table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_RIGHT;
            document.Add(table);

            document.Add(table);
            PdfPTable table1 = new PdfPTable(4);
            table1.AddCell(PhraseCell(new Phrase("Date\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Day\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Doctor 1\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Doctor 2\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 4;
            cell.PaddingBottom = 10f;
            table1.AddCell(cell);
            c.getCon();
            SqlCommand cmd = new SqlCommand("select d.day,CAST(s.dateofsch AS DATE),e.empname as e1name,e2.empname as e2name from GeneralOPDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join EmployeeDetails e on s.doc1id=e.empid inner join EmployeeDetails e2 on s.doc2id=e2.empid  where  s.dateofsch>='" + txt_copfrom.Text + "' and s.dateofsch<='" + txt_copto.Text + "' order by s.dateofsch", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][1].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][0].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][2].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][3].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 5;
                    cell.PaddingBottom = 10f;
                    table1.AddCell(cell);
                }
            }
            document.Add(table1);
            c.Con.Close();
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=GenOPSchedule.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
    }

    protected void link_cgenop_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 5;
    }
    protected void link_genop_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void ddl_cop_doc1_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' and c.empid != '" + ddl_cop_doc1.SelectedItem.Value + "'", c.Con);
        //where not exists (select * from CasualityEmployeeDetails ct inner join EmpTypeMaster et on et.emptypeid=ct.emptypeid inner join CasualityDocSchedule st on st.docid=ct.empid where st.dateofsch='" + txt_ccas.Text + "' and et.emptypename='Doctor') and
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_cop_doc2.DataSource = dt;
            ddl_cop_doc2.DataValueField = "empid";
            ddl_cop_doc2.DataTextField = "empname";
            ddl_cop_doc2.DataBind();
            ddl_cop_doc2.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();

    }
    protected void btn_cwardcan_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepageRMO");
    }
    protected void ddl_cwarddoc1_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails c inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where e.emptypename='Doctor' and c.empid != '" + ddl_cdoc1.SelectedItem.Value + "'", c.Con);
        //where not exists (select * from CasualityEmployeeDetails ct inner join EmpTypeMaster et on et.emptypeid=ct.emptypeid inner join CasualityDocSchedule st on st.docid=ct.empid where st.dateofsch='" + txt_ccas.Text + "' and et.emptypename='Doctor') and
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_cdoc2.DataSource = dt;
            ddl_cdoc2.DataValueField = "empid";
            ddl_cdoc2.DataTextField = "empname";
            ddl_cdoc2.DataBind();
            ddl_cdoc2.Items.Insert(0, "[SELECT]");

        }
        c.Con.Close();

    }
    protected void btn_cwardadd_Click(object sender, EventArgs e)
    {
        c.getCon();
        if (ddl_cdoc1.SelectedIndex != 0)
        {
            if (ddl_cdoc2.SelectedIndex != 0)
            {

                DateTime day = (Convert.ToDateTime(txt_cwarddate.Text));
                SqlCommand cmd = new SqlCommand("select dayid from DayMaster where day='" + day.DayOfWeek + "'", c.Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                int i = cmd.ExecuteNonQuery();
                int j = dt.Rows.Count;

                if (j > 0)
                {

                    String s = "insert into WardDocSchedule values('" + txt_cwarddate.Text + "','" + ddl_cdoc1.SelectedItem.Value + "','" + ddl_cdoc2.SelectedItem.Value + "','" + dt.Rows[0][0] + "','" + DateTime.Today + "')";
                    SqlCommand cmds = new SqlCommand(s, c.Con);
                    cmds.ExecuteNonQuery();
                }

            }
            else
                lbl_cwarddoc2.Visible = true;
        }
        else
            lbl_cwarddoc1.Visible = true;

        ddl_cdoc2.SelectedIndex = 0;
        ddl_cdoc1.SelectedIndex = 0;
        txt_cwarddate.Text = " ";
        c.Con.Close();
        BindGridView_gridcward();

    }
    public void BindGridView_gridcward()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name from WardDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join EmployeeDetails e on s.doc1id=e.empid inner join EmployeeDetails e2 on s.doc2id=e2.empid where s.date='" + DateTime.Today + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_cward.DataSource = dt;
            grid_cward.DataBind();
        }
        c.Con.Close();

    }

    protected void btn_wardprint_Click(object sender, EventArgs e)
    {
        Print_Ward_Schedule();
    }
    public void Print_Ward_Schedule()
    {
        Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(2);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            table.SetWidths(new float[] { 0.3f, 0.7f });

            //Company Logo
            cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("Ward Schedule from " + txt_copfrom.Text + " to " + txt_copto.Text + "\n\n", FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)));

            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
            DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            table = new PdfPTable(2);
            table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_RIGHT;
            document.Add(table);

            document.Add(table);
            PdfPTable table1 = new PdfPTable(4);
            table1.AddCell(PhraseCell(new Phrase("Date\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Day\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Day Ward Doctor\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Night Ward Doctor\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 4;
            cell.PaddingBottom = 10f;
            table1.AddCell(cell);
            c.getCon();
            SqlCommand cmd = new SqlCommand("select d.day,CAST(s.dateofsch AS DATE),e.empname as e1name,e2.empname as e2name from WardDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join EmployeeDetails e on s.doc1id=e.empid inner join EmployeeDetails e2 on s.doc2id=e2.empid  where  s.dateofsch>='" + txt_wardfrom.Text + "' and s.dateofsch<='" + txt_wardto.Text + "' order by s.dateofsch", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][1].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][0].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][2].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][3].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 5;
                    cell.PaddingBottom = 10f;
                    table1.AddCell(cell);
                }
            }
            document.Add(table1);
            c.Con.Close();
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=WardSchedule.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
    }

    protected void btn_wardfind_Click(object sender, EventArgs e)
    {
        BindGridView_gridvward();
    }
    public void BindGridView_gridvward()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname as e1name,e2.empname as e2name from WardDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join EmployeeDetails e on s.doc1id=e.empid inner join EmployeeDetails e2 on s.doc2id=e2.empid where  s.dateofsch>='" + txt_wardfrom.Text + "' and s.dateofsch<='" + txt_wardto.Text + "' order by s.dateofsch", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_vward.DataSource = dt;
            grid_vward.DataBind();
            btn_wardprint.Visible = true;
        }
        else
        {
            lbl_wardno.Visible = true;
            btn_wardprint.Visible = false;
        }
        c.Con.Close();

    }
    protected void link_cward_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 6;
    }
    protected void link_ward_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void link_ncd_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;

    }
    protected void link_cncd_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 7;

    }
    protected void btn_ncdfind_Click(object sender, EventArgs e)
    {
        BindGridView_gridvncd();
    }
    public void BindGridView_gridvncd()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname  from NCDDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join NCDEmployeeDetails e on s.doc1id=e.empid where  s.dateofsch>='" + txt_ncdfrom.Text + "' and s.dateofsch<='" + txt_ncdto.Text + "' order by s.dateofsch", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_vncd.DataSource = dt;
            grid_vncd.DataBind();
            btn_ncdprint.Visible = true;
        }
        else
        {
            lbl_ncdno.Visible = true;
            btn_ncdprint.Visible = false;
        }
        c.Con.Close();

    }

    protected void btn_ncdprint_Click(object sender, EventArgs e)
    {
        Print_NCD_Schedule();
    }
    public void Print_NCD_Schedule()
    {
        Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(2);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            table.SetWidths(new float[] { 0.3f, 0.7f });

            //Company Logo
            cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("NCD Clinic Schedule from " + txt_copfrom.Text + " to " + txt_copto.Text + "\n\n", FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)));

            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
            DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            table = new PdfPTable(2);
            table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_RIGHT;
            document.Add(table);

            document.Add(table);
            PdfPTable table1 = new PdfPTable(3);
            table1.AddCell(PhraseCell(new Phrase("Date\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Day\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Doctor\n\n", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 3;
            cell.PaddingBottom = 10f;
            table1.AddCell(cell);
            c.getCon();
            SqlCommand cmd = new SqlCommand("select d.day,CAST(s.dateofsch AS DATE),e.empname from NCDDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join NCDEmployeeDetails e on s.doc1id=e.empid   where  s.dateofsch>='" + txt_ncdfrom.Text + "' and s.dateofsch<='" + txt_ncdto.Text + "' order by s.dateofsch", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][1].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][0].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][2].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                    cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                    cell.Colspan = 5;
                    cell.PaddingBottom = 10f;
                    table1.AddCell(cell);
                }
            }
            document.Add(table1);
            c.Con.Close();
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=NCDSchedule.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
    }

    protected void btn_cncdcan_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepageRMO");
    }
    protected void btn_cncdadd_Click(object sender, EventArgs e)
    {
        c.getCon();
        if (ddl_cncddoc.SelectedIndex != 0)
        {

            DateTime day = (Convert.ToDateTime(txt_cncddate.Text));
            SqlCommand cmd = new SqlCommand("select dayid from DayMaster where day='" + day.DayOfWeek + "'", c.Con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            int i = cmd.ExecuteNonQuery();
            int j = dt.Rows.Count;

            if (j > 0)
            {

                String s = "insert into NCDDocSchedule values('" + txt_cncddate.Text + "','" + ddl_cncddoc.SelectedItem.Value + "','" + dt.Rows[0][0] + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();
            }


        }
        else
            lbl_cncddoc.Visible = true;

        ddl_cncddoc.SelectedIndex = 0;
        txt_cncddate.Text = " ";
        c.Con.Close();
        BindGridView_gridcncd();

    }
    public void BindGridView_gridcncd()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select d.day,s.dateofsch,e.empname  from NCDDocSchedule s inner join DayMaster d on s.dayid=d.dayid inner join NCDEmployeeDetails e on s.doc1id=e.empid  where s.date='" + DateTime.Today + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grid_cncd.DataSource = dt;
            grid_cncd.DataBind();
        }
        c.Con.Close();

    }

}