﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Superintendent_CreateMessage : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Superintendent/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            bindddltype();
            ddl_dept.Visible = false;
            lbl_type.Visible = false;
        }

    }
    protected void btn_can_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepageSuper.aspx");
    }
    protected void btn_send_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s = "insert into MessageTable values('" + txt_sub.Text + "','" + txt_msg.Text + "','" + DateTime.Now + "')Select @@Identity";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        Object o = cmds.ExecuteScalar();
        int msgid = Convert.ToInt32(o);
        if (ddl_type.SelectedItem.Value == "All")
        {
            SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor','Head Nurse','Nurse','Receptionist') ", c.Con);
            SqlDataAdapter sda_all= new SqlDataAdapter(cmd_all);
            DataTable dt_all = new DataTable();
            sda_all.Fill(dt_all);


            int k_all = cmd_all.ExecuteNonQuery();

            int c_all=dt_all.Rows.Count;
            while (c_all > 0)
            {
                DataRow row_all = dt_all.Rows[c_all - 1];

                String id = Convert.ToString(row_all[0]);
            
                String str = "insert into OPEmployeeMessages values('" + id + "','" +msgid + "','Unread')";
                SqlCommand cmdstr = new SqlCommand(str, c.Con);
                cmdstr.ExecuteNonQuery();
                c_all--;
            }
            SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor','Nurse','Receptionist') ", c.Con);
            SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
            DataTable dt_allncd = new DataTable();
            sda_allncd.Fill(dt_allncd);


            int k_allncd = cmd_allncd.ExecuteNonQuery();

            int c_allncd = dt_allncd.Rows.Count;
            while (c_allncd > 0)
            {
                DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                String id = Convert.ToString(row_allncd[0]);

                String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                SqlCommand cmdstr = new SqlCommand(str, c.Con);
                cmdstr.ExecuteNonQuery();
                c_allncd--;
            }
            SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor','Nurse','Receptionist') ", c.Con);
            SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
            DataTable dt_allcas = new DataTable();
            sda_allcas.Fill(dt_allcas);


            int k_allcas= cmd_allcas.ExecuteNonQuery();

            int c_allcas = dt_allcas.Rows.Count;
            while (c_allcas > 0)
            {
                DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                String id = Convert.ToString(row_allcas[0]);

                String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                SqlCommand cmdstr = new SqlCommand(str, c.Con);
                cmdstr.ExecuteNonQuery();
                c_allcas--;
            }
            
        }
        else if (ddl_type.SelectedItem.Text == "Doctor")
        {
            if (ddl_dept.SelectedItem.Text == "All")
            {
                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
                SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor')", c.Con);
                SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
                DataTable dt_allncd = new DataTable();
                sda_allncd.Fill(dt_allncd);


                int k_allncd = cmd_allncd.ExecuteNonQuery();

                int c_allncd = dt_allncd.Rows.Count;
                while (c_allncd > 0)
                {
                    DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                    String id = Convert.ToString(row_allncd[0]);

                    String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allncd--;
                }
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            }
            else if(ddl_dept.SelectedItem.Text == "OP")
            {
                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
               
            }
            else if (ddl_dept.SelectedItem.Text == "Casuality")
            {
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            }
            else if (ddl_dept.SelectedItem.Text == "NCD")
            {
                SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Doctor')", c.Con);
                SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
                DataTable dt_allncd = new DataTable();
                sda_allncd.Fill(dt_allncd);


                int k_allncd = cmd_allncd.ExecuteNonQuery();

                int c_allncd = dt_allncd.Rows.Count;
                while (c_allncd > 0)
                {
                    DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                    String id = Convert.ToString(row_allncd[0]);

                    String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allncd--;
                }
               
            }
        }
        else if (ddl_type.SelectedItem.Text == "Head Nurse")
        {
            if (ddl_dept.SelectedItem.Text == "All")
            {

                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Head Nurse') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Head Nurse') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "OP")
            {
                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Head Nurse') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "Casuality")
            {
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Head Nurse') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            
            }
           
        }
        else if (ddl_type.SelectedItem.Text == "Nurse")
        {
            if (ddl_dept.SelectedItem.Text == "All")
            {

                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Nurse') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
                SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Nurse')", c.Con);
                SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
                DataTable dt_allncd = new DataTable();
                sda_allncd.Fill(dt_allncd);


                int k_allncd = cmd_allncd.ExecuteNonQuery();

                int c_allncd = dt_allncd.Rows.Count;
                while (c_allncd > 0)
                {
                    DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                    String id = Convert.ToString(row_allncd[0]);

                    String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allncd--;
                }
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Nurse') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "OP")
            {
                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Nurse') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "Casuality")
            {
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Nurse') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            
            
            }
            else if (ddl_dept.SelectedItem.Text == "NCD")
            {
                SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Nurse')", c.Con);
                SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
                DataTable dt_allncd = new DataTable();
                sda_allncd.Fill(dt_allncd);


                int k_allncd = cmd_allncd.ExecuteNonQuery();

                int c_allncd = dt_allncd.Rows.Count;
                while (c_allncd > 0)
                {
                    DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                    String id = Convert.ToString(row_allncd[0]);

                    String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allncd--;
                }
            
            }
        }
        else if (ddl_type.SelectedItem.Text == "Receptionist")
        {
            if (ddl_dept.SelectedItem.Text == "All")
            {

                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Receptionist') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
                SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Receptionist')", c.Con);
                SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
                DataTable dt_allncd = new DataTable();
                sda_allncd.Fill(dt_allncd);


                int k_allncd = cmd_allncd.ExecuteNonQuery();

                int c_allncd = dt_allncd.Rows.Count;
                while (c_allncd > 0)
                {
                    DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                    String id = Convert.ToString(row_allncd[0]);

                    String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allncd--;
                }
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Receptionist') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "OP")
            {
                SqlCommand cmd_all = new SqlCommand("select empid from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Receptionist') ", c.Con);
                SqlDataAdapter sda_all = new SqlDataAdapter(cmd_all);
                DataTable dt_all = new DataTable();
                sda_all.Fill(dt_all);


                int k_all = cmd_all.ExecuteNonQuery();

                int c_all = dt_all.Rows.Count;
                while (c_all > 0)
                {
                    DataRow row_all = dt_all.Rows[c_all - 1];

                    String id = Convert.ToString(row_all[0]);

                    String str = "insert into OPEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_all--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "Casuality")
            {
                SqlCommand cmd_allcas = new SqlCommand("select empid from CasualityEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Receptionist') ", c.Con);
                SqlDataAdapter sda_allcas = new SqlDataAdapter(cmd_allcas);
                DataTable dt_allcas = new DataTable();
                sda_allcas.Fill(dt_allcas);


                int k_allcas = cmd_allcas.ExecuteNonQuery();

                int c_allcas = dt_allcas.Rows.Count;
                while (c_allcas > 0)
                {
                    DataRow row_allcas = dt_allcas.Rows[c_allcas - 1];

                    String id = Convert.ToString(row_allcas[0]);

                    String str = "insert into CasualityEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allcas--;
                }
            
            }
            else if (ddl_dept.SelectedItem.Text == "NCD")
            {
                SqlCommand cmd_allncd = new SqlCommand("select empid from NCDEmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename in('Receptionist')", c.Con);
                SqlDataAdapter sda_allncd = new SqlDataAdapter(cmd_allncd);
                DataTable dt_allncd = new DataTable();
                sda_allncd.Fill(dt_allncd);


                int k_allncd = cmd_allncd.ExecuteNonQuery();

                int c_allncd = dt_allncd.Rows.Count;
                while (c_allncd > 0)
                {
                    DataRow row_allncd = dt_allncd.Rows[c_allncd - 1];

                    String id = Convert.ToString(row_allncd[0]);

                    String str = "insert into NCDEmployeeMessages values('" + id + "','" + msgid + "','Unread')";
                    SqlCommand cmdstr = new SqlCommand(str, c.Con);
                    cmdstr.ExecuteNonQuery();
                    c_allncd--;
                }
            
            }
        }
        c.Con.Close();
        Response.Redirect("homepageSuper.aspx");

    }
    public void bindddltype()
    {
        ddl_type.Items.Insert(0, "[Select]");
        ddl_type.Items.Insert(1, "All");
        ddl_type.Items.Insert(2, "Doctor");
        ddl_type.Items.Insert(3, "Head Nurse");
        ddl_type.Items.Insert(4, "Nurse");
        ddl_type.Items.Insert(5, "Receptionist");


    }
    
    protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_type.SelectedItem.Text == "Doctor" || ddl_type.SelectedItem.Text == "Nurse" || ddl_type.SelectedItem.Text == "Receptionist")
        {
            lbl_type.Visible = true;
            ddl_dept.Visible = true;
            ddl_dept.Items.Insert(0, "[Select]");
            ddl_dept.Items.Insert(1, "All");
            ddl_dept.Items.Insert(2, "OP");
            ddl_dept.Items.Insert(3, "Casuality");
            ddl_dept.Items.Insert(4, "NCD");
        }

        else if (ddl_type.SelectedItem.Text == "Head Nurse")
        {
            lbl_type.Visible = true;
            ddl_dept.Visible = true;
            ddl_dept.Items.Insert(0, "[Select]");
            ddl_dept.Items.Insert(1, "All");
            ddl_dept.Items.Insert(2, "OP");
            ddl_dept.Items.Insert(3, "Casuality");
        }
    }
}