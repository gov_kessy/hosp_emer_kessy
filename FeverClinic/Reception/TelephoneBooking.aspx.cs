﻿//Book an appointment earlier using telephone
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class FeverClinic_Reception_TelephoneBooking : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/FeverClinic/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            //lbl_seldoc.Visible = false;
            //ddl_doc.Items.Insert(0, "[SELECT]");
            //ddl_docbind();

        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmdbk = new SqlCommand("select * from patientdetails where patientid= '" + txt_pid.Text + "' ", c.Con);
        SqlDataAdapter sdabk = new SqlDataAdapter(cmdbk);
        DataTable dtbk = new DataTable();
        sdabk.Fill(dtbk);

        int k = cmdbk.ExecuteNonQuery();

        if (dtbk.Rows.Count > 0)
        {
            DataRow rowbk = dtbk.Rows[dtbk.Rows.Count - 1];
            DateTime todaydate = DateTime.Now;
            string pname = Convert.ToString(rowbk[1]);
            String pdob = Convert.ToString(rowbk[7]);
            DateTime lrdate = Convert.ToDateTime(rowbk[9]);
            int y = todaydate.Year.CompareTo(lrdate.Year);
            if (y >= 1)
            {
                Response.Write("<script>alert('Renewal Needed');</script>");
            }

            txt_pname.Text = pname;
            txt_dob.Text = pdob;
        }
        c.Con.Close();

    }
    //protected void ddl_docbind()
    //{
    //    c.getCon();
    //    SqlCommand cmddept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%'", c.Con);
    //    SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
    //    DataTable dtdept = new DataTable();
    //    sdadept.Fill(dtdept);

    //    int idept = cmddept.ExecuteNonQuery();
    //    int j = dtdept.Rows.Count;
    //    if (j > 0)
    //    {
    //        DataRow row = dtdept.Rows[dtdept.Rows.Count - 1];

    //        int did = Convert.ToInt32(row[0]);
    //        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails  where deptid='" + did + "' order by empname desc", c.Con);
    //        SqlDataAdapter sda = new SqlDataAdapter(cmd);
    //        DataTable dt = new DataTable();
    //        sda.Fill(dt);

    //        int i = cmd.ExecuteNonQuery();
    //        int k = dt.Rows.Count;

    //        if (k > 0)
    //        {
    //            ddl_doc.DataSource = dt;
    //            ddl_doc.DataValueField = "empid";
    //            ddl_doc.DataTextField = "empname";
    //            ddl_doc.DataBind();
    //            ddl_doc.Items.Insert(0, "[SELECT]");
    //            ddl_doc.Items.Insert(1, "No preference");

    //        }
    //    }
    //    c.Con.Close();
    //}
    protected void btn_generate_Click(object sender, EventArgs e)
    {
        int count;
        c.getCon();

        //if (ddl_doc.SelectedIndex != 0)
        //{
        //    if (ddl_doc.SelectedItem.Text == "No preference")
        //        ddl_doc.SelectedItem.Value = "0";
            SqlCommand cmddept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%'", c.Con);
            SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
            DataTable dtdept = new DataTable();
            sdadept.Fill(dtdept);

            int idept = cmddept.ExecuteNonQuery();
            int j = dtdept.Rows.Count;
            if (j > 0)
            {
                DataRow row = dtdept.Rows[dtdept.Rows.Count - 1];

                int did = Convert.ToInt32(row[0]);
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule where scheduledate='" + txtDate.Text + "' and deptid ='" + did + "'", c.Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                int r = cmd.ExecuteNonQuery();

                if (dt.Rows.Count > 0)
                {
                    DataRow ro = dt.Rows[dt.Rows.Count - 1];
                    count = Convert.ToInt32(ro[0]);
                    lbl_token.Text = Convert.ToString(count + 1);
                }
                String str = "insert into BookingTable values('" + txtDate.Text + "','" + DateTime.Today + "', '" + txt_pid.Text + "','" + did + "','0','" + lbl_token.Text + "')";
                SqlCommand cmds = new SqlCommand(str, c.Con);
                cmds.ExecuteNonQuery();

                String s = "insert into Schedule values('" + txt_pid.Text + "','" + txtDate.Text + "','" + DateTime.Now + "','" + did + "','Confirmed','0',' ')";
                SqlCommand cmd2 = new SqlCommand(s, c.Con);
                cmd2.ExecuteNonQuery();
                txt_dob.Text = " ";
                txt_pid.Text = " ";
                txt_pname.Text = " ";
                txtDate.Text = " ";
            }
        //}
        //else
        //{
        //    lbl_seldoc.Visible = true;
        //}

        c.Con.Close();

    }
}