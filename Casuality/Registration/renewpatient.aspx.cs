﻿//Renew patient who has not visited the hospital for more than an year
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Registration_renewpatient : System.Web.UI.Page
{
     conclass c = new conclass();
     protected void Page_Load(object sender, EventArgs e)
     {
         if (!IsPostBack)
         {
             if (Session["Id"] == null)
                 Response.Redirect("~/Casuality/loginpage.aspx");
             else
             {
                 Response.ClearHeaders();
                 Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                 Response.AddHeader("Pragma", "no-cache");
             }
             this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
         }
     }
    protected void btn_refresh_Click(object sender, EventArgs e)
    {
        txt_pno.Text = " ";
        lbl_dob.Text = " ";
        lbl_gname.Text = " ";
        lbl_lrd.Text = " ";
        lbl_pname.Text = " ";
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd111 = new SqlCommand("select * from patientdetails where patientid= '" + txt_pno.Text + "' ", c.Con);
        SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
        DataTable dt111 = new DataTable();
        sda111.Fill(dt111);

        int k = cmd111.ExecuteNonQuery();

        if (dt111.Rows.Count > 0)
        {
            DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];
            string lrdate = Convert.ToString(row11[9]);
            string bdate = Convert.ToString(row11[7]);
            string pname = Convert.ToString(row11[1]);
            string gname = Convert.ToString(row11[10]);
            lbl_dob.Text = bdate;
            lbl_gname.Text = gname;
            lbl_lrd.Text = lrdate;
            lbl_pname.Text = pname;
            
        }
    }
    protected void btn_renew_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s = "update patientdetails set dateoflastrenewal='"+DateTime.Today+"' where patientid='" + txt_pno.Text + "'";
        SqlCommand cmd = new SqlCommand(s, c.Con);
        cmd.ExecuteNonQuery();
         SqlCommand cmd111 = new SqlCommand("select * from patientdetails where patientid= '" + txt_pno.Text + "' ", c.Con);
        SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
        DataTable dt111 = new DataTable();
        sda111.Fill(dt111);

        int k = cmd111.ExecuteNonQuery();

        if (dt111.Rows.Count > 0)
        {
            DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];
            string lrdate = Convert.ToString(row11[9]);
            lbl_lrd.Text = lrdate;
            Response.Write("<script>alert('Patient Renewed Successfully ');</script>");
        }
        c.Con.Close();

    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepagereception.aspx");

    }
}