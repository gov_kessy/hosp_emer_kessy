﻿//Message page for individual message
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Casuality_Nurse_MessagePage : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            bindlabel();
        }
    }
    public void bindlabel()
    {
        int msgid = Convert.ToInt32(Request.QueryString["Id"].ToString());
        c.getCon();
        SqlCommand cmd = new SqlCommand("select m.subject,m.message,m.date from MessageTable m inner Join CasualityEmployeeMessages c on m.msgid=c.msgid where c.receiptid='" + msgid + "'  order by c.receiptid", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            string sub = dt.Rows[0][0].ToString();
            string msg = dt.Rows[0][1].ToString();
            string date = dt.Rows[0][2].ToString();
            lbl_date.Text = date;
            lbl_msg.Text = msg;
            lbl_sub.Text = sub;
        }
        String s = "update CasualityEmployeeMessages set status='Read' where receiptid='" + msgid + "'";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();

        c.Con.Close();
    }
    protected void btn_back_Click(object sender, EventArgs e)
    {
        Response.Redirect("BulletinBoard.aspx");
    }
}