﻿//The injection requests for the patient is viewed and the injection is taken here 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
public partial class Nurse_InjectionPage : System.Web.UI.Page
{ 
    int injecno;
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindLabels();
            BindGrid();
        }
    }
    public void BindGrid()
    {
        injecno = Convert.ToInt32(Request.QueryString["InjreqNo"].ToString());
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select i.dateofinjres,e.empname from CasualityInjectionResult i inner join CasualityEmployeeDetails e on e.empid=i.nurseid where i.injecreqid= '" + injecno + "' order by injectionresid", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);
        int k_pat = cmd_pat.ExecuteNonQuery();
        if (dt_pat.Rows.Count > 0)
        {
            GridView_InjRes.DataSource = dt_pat;
            GridView_InjRes.DataBind();


        }
        c.Con.Close();
    }
    public void BindLabels()
    {
        injecno = Convert.ToInt32(Request.QueryString["InjreqNo"].ToString());
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select p.pname,i.dosage,i.frequency,i.remarks,i.dateofinj,m.injectionname,e.empname,i.status from patientdetails p inner join CasualityInjectionTable i on p.patientid=i.patientid inner join InjectionMaster m on i.injectionid=m.injectionid inner join CasualityEmployeeDetails e on e.empid=i.docid where i.injecreqid= '" + injecno + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);
        int k_pat = cmd_pat.ExecuteNonQuery();
        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];
            String pname = Convert.ToString(row_pat[0]);
            String dosage = Convert.ToString(row_pat[1]);
            String freq = Convert.ToString(row_pat[2]);
            String rem = Convert.ToString(row_pat[3]);
            String inj = Convert.ToString(row_pat[5]);
            String ename = Convert.ToString(row_pat[6]);
            String stat = Convert.ToString(row_pat[7]);
            String date = Convert.ToString(row_pat[4]);
            lbl_dosage.Text = dosage + " mL";
            lbl_injname.Text = inj;
            lbl_patname.Text = pname;
            lbl_reqby.Text = ename;
            lbl_reqdate.Text = date;
            lbl_totaldose.Text = freq;
            lbl_stat.Text = stat;

        }
        SqlCommand cmd_count = new SqlCommand("select count(*) from CasualityInjectionResult where injecreqid= '" + injecno + "' ", c.Con);
        SqlDataAdapter sda_count = new SqlDataAdapter(cmd_count);
        DataTable dt_count = new DataTable();
        sda_count.Fill(dt_count);
        int k_count = cmd_count.ExecuteNonQuery();
        if (dt_count.Rows.Count > 0)
        {
            DataRow row_count = dt_count.Rows[dt_count.Rows.Count - 1];
            int count = Convert.ToInt32(row_count[0]);
            lbl_takenno.Text = count.ToString();
            if (count >= 1)
            {
                SqlCommand cmd = new SqlCommand("select dateofinjres from CasualityInjectionResult where injecreqid= '" + injecno + "' order by injectionresid", c.Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                int k = cmd_count.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[dt.Rows.Count - 1];
                    string ct = Convert.ToString(row[0]);
                    lbl_lastdate.Text = ct;

                }
            }
        }
        c.Con.Close();
    }
    protected void link_dose_Click(object sender, EventArgs e)
    {
        c.getCon();
        injecno = Convert.ToInt32(Request.QueryString["InjreqNo"].ToString());
        String s;
        if (Convert.ToInt32(lbl_totaldose.Text) - 1 == Convert.ToInt32(lbl_takenno.Text))
            s = "update CasualityInjectionTable set status='Completed' where injecreqid='" + injecno + "'";

        else
            s = "update CasualityInjectionTable set status='In Progress' where injecreqid='" + injecno + "'";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();
        String s_in = "insert into CasualityInjectionResult values('" + Session["uid"] + "','" + DateTime.Now + "','" + injecno + "')";
        SqlCommand cmdsin = new SqlCommand(s_in, c.Con);
        cmdsin.ExecuteNonQuery();
        SqlCommand cmd_pat = new SqlCommand("select s.stockvalue,s.batchid,i.injectionid,s.injstockid from CasualityInjectionStock s inner join CasualityInjectionTable i on s.injecid=i.injectionid where injecreqid= '" + injecno + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];
            int inj = Convert.ToInt32(row_pat[3]);
            int batch = Convert.ToInt32(row_pat[1]);

            int stock = Convert.ToInt32(row_pat[0]) - 1;
            String s_inj = "update CasualityInjectionStock set stockvalue='" + stock + "' where injstockid='" + inj + "' and batchid='" + batch + "'";
            SqlCommand cmd_inj = new SqlCommand(s_inj, c.Con);
            cmd_inj.ExecuteNonQuery();

        }
        
        Response.Redirect("CasualityHome.aspx");
        c.Con.Close();

    }

    
}