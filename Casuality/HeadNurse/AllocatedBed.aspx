﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="AllocatedBed.aspx.cs" Inherits="Casuality_HeadNurse_AllocatedBed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
    
    <link href="../../assets/css/flexslider.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td>Patient Id:</td>
                <td class="auto-style4">
                    <asp:Label ID="lbl_patno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Patient Name:</td>
                <td>
                    <asp:Label ID="lbl_pname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Gender:</td>
                <td class="auto-style4">
                    <asp:Label ID="lbl_gen" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Age:</td>
                <td>
                    <asp:Label ID="lbl_age" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Treatment Status:</td>
                <td>
                    <asp:Label ID="lbl_status" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"> <div class="drop">
<ul class="drop_menu">

     <li>
         <asp:LinkButton ID="link_observ" runat="server" OnClick="link_observ_Click">Observations</asp:LinkButton>
     </li>
     <li>
         <asp:LinkButton ID="link_inj" runat="server" OnClick="link_inj_Click">Injections</asp:LinkButton>
     </li>
    <li>
         <asp:LinkButton ID="link_vitals" runat="server" OnClick="link_vit_Click">Vitals</asp:LinkButton>
     </li>
    <li>
        <asp:LinkButton ID="link_dis" runat="server" OnClick="link_dis_Click">Discharge</asp:LinkButton>
     </li>
</ul>
</div>
</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="mulview_headnurse" runat="server">
                        <table class="auto-style1">
                            <tr>
                                <td>
                                    <asp:View ID="view_observations" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style2">&nbsp;</td>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">Particulars:</td>
                                                <td class="auto-style2">
                                                    <asp:TextBox ID="txt_obspart" runat="server" CssClass="twitter"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">&nbsp;</td>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">Observation:</td>
                                                <td class="auto-style2">
                                                    <asp:TextBox ID="txt_observ" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">&nbsp;</td>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">Remarks:</td>
                                                <td class="auto-style2">
                                                    <asp:TextBox ID="txt_rem" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style3">&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">
                                                    <asp:Button ID="btn_add" cssclass="btn" runat="server" Text="Add" OnClick="btn_add_Click"  />
                                                </td>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">&nbsp;</td>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_injections" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_norec" runat="server" ForeColor="Red" Text="No records found"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridView_InjReq" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="injecreqid" OnPageIndexChanging="GridView_InjReq_PageIndexChanging" OnRowCommand="GridView_InjReq_RowCommand" Width="800px" HorizontalAlign="Center">
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                        <RowStyle ForeColor="#000066" />
                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                        <Columns>
                                                            <asp:BoundField DataField="injecreqid" HeaderStyle-HorizontalAlign="Center" HeaderText="Request Id" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="injectionname" HeaderStyle-HorizontalAlign="Center" HeaderText="Particulars " ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="pname" HeaderStyle-HorizontalAlign="Center" HeaderText="Patient Name" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="dateofinj" HeaderStyle-HorizontalAlign="Center" HeaderText="Requested date" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="empname" HeaderStyle-HorizontalAlign="Center" HeaderText="Requested By" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:ButtonField CommandName="Select" ItemStyle-Width="30" Text="View">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_discharge" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">Are you sure you want to discharge this patient?</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btn_dis" runat="server" CssClass="btn" OnClick="btn_dis_Click" Text="Discharge" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_vitals" runat="server">
                                        <table>
                                             <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
                                             <tr>
                                                 <td class="auto-style1">Particulars</td>
                                                 <td>
                                                     <asp:DropDownList ID="ddl_vital" runat="server">
                                                     </asp:DropDownList>
                                                 </td>
                                                 <td>&nbsp;</td>
                                             </tr>
                                             <tr>
                                                 <td class="auto-style1">&nbsp;</td>
                                                 <td>&nbsp;</td>
                                                 <td>&nbsp;</td>
                                             </tr>
        <tr>
            <td class="auto-style1">Value</td>
            <td>
                <asp:TextBox ID="txt_value" runat="server" CssClass="twitter"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
                                             <tr>
                                                 <td class="auto-style1">&nbsp;</td>
                                                 <td>&nbsp;</td>
                                                 <td>&nbsp;</td>
                                             </tr>
        <tr>
            <td class="auto-style1">Remarks</td>
            <td>
                <asp:TextBox ID="txt_remarks" runat="server" Height="45px" TextMode="MultiLine" Width="264px" CssClass="twitter"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_addvit" runat="server"  OnClick="btn_addvit_Click" Text="Add" CssClass="btn" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="3">
                <asp:GridView ID="GridView_Vitals" AutoGenerateColumns="False" runat="server" CellPadding="3" AllowPaging="True" OnPageIndexChanging="GridView_Vitals_PageIndexChanging" PageSize="50" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" HorizontalAlign="Center">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                     <Columns><asp:BoundField DataField="vital" ItemStyle-HorizontalAlign="Center" HeaderText="Particulars" HeaderStyle-HorizontalAlign="Center">
                                 <HeaderStyle HorizontalAlign="Center" />
                         <ItemStyle HorizontalAlign="Center" />
                         </asp:BoundField>
                                 <asp:BoundField DataField="value" HeaderText="Value" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                                 <HeaderStyle HorizontalAlign="Center" />
                         <ItemStyle HorizontalAlign="Center" />
                         </asp:BoundField>
                                 <asp:BoundField DataField="remarks" HeaderText="Remarks" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                                
                               <HeaderStyle HorizontalAlign="Center" />
                         <ItemStyle HorizontalAlign="Center" />
                         </asp:BoundField>
                                
                               </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="3">
                &nbsp;</td>
        </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
            </tr>
            </table>
    </form>
</asp:Content>

