﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Reception/FeverClinicReceptionMaster.master" AutoEventWireup="true" CodeFile="renewpatient.aspx.cs" Inherits="FeverClinic_Reception_renewpatient" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
                    <link href="style.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">Enter Patient No:</td>
                <td>
                    <asp:TextBox ID="txt_pno" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Search" CssClass="btn" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pno" ErrorMessage="*Enter Patient Card no" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td>Patient Name:<td>
                    <asp:Label ID="lbl_pname" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td>&nbsp;<td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td class="auto-style2">Date of Birth:</td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_dob" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td class="auto-style3">Date of Last Renewal:</td>
                <td class="auto-style3">
                    <asp:Label ID="lbl_lrd" runat="server"></asp:Label>
                </td>
                <td class="auto-style3">
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style3">
                    &nbsp;</td>
                <td class="auto-style3">
                    &nbsp;</td>
            </tr>
            <tr class="auto-style4">
                <td>Guardian Name:</td>
                <td>
                    <asp:Label ID="lbl_gname" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="btn_renew" runat="server" OnClick="btn_renew_Click" Text="Renew" CssClass="btn" />
                </td>
                <td class="auto-style3">
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel"  OnClick="btn_cancel_Click" CssClass="btn" /></td>
                <td class="auto-style3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</asp:Content>

