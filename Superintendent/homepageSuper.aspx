﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Superintendent/SuperintendentHomeMaster.master" AutoEventWireup="true" CodeFile="homepageSuper.aspx.cs" Inherits="Superintendent_homepageSuper" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        </style>
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
    <link href="style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                 
    <div>
         <section class= "featured-service-content">
			<div class="container">
				<div class="row">
					
                    <ul id="sti-menu" class="sti-menu">
	        		<li data-hovercolor="#fff">
					<a href="CreateMessage.aspx">
						<h4 data-type="mText" class="sti-item">Create Messages</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-inbox sti-item"></span>
						<span data-type="icon" class="gly"></span>

					</a>
				</li>
                        <li data-hovercolor="#fff">
					<a href="ChangeUsername.aspx">
						<h4 data-type="mText" class="sti-item">Change Username</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-user sti-item"></span>
					
                    </a>
				</li>
                        <li data-hovercolor="#fff">
					<a href="ChangePassword.aspx">
						<h4 data-type="mText" class="sti-item">Change Password</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-edit sti-item"></span>
					</a>
				</li>
                         <li data-hovercolor="#fff">
					<a href="logout.aspx">
						<h4 data-type="mText" class="sti-item">Logout</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-lock sti-item"></span>

					</a>
				</li>
						</ul>				
                    
	        		</div>
			</div>
		</section>
               
    
    </div>
   
</asp:Content>

