﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/AdminMaster.master" AutoEventWireup="true" CodeFile="employeeregistration.aspx.cs" Inherits="employeeregistration" %>

 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <title></title>
   
        <link href="../assets/css/flexslider.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table id="Table1" class="auto-style1" runat="server">

             

             <tr>
                <td ><ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
                <td class="auto-style12" >&nbsp;</td>
            </tr>

             <tr>
                <td class="auto-style15" >&nbsp;</td>
                <td class="auto-style12" >&nbsp;</td>
            </tr>

            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style13">&nbsp;</td>
            </tr>
            
            <tr>
                <td class="auto-style15">&nbsp;</td>
                <td class="auto-style12">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_name" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="*Invalid Name" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gender</td>
                
                <td class="auto-style4">
                    <asp:RadioButtonList ID="rbl_gen" runat="server" AutoPostBack="True" Width="167px" Height="42px" style="font-size: small" CssClass="twitter">
                        <asp:ListItem Selected="True">Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
          
            <tr>
                <td class="auto-style11">&nbsp;</td>
                
                <td class="auto-style4">
                    &nbsp;</td>
            </tr>
          
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date Of Birth(MM/DD/YYYY)</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_dob" runat="server" CssClass="twitter"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txt_dob" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_dob" ErrorMessage="*Enter Date of Birth" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
          
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_email" runat="server" CausesValidation="True" CssClass="twitter"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_email" ErrorMessage="*Invalid Email" ForeColor="#CC0000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                 </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact1</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_contact1" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_contact1" ErrorMessage="*Invalid Phone Number" ForeColor="#CC0000" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact2</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_contact2" runat="server" CssClass="twitter"></asp:TextBox>
            
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txt_contact2" ErrorMessage="*Invalid Phone Number" ForeColor="#CC0000" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Current&nbsp; Address</td>
                <td class="auto-style13">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address Line1</span></td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_cadd1" runat="server" CssClass="twitter"></asp:TextBox>
           
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_cadd1" ErrorMessage="*Invalid Address Line" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address Line2</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_cadd2" runat="server" CssClass="twitter" ></asp:TextBox>
           
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txt_cadd2" ErrorMessage="*Invalid Address Line" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Place&nbsp;</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_cplace" runat="server" CssClass="twitter"></asp:TextBox>
           
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txt_cplace" ErrorMessage="*Invalid Place" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pin&nbsp;</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_cpin" runat="server" CssClass="twitter"></asp:TextBox>
            
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txt_cpin" ErrorMessage="*Invalid Pin" ForeColor="#CC0000" ValidationExpression="\d{6}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Permanent Address</td>
                
                <td class="auto-style12">
&nbsp; 
                    
                    <asp:CheckBoxList ID="cbl_perm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cbl_perm_SelectedIndexChanged" CssClass="twitter">
                        <asp:ListItem>[Check if same as current address]</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address&nbsp;Line1</span></td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_padd1" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Addres&nbsp;Line2</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_padd2" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Place</td>
                <td class="auto-style3">
                    <asp:TextBox ID="txt_pplace" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pin&nbsp;</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txt_ppin" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Employee Type</td>
                <td class="auto-style12">
                    <asp:DropDownList ID="ddl_emptype" runat="server" AutoPostBack="True" CssClass="twitter" ForeColor="Gray" OnSelectedIndexChanged="ddl_emptype_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lbl_type" runat="server" ForeColor="#CC0000" Text="*Select Employee Type"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Designation</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddl_desig" runat="server" AutoPostBack="True" CssClass="twitter" ForeColor="Gray">
                    </asp:DropDownList>
           
                    <asp:Label ID="lbl_desig" runat="server" ForeColor="#CC0000" Text="*Select Designation"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style16">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Image</td>
                <td class="auto-style12">
                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="btn" ForeColor="Gray" />
                </td>
            </tr>
            <tr class="auto-style7">
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style13">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style14"></td>
            </tr>
            <tr>
                <td class="auto-style15">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_submit" runat="server" Text="Submit" OnClick="btn_submit_Click" CssClass="btn"  />
                </td>
                <td class="auto-style12">
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click" CssClass="btn" />
                </td>
            </tr>
           
            <tr>
                <td class="auto-style15">
                    &nbsp;</td>
                <td class="auto-style12">
                    &nbsp;</td>
            </tr>
           
        </table>
        </form>
    <div>
    
    </div>
   
</asp:Content>




