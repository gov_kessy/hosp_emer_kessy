﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Doctor/FeverClinicDocMaster.master" AutoEventWireup="true" CodeFile="MedicalCer.aspx.cs" Inherits="FeverClinic_Doctor_MedicalCer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <center>
        <table>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>            <td> <ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
</tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Doctor Name</td><td><asp:TextBox ID="txt_docname" runat="server" CssClass="twitter" ReadOnly="True"></asp:TextBox></td></tr>
            
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            
            <tr><td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Registered Patient? &nbsp;</td><td class="auto-style1">
                <asp:DropDownList ID="ddl_reg" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_reg_SelectedIndexChanged">
                    <asp:ListItem>[Select]</asp:ListItem>
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:DropDownList>
                &nbsp;</td></tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lbl_pno" runat="server" Text="Enter Patient Card Number"></asp:Label>
                &nbsp;</td><td>
                    <asp:TextBox ID="txt_pno" runat="server" ></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_search" runat="server" CssClass="btn" OnClick="btn_search_Click" Text="Search" />
                    &nbsp;</td></tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Patient Name</td><td>        <asp:TextBox ID="txt_patname" CssClass="twitter" runat="server"></asp:TextBox>
                <asp:Label ID="lbl_gen" runat="server" Text="F/M"></asp:Label>

</td></tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Diagnosis</td><td>        <asp:TextBox ID="txt_diag" CssClass="twitter" runat="server"></asp:TextBox>                    

</td></tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No of days of leave</td><td>        <asp:TextBox ID="txt_days" CssClass="twitter" runat="server"></asp:TextBox>

</td></tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; From Date</td><td><asp:TextBox ID="txt_datefrom" CssClass="twitter" runat="server"></asp:TextBox><ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txt_datefrom" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>                    
</td></tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To Date</td><td>        <asp:TextBox ID="txt_dateto" CssClass="twitter" runat="server"></asp:TextBox> <ajax:CalendarExtender ID="CalendarExtender2" TargetControlID="txt_dateto" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>                   

</td></tr>
            
            <tr><td>&nbsp;</td><td>        &nbsp;</td></tr>
             
<tr><td><asp:Button ID="btnReport" runat="server" Text="Generate" CssClass="btn" OnClick = "GenerateReport" /></td><td></td></tr>
     
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

        </table></center> </form>
</asp:Content>

