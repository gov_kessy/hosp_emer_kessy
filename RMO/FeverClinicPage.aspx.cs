﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class RMO_FeverClinicPage : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            MultiView1.ActiveViewIndex = 0;
            bindddldoc();
            bindddlmon();
            binddurdoc();
            ddl_mondoc.Visible = false;
            lbl_selmon.Visible = false;
            ddl_docyear.Visible = false;
            lbl_selyr.Visible = false;
            if (Session["Id"] == null)
                Response.Redirect("~/RMO/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }

    }
    public void binddurdoc()
    {
        ddl_docdur.Items.Insert(0, "[Select]");
        ddl_docdur.Items.Insert(1, "By Month");
        ddl_docdur.Items.Insert(2, "By Year");
        ddl_docdur.Items.Insert(3, "By Year and Month");

    }
    public void bindddlmon()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from Month", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);


        int k = cmd.ExecuteNonQuery();

        if (dt.Rows.Count > 0)
        {
            ddl_mondoc.DataSource = dt;
            ddl_mondoc.DataTextField = "month";
            ddl_mondoc.DataValueField = "monid";
            ddl_mondoc.DataBind();
        }
        ddl_mondoc.Items.Insert(0, "[Select]");
        c.Con.Close();

    }

    public void bindddldoc()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails d inner join EmpTypeMaster e on e.emptypeid=d.emptypeid where e.emptypename like '%Doctor%'  ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);


        int k = cmd.ExecuteNonQuery();

        if (dt.Rows.Count > 0)
        {
            ddl_doc.DataSource = dt;
            ddl_doc.DataTextField = "empname";
            ddl_doc.DataValueField = "empid";
            ddl_doc.DataBind();
        }
        if (dt.Rows.Count > 0)
        {
            DateTime date = Convert.ToDateTime(dt.Rows[0][5]);
            int year = date.Year;
            int yr = DateTime.Today.Year + 1;
            for (int i = year; i <= yr - 1; i++)
            {
                ddl_docyear.Items.Add("" + i);

            }
        }
        ddl_docyear.Items.Insert(0, "[Select]");
        ddl_doc.Items.Insert(0, "[Select]");
        c.Con.Close();

    }
    protected void link_doc_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }

    public void bindchart()
    {
        c.getCon();
        if (ddl_docdur.SelectedItem.Text == "By Year")
        {
            SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join Department d on s.deptid=d.deptid inner join EmpTypeMaster e on e.emptypeid=d.emptype where s.treatedby='" + ddl_doc.SelectedItem.Value + "' and d.deptname like @dep and e.emptypename like @doc group by Year(scheduledate)", c.Con);
            cmd.Parameters.AddWithValue("@dep","%Fever%");
            cmd.Parameters.AddWithValue("@doc", "%Doctor%");

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
            if (dt.Rows.Count > 0)
            {
                string[] x = new string[dt.Rows.Count];
                decimal[] y = new decimal[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    int count = Convert.ToInt32(dt.Rows[i][0]);
                    int date = Convert.ToInt32(dt.Rows[i][1]);
                    //int year = date.Year;
                    x[i] = Convert.ToString(date);
                    y[i] = count;
                }
                //LineChart1.Series[0].Points.DataBindXY(x, y);
                //lbl_est.Text = amt.ToString();
                LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
                LineChart1.CategoriesAxis = string.Join(",", x);
                LineChart1.ChartTitle = string.Format("{0} ", ddl_doc.SelectedItem.Text);
                if (x.Length > 3)
                {
                    LineChart1.ChartWidth = (x.Length * 75).ToString();
                }
                LineChart1.Visible = true;
            }
        }
        else if (ddl_docdur.SelectedItem.Text == "By Month")
        {
            SqlCommand cmd = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join Department d on s.deptid=d.deptid inner join EmpTypeMaster e on e.emptypeid=d.emptype where s.treatedby='" + ddl_doc.SelectedItem.Value + "' and d.deptname like @dep and e.emptypename like @doc and Year(scheduledate)=@date group by datepart(mm,scheduledate)", c.Con);
            cmd.Parameters.AddWithValue("@dep", "%Fever%");
            cmd.Parameters.AddWithValue("@date", DateTime.Today.Year);
            cmd.Parameters.AddWithValue("@doc", "%Doctor%");

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
            if (dt.Rows.Count > 0)
            {
                string[] x = new string[dt.Rows.Count];
                decimal[] y = new decimal[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    int count = Convert.ToInt32(dt.Rows[i][0]);
                    int date = Convert.ToInt32(dt.Rows[i][1]);
                    SqlCommand cmdsm = new SqlCommand("select month from Month s where s.monid='" + date + "'", c.Con);
                    DataTable dtsm = new DataTable();
                    SqlDataAdapter dasm = new SqlDataAdapter(cmdsm);
                    dasm.Fill(dtsm);

                    cmdsm.ExecuteNonQuery();
                    if (dtsm.Rows.Count > 0)
                    {
                        String mon = Convert.ToString(dtsm.Rows[0][0]);


                        //int year = date.Year;
                        x[i] = Convert.ToString(mon);
                        y[i] = count;
                    }
                }
                //LineChart1.Series[0].Points.DataBindXY(x, y);
                //lbl_est.Text = amt.ToString();
                LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
                LineChart1.CategoriesAxis = string.Join(",", x);
                LineChart1.ChartTitle = string.Format("{0} ", ddl_doc.SelectedItem.Text);
                if (x.Length > 3)
                {
                    LineChart1.ChartWidth = (x.Length * 75).ToString();
                }
                LineChart1.Visible = true;
            }
            else
                Response.Write("<alert>Select A duration</alert>");
        }

        else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
        {
            SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join Department d on s.deptid=d.deptid inner join EmpTypeMaster e on e.emptypeid=d.emptype where s.treatedby='" + ddl_doc.SelectedItem.Value + "' and d.deptname like @dep and e.emptypename like @doc and datepart(mm,scheduledate)=@date group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
            cmd.Parameters.AddWithValue("@dep", "%Fever%");
            cmd.Parameters.AddWithValue("@date", ddl_mondoc.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@doc", "%Doctor%");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
            if (dt.Rows.Count > 0)
            {
                string[] x = new string[dt.Rows.Count];
                decimal[] y = new decimal[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    int count = Convert.ToInt32(dt.Rows[i][0]);
                    string date = Convert.ToString(dt.Rows[i][1]);

                    x[i] = Convert.ToString(date + ' ' + ddl_mondoc.SelectedItem.Text);
                    y[i] = count;
                }

                //LineChart1.Series[0].Points.DataBindXY(x, y);
                //lbl_est.Text = amt.ToString();
                LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
                LineChart1.CategoriesAxis = string.Join(",", x);
                LineChart1.ChartTitle = string.Format("{0} ", ddl_doc.SelectedItem.Text);
                if (x.Length > 3)
                {
                    LineChart1.ChartWidth = (x.Length * 75).ToString();
                }
                LineChart1.Visible = true;
            }
        }
        c.Con.Close();
    }
    public void binddeptchart()
    {

        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);

            if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s where s.deptid='" + deptid + "' group by Year(scheduledate)", c.Con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(dt.Rows[i][1]);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart2.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
                    LineChart2.CategoriesAxis = string.Join(",", x);
                    LineChart2.ChartTitle = string.Format("Fever Clinic");
                    if (x.Length > 3)
                    {
                        LineChart2.ChartWidth = (x.Length * 75).ToString();
                    }
                    LineChart2.Visible = true;
                }
            }
        
            else if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s where s.deptid='" + deptid + "' and Year(scheduledate)='" + DateTime.Today.Year + "' group by datepart(mm,scheduledate)", c.Con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        SqlCommand cmdsm = new SqlCommand("select month from Month s where s.monid='" + date + "'", c.Con);
                        DataTable dtsm = new DataTable();
                        SqlDataAdapter dasm = new SqlDataAdapter(cmdsm);
                        dasm.Fill(dtsm);

                        cmdsm.ExecuteNonQuery();
                        if (dtsm.Rows.Count > 0)
                        {
                            String mon = Convert.ToString(dtsm.Rows[0][0]);


                            //int year = date.Year;
                            x[i] = Convert.ToString(mon);
                            y[i] = count;
                        }
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart2.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
                    LineChart2.CategoriesAxis = string.Join(",", x);
                    LineChart2.ChartTitle = string.Format("Fever Clinic");
                    if (x.Length > 3)
                    {
                        LineChart2.ChartWidth = (x.Length * 75).ToString();
                    }
                    LineChart2.Visible = true;
                }
            }

            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s where s.deptid='" + deptid + "' and datepart(mm,scheduledate)='" + ddl_mondoc.SelectedItem.Value + "' group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        string date = Convert.ToString(dt.Rows[i][1]);

                        x[i] = Convert.ToString(date + ' ' + ddl_mondoc.SelectedItem.Text);
                        y[i] = count;
                    }

                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart2.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
                    LineChart2.CategoriesAxis = string.Join(",", x);
                    LineChart2.ChartTitle = string.Format("{0} ", ddl_doc.SelectedItem.Text);
                    if (x.Length > 3)
                    {
                        LineChart2.ChartWidth = (x.Length * 85).ToString();
                    }
                    LineChart2.Visible = true;
                }
            }
        }
        c.Con.Close();

    }
    protected void link_dept_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;

    }
    protected void link_age_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 4;

    }
    protected void link_gen_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
    }
    protected void ddl_docdur_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddl_docdur.SelectedItem.Text == "By Month")
        {
            ddl_mondoc.Visible = true;
            lbl_selmon.Visible = true;
        }
        else if (ddl_docdur.SelectedItem.Text == "By Year")
        {
            ddl_docyear.Visible = true;
            lbl_selyr.Visible = true;
        }
        else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
        {
            ddl_mondoc.Visible = true;
            lbl_selmon.Visible = true;
            ddl_docyear.Visible = true;
            lbl_selyr.Visible = true;
        }

    }
    protected void link_dispdoc_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Show();
    }

    protected void link_dispdept_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();

    }

    protected void link_viewdoc_Click(object sender, EventArgs e)
    {
        lbl_pat.Visible = true;
        c.getCon();
        if (ddl_docdur.SelectedItem.Text == "By Month")
        {
            SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join Department d on s.deptid=d.deptid inner join EmpTypeMaster e on e.emptypeid=d.emptype where s.treatedby='" + ddl_doc.SelectedItem.Value + "' and d.deptname like @dep and e.emptypename like @doc and Year(scheduledate)=@date and datepart(mm,scheduledate)=@mon", c.Con);
            cmd.Parameters.AddWithValue("@dep", "%Fever%");
            cmd.Parameters.AddWithValue("@date", DateTime.Today.Year);
            cmd.Parameters.AddWithValue("@doc", "%Doctor%");
            cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
            if (dt.Rows.Count > 0)
            {
                int count = Convert.ToInt32(dt.Rows[0][0]);
                lbl_pat.Text = Convert.ToString(count);
            }
            }
        else if (ddl_docdur.SelectedItem.Text == "By Year")
        {
            SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join Department d on s.deptid=d.deptid inner join EmpTypeMaster e on e.emptypeid=d.emptype where s.treatedby='" + ddl_doc.SelectedItem.Value + "' and d.deptname like @dep and e.emptypename like @doc and Year(scheduledate)=@date", c.Con);
            cmd.Parameters.AddWithValue("@dep", "%Fever%");
            cmd.Parameters.AddWithValue("@date", ddl_docyear.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@doc", "%Doctor%");
           DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
            if (dt.Rows.Count > 0)
            {
                int count = Convert.ToInt32(dt.Rows[0][0]);
                lbl_pat.Text = Convert.ToString(count);
            }
            
        }
        else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
        {
            SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join Department d on s.deptid=d.deptid inner join EmpTypeMaster e on e.emptypeid=d.emptype where s.treatedby='" + ddl_doc.SelectedItem.Value + "' and d.deptname like @dep and e.emptypename like @doc and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon", c.Con);
            cmd.Parameters.AddWithValue("@dep", "%Fever%");
           cmd.Parameters.AddWithValue("@doc", "%Doctor%");
            cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
            if (dt.Rows.Count > 0)
            {
                int count = Convert.ToInt32(dt.Rows[0][0]);
                lbl_pat.Text = Convert.ToString(count);
            }
           }


        c.Con.Close();
        bindchart();
        ddl_docdur.SelectedIndex = 0;
        ddl_doc.SelectedIndex = 0;
        ddl_mondoc.Visible = false;
        lbl_selmon.Visible = false;
        ddl_docyear.Visible = false;
        lbl_selyr.Visible = false;
        ddl_docyear.SelectedIndex = 0;
        ddl_mondoc.SelectedIndex = 0;


    }
    protected void link_deptview_Click(object sender, EventArgs e)
    {
        binddeptchart();

        lbl_patdept.Visible = true;
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_patdept.Text = Convert.ToString(count);
                }
               }
            else if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s where s.deptid='" + deptid + "' and Year(scheduledate)=@year", c.Con);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_patdept.Text = Convert.ToString(count);
                }
               }
            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_patdept.Text = Convert.ToString(count);
                }
               
            }
        }

        c.Con.Close();
        ddl_docdur.SelectedIndex = 0;
        ddl_doc.SelectedIndex = 0;
        ddl_mondoc.Visible = false;
        lbl_selmon.Visible = false;
        ddl_docyear.Visible = false;
        lbl_selyr.Visible = false;
        ddl_docyear.SelectedIndex = 0;
        ddl_mondoc.SelectedIndex = 0;


    }
    protected void link_genview_Click(object sender, EventArgs e)
    {
        bindgenchart();
        lbl_male.Visible = true;
        lbl_female.Visible = true;
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and p.gender=@gen", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);
                cmd.Parameters.AddWithValue("@gen", "Female");

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_female.Text = Convert.ToString(count);
                }
                SqlCommand cmds = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and p.gender=@gen", c.Con);
                cmds.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds.Parameters.AddWithValue("@year", DateTime.Today.Year);
                cmds.Parameters.AddWithValue("@gen", "Male");

                DataTable dts = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);

                cmds.ExecuteNonQuery();
                if (dts.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts.Rows[0][0]);
                    lbl_male.Text = Convert.ToString(counts);
                }
            }
            else if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and p.gender=@gen", c.Con);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@gen", "Male");

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_male.Text = Convert.ToString(count);
                }
                SqlCommand cmds = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and p.gender=@gen", c.Con);
                cmds.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                cmds.Parameters.AddWithValue("@gen", "Female");

                DataTable dts = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);

                cmds.ExecuteNonQuery();
                if (dts.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts.Rows[0][0]);
                    lbl_female.Text = Convert.ToString(counts);
                }
            }
            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and p.gender=@gen", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@gen", "Male");

                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_male.Text = Convert.ToString(count);
                }
                SqlCommand cmds = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and p.gender=@gen", c.Con);
                cmds.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds.Parameters.AddWithValue("@gen", "Female");

                cmds.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dts = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);

                cmds.ExecuteNonQuery();
                if (dts.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts.Rows[0][0]);
                    lbl_female.Text = Convert.ToString(counts);
                }
            }
        }

        c.Con.Close();
        ddl_docdur.SelectedIndex = 0;
        ddl_doc.SelectedIndex = 0;
        ddl_mondoc.Visible = false;
        lbl_selmon.Visible = false;
        ddl_docyear.Visible = false;
        lbl_selyr.Visible = false;
        ddl_docyear.SelectedIndex = 0;
        ddl_mondoc.SelectedIndex = 0;


    }
    public void bindgenchart()
    {


        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and p.gender='Male' group by Year(scheduledate)", c.Con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(date);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart3.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Male", Data = y });
                    LineChart3.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart3.ChartWidth = (x.Length * 75).ToString();
                    }
                }
                SqlCommand cmds2 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and p.gender='Female' group by Year(scheduledate)", c.Con);
                DataTable dts2 = new DataTable();
                SqlDataAdapter das2 = new SqlDataAdapter(cmds2);
                das2.Fill(dts2);

                cmds2.ExecuteNonQuery();
                if (dts2.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts2.Rows.Count];
                    for (int i = 0; i < dts2.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts2.Rows[i][0]);
                        int date = Convert.ToInt32(dts2.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart3.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Female", Data = y });
                    //LineChart3.CategoriesAxis = string.Join(",", xs2);
                    LineChart3.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart3.Visible = true;
                }
            }
            else if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and p.gender=@gen and Year(scheduledate)=@year group by datepart(mm,scheduledate)", c.Con);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);
                cmd.Parameters.AddWithValue("@gen", "Male");

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        SqlCommand cmdsm = new SqlCommand("select month from Month s where s.monid='" + date + "'", c.Con);
                        DataTable dtsm = new DataTable();
                        SqlDataAdapter dasm = new SqlDataAdapter(cmdsm);
                        dasm.Fill(dtsm);

                        cmdsm.ExecuteNonQuery();
                        if (dtsm.Rows.Count > 0)
                        {
                            String mon = Convert.ToString(dtsm.Rows[0][0]);


                            //int year = date.Year;
                            x[i] = Convert.ToString(mon);
                            y[i] = count;
                        }
                    }
                    //lbl_est.Text = amt.ToString();
                    LineChart3.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Male", Data = y });
                    LineChart3.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart3.ChartWidth = (x.Length * 75).ToString();
                    }
                }
                SqlCommand cmds2 = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and p.gender=@gen and Year(scheduledate)=@year group by datepart(mm,scheduledate)", c.Con);
                cmds2.Parameters.AddWithValue("@year", DateTime.Today.Year);
                cmds2.Parameters.AddWithValue("@gen", "Female");
                DataTable dts2 = new DataTable();
                SqlDataAdapter das2 = new SqlDataAdapter(cmds2);
                das2.Fill(dts2);

                cmds2.ExecuteNonQuery();
                if (dts2.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts2.Rows.Count];
                    for (int i = 0; i < dts2.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts2.Rows[i][0]);
                        int date = Convert.ToInt32(dts2.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart3.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Female", Data = y });
                    //LineChart3.CategoriesAxis = string.Join(",", xs2);
                    LineChart3.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart3.Visible = true;
                }
            }

            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and p.gender=@gen and datepart(mm,scheduledate)=@mon group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                cmd.Parameters.AddWithValue("@gen", "Male");
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(date + " " + ddl_mondoc.SelectedItem.Text);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart3.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Male", Data = y });
                    LineChart3.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart3.ChartWidth = (x.Length * 85).ToString();
                    }
                }
                SqlCommand cmds2 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and p.gender=@gen and datepart(mm,scheduledate)=@mon group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                cmds2.Parameters.AddWithValue("@gen", "Female");
                cmds2.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                DataTable dts2 = new DataTable();
                SqlDataAdapter das2 = new SqlDataAdapter(cmds2);
                das2.Fill(dts2);

                cmds2.ExecuteNonQuery();
                if (dts2.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts2.Rows.Count];
                    for (int i = 0; i < dts2.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts2.Rows[i][0]);
                        int date = Convert.ToInt32(dts2.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart3.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Female", Data = y });
                    //LineChart3.CategoriesAxis = string.Join(",", xs2);
                    LineChart3.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart3.Visible = true;
                }

            }
        }
        c.Con.Close();
    }
    protected void link_dispgen_Click(object sender, EventArgs e)
    {
        ModalPopup3.Show();
    }
    protected void link_dispage_Click(object sender, EventArgs e)
    {
        ModalPopupExtender4.Show();
    }
    protected void link_ageview_Click(object sender, EventArgs e)
    {
        bindagechart();
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) between 0 and 20 and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon ", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_bet120.Text = Convert.ToString(count);
                }
                SqlCommand cmds = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and Year(scheduledate)-Year(p.dob) between 21 and 40", c.Con);
                cmds.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dts = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);

                cmds.ExecuteNonQuery();
                if (dts.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts.Rows[0][0]);
                    lbl_bet2140.Text = Convert.ToString(counts);
                }
                SqlCommand cmds3 = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and Year(scheduledate)-Year(p.dob) between 41 and 60", c.Con);
                cmds3.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds3.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dts3 = new DataTable();
                SqlDataAdapter das3 = new SqlDataAdapter(cmds3);
                das3.Fill(dts3);

                cmds3.ExecuteNonQuery();
                if (dts3.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts3.Rows[0][0]);
                    lbl_bet4160.Text = Convert.ToString(counts);
                }

                SqlCommand cmds4 = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and Year(scheduledate)-Year(p.dob) > 60", c.Con);
                cmds4.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds4.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dts4 = new DataTable();
                SqlDataAdapter das4 = new SqlDataAdapter(cmds4);
                das4.Fill(dts4);

                cmds4.ExecuteNonQuery();
                if (dts4.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts4.Rows[0][0]);
                    lbl_greater60.Text = Convert.ToString(counts);
                }

            }
            else if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) between 0 and 20 and Year(scheduledate)=@year", c.Con);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@gen", "Male");

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_bet120.Text = Convert.ToString(count);
                }
                SqlCommand cmds = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) between 21 and 40", c.Con);
                cmds.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);

                DataTable dts = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);

                cmds.ExecuteNonQuery();
                if (dts.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts.Rows[0][0]);
                    lbl_bet2140.Text = Convert.ToString(counts);
                }
                SqlCommand cmds3 = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) between 41 and 60", c.Con);
                cmds3.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);

                DataTable dts3 = new DataTable();
                SqlDataAdapter das3 = new SqlDataAdapter(cmds3);
                das3.Fill(dts3);

                cmds3.ExecuteNonQuery();
                if (dts3.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts3.Rows[0][0]);
                    lbl_bet4160.Text = Convert.ToString(counts);
                }
                SqlCommand cmds4 = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid where s.deptid='" + deptid + "' and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) > 60", c.Con);
                cmds4.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);

                DataTable dts4 = new DataTable();
                SqlDataAdapter das4 = new SqlDataAdapter(cmds4);
                das4.Fill(dts4);

                cmds4.ExecuteNonQuery();
                if (dts4.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts4.Rows[0][0]);
                    lbl_greater60.Text = Convert.ToString(counts);
                }
            }
            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) between 0 and 20 and datepart(mm,scheduledate)=@mon", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_bet120.Text = Convert.ToString(count);
                }
                SqlCommand cmds = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and Year(scheduledate)-Year(p.dob) between 21 and 40", c.Con);
                cmds.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dts = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);

                cmds.ExecuteNonQuery();
                if (dts.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts.Rows[0][0]);
                    lbl_bet2140.Text = Convert.ToString(counts);
                }
                SqlCommand cmds3 = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and Year(scheduledate)-Year(p.dob) between 41 and 60", c.Con);
                cmds3.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds3.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dts3 = new DataTable();
                SqlDataAdapter das3 = new SqlDataAdapter(cmds3);
                das3.Fill(dts3);

                cmds3.ExecuteNonQuery();
                if (dts3.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts3.Rows[0][0]);
                    lbl_bet4160.Text = Convert.ToString(counts);
                }

                SqlCommand cmds4 = new SqlCommand("select count(*) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and datepart(mm,scheduledate)=@mon and Year(scheduledate)-Year(p.dob) > 60", c.Con);
                cmds4.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmds4.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dts4 = new DataTable();
                SqlDataAdapter das4 = new SqlDataAdapter(cmds4);
                das4.Fill(dts4);

                cmds4.ExecuteNonQuery();
                if (dts4.Rows.Count > 0)
                {
                    int counts = Convert.ToInt32(dts4.Rows[0][0]);
                    lbl_greater60.Text = Convert.ToString(counts);
                }

            }
        }

        c.Con.Close();
        ddl_docdur.SelectedIndex = 0;
        ddl_doc.SelectedIndex = 0;
        ddl_mondoc.Visible = false;
        lbl_selmon.Visible = false;
        ddl_docyear.Visible = false;
        lbl_selyr.Visible = false;
        ddl_docyear.SelectedIndex = 0;
        ddl_mondoc.SelectedIndex = 0;


    }
    public void bindagechart()
    {


        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) between 0 and 20 group by Year(scheduledate)", c.Con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(date);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 0 and 20", Data = y });
                    LineChart4.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart4.ChartWidth = (x.Length * 75).ToString();
                    }
                }
                SqlCommand cmds2 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) between 21 and 40  group by Year(scheduledate)", c.Con);
                DataTable dts2 = new DataTable();
                SqlDataAdapter das2 = new SqlDataAdapter(cmds2);
                das2.Fill(dts2);

                cmds2.ExecuteNonQuery();
                if (dts2.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts2.Rows.Count];
                    for (int i = 0; i < dts2.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts2.Rows[i][0]);
                        int date = Convert.ToInt32(dts2.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 21 and 40", Data = y });
                }
                SqlCommand cmds3 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) between 41 and 60 group by Year(scheduledate)", c.Con);
                DataTable dts3 = new DataTable();
                SqlDataAdapter das3 = new SqlDataAdapter(cmds3);
                das3.Fill(dts3);

                cmds3.ExecuteNonQuery();
                if (dts3.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts3.Rows.Count];
                    for (int i = 0; i < dts3.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts3.Rows[i][0]);
                        int date = Convert.ToInt32(dts3.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 41 and 60", Data = y });
                    //LineChart3.CategoriesAxis = string.Join(",", xs2);
                }
                SqlCommand cmds4 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) > 60 group by Year(scheduledate)", c.Con);
                DataTable dts4 = new DataTable();
                SqlDataAdapter das4 = new SqlDataAdapter(cmds4);
                das4.Fill(dts4);

                cmds4.ExecuteNonQuery();
                if (dts4.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts4.Rows.Count];
                    for (int i = 0; i < dts4.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts4.Rows[i][0]);
                        int date = Convert.ToInt32(dts4.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Greater than 60", Data = y });
                    //LineChart3.CategoriesAxis = string.Join(",", xs2);

                    LineChart4.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart4.Visible = true;
                }
            }
            else if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "'  and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) between 0 and 20 group by datepart(mm,scheduledate)", c.Con);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        SqlCommand cmdsm = new SqlCommand("select month from Month s where s.monid='" + date + "'", c.Con);
                        DataTable dtsm = new DataTable();
                        SqlDataAdapter dasm = new SqlDataAdapter(cmdsm);
                        dasm.Fill(dtsm);

                        cmdsm.ExecuteNonQuery();
                        if (dtsm.Rows.Count > 0)
                        {
                            String mon = Convert.ToString(dtsm.Rows[0][0]);


                            //int year = date.Year;
                            x[i] = Convert.ToString(mon);
                            y[i] = count;
                        }
                    }
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 0 and 20", Data = y });
                    LineChart4.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart4.ChartWidth = (x.Length * 75).ToString();
                    }
                }
                SqlCommand cmds2 = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "'  and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) between 21 and 40 group by datepart(mm,scheduledate)", c.Con);
                cmds2.Parameters.AddWithValue("@year", DateTime.Today.Year);
                DataTable dts2 = new DataTable();
                SqlDataAdapter das2 = new SqlDataAdapter(cmds2);
                das2.Fill(dts2);

                cmds2.ExecuteNonQuery();
                if (dts2.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts2.Rows.Count];
                    for (int i = 0; i < dts2.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts2.Rows[i][0]);
                        int date = Convert.ToInt32(dts2.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 21 and 40", Data = y });
                }
                SqlCommand cmds3 = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) between 41 and 60 group by datepart(mm,scheduledate)", c.Con);
                cmds3.Parameters.AddWithValue("@year", DateTime.Today.Year);
                DataTable dts3 = new DataTable();
                SqlDataAdapter das3 = new SqlDataAdapter(cmds3);
                das3.Fill(dts3);

                cmds3.ExecuteNonQuery();
                if (dts3.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts3.Rows.Count];
                    for (int i = 0; i < dts3.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts3.Rows[i][0]);
                        int date = Convert.ToInt32(dts3.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 41 and 60", Data = y });
                }
                SqlCommand cmds4 = new SqlCommand("select count(*),datepart(mm,scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)=@year and Year(scheduledate)-Year(p.dob) > 60 group by datepart(mm,scheduledate)", c.Con);
                cmds4.Parameters.AddWithValue("@year", DateTime.Today.Year);
                DataTable dts4 = new DataTable();
                SqlDataAdapter das4 = new SqlDataAdapter(cmds4);
                das4.Fill(dts4);

                cmds4.ExecuteNonQuery();
                if (dts4.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts4.Rows.Count];
                    for (int i = 0; i < dts4.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts4.Rows[i][0]);
                        int date = Convert.ToInt32(dts4.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Greater than 60", Data = y });

                    //LineChart3.CategoriesAxis = string.Join(",", xs2);
                    LineChart4.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart4.Visible = true;
                }
            }

            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "'  and Year(scheduledate)-Year(p.dob) between 0 and 20 and datepart(mm,scheduledate)=@mon group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(date + " " + ddl_mondoc.SelectedItem.Text);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 0 and 20", Data = y });

                    LineChart4.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart4.ChartWidth = (x.Length * 85).ToString();
                    }
                }
                SqlCommand cmds2 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "' and Year(scheduledate)-Year(p.dob) between 21 and 40 and datepart(mm,scheduledate)=@mon group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                cmds2.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                DataTable dts2 = new DataTable();
                SqlDataAdapter das2 = new SqlDataAdapter(cmds2);
                das2.Fill(dts2);

                cmds2.ExecuteNonQuery();
                if (dts2.Rows.Count > 0)
                {
                    decimal[] y = new decimal[dts2.Rows.Count];
                    for (int i = 0; i < dts2.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts2.Rows[i][0]);
                        int date = Convert.ToInt32(dts2.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 21 and 40", Data = y });
                }
                SqlCommand cmds3 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "'  and Year(scheduledate)-Year(p.dob) between 41 and 60 and datepart(mm,scheduledate)=@mon group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                cmds3.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);

                DataTable dts3 = new DataTable();
                SqlDataAdapter das3 = new SqlDataAdapter(cmds3);
                das3.Fill(dts3);

                cmds3.ExecuteNonQuery();
                if (dts3.Rows.Count > 0)
                {
                    string[] x = new string[dts3.Rows.Count];
                    decimal[] y = new decimal[dts3.Rows.Count];
                    for (int i = 0; i < dts3.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts3.Rows[i][0]);
                        int date = Convert.ToInt32(dts3.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Between 41 and 60", Data = y });
                }
                SqlCommand cmds4 = new SqlCommand("select count(*),Year(scheduledate) from Schedule s inner join patientdetails p on p.patientid=s.patientid  where s.deptid='" + deptid + "'  and Year(scheduledate)-Year(p.dob) > 60 and datepart(mm,scheduledate)=@mon group by Year(scheduledate), datepart(mm,scheduledate)", c.Con);
                cmds4.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);

                DataTable dts4 = new DataTable();
                SqlDataAdapter das4 = new SqlDataAdapter(cmds4);
                das4.Fill(dts4);

                cmds4.ExecuteNonQuery();
                if (dts4.Rows.Count > 0)
                {
                    string[] x = new string[dts4.Rows.Count];
                    decimal[] y = new decimal[dts4.Rows.Count];
                    for (int i = 0; i < dts4.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dts4.Rows[i][0]);
                        int date = Convert.ToInt32(dts4.Rows[i][1]);
                        //int year = date.Year;
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart4.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Greater than 60", Data = y });
                    //LineChart3.CategoriesAxis = string.Join(",", xs2);
                    LineChart4.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart4.Visible = true;
                }

            }
        }
        c.Con.Close();
    }
     protected void link_dis_Click(object sender, EventArgs e)
    {

        binddiseasechart();
            c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Diagnosis s  where s.deptid='" + deptid + "' and prodiagnosis like '"+txt_dis.Text.Trim()+"' and Year(dateofdiag)=@year and datepart(mm,dateofdiag)=@mon ", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_dis.Text = Convert.ToString(count);
                }
              
            }
            else if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Diagnosis s where s.deptid='" + deptid + "' and prodiagnosis like '"+txt_dis.Text.Trim()+"' and Year(dateofdiag)=@year", c.Con);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@gen", "Male");

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_dis.Text = Convert.ToString(count);
                }
               
            }
            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from Diagnosis s  where s.deptid='" + deptid + "' and prodiagnosis like '"+txt_dis.Text.Trim()+"' and Year(dateofdiag)=@year  and datepart(mm,dateofdiag)=@mon", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@year", ddl_docyear.SelectedItem.Text);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dt.Rows[0][0]);
                    lbl_dis.Text = Convert.ToString(count);
                }
               
            }
        }

        c.Con.Close();
        ddl_docdur.SelectedIndex = 0;
        ddl_doc.SelectedIndex = 0;
        ddl_mondoc.Visible = false;
        lbl_selmon.Visible = false;
        ddl_docyear.Visible = false;
        lbl_selyr.Visible = false;
        ddl_docyear.SelectedIndex = 0;
        ddl_mondoc.SelectedIndex = 0;


    }
    public void binddiseasechart()
    {


        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like '%Fever%' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);


            if (ddl_docdur.SelectedItem.Text == "By Year")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(dateofdiag) from Diagnosis s where s.deptid='" + deptid + "' and s.prodiagnosis like '" + txt_dis.Text.Trim() + "' group by Year(dateofdiag)", c.Con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(date);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart5.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Disease", Data = y });
                    LineChart5.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart5.ChartWidth = (x.Length * 75).ToString();
                    }
                }

                LineChart5.ChartTitle = string.Format("Fever Clinic Statistics");
                LineChart5.Visible = true;
            }

            else if (ddl_docdur.SelectedItem.Text == "By Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),datepart(mm,dateofdiag) from Diagnosis s where s.deptid='" + deptid + "' and prodiagnosis like '" + txt_dis.Text.Trim() + "'  and Year(dateofdiag)=@year  group by datepart(mm,dateofdiag)", c.Con);
                cmd.Parameters.AddWithValue("@year", DateTime.Today.Year);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        SqlCommand cmdsm = new SqlCommand("select month from Month s where s.monid='" + date + "'", c.Con);
                        DataTable dtsm = new DataTable();
                        SqlDataAdapter dasm = new SqlDataAdapter(cmdsm);
                        dasm.Fill(dtsm);

                        cmdsm.ExecuteNonQuery();
                        if (dtsm.Rows.Count > 0)
                        {
                            String mon = Convert.ToString(dtsm.Rows[0][0]);


                            //int year = date.Year;
                            x[i] = Convert.ToString(mon);
                            y[i] = count;
                        }
                    }
                    //lbl_est.Text = amt.ToString();
                    LineChart5.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Disease", Data = y });
                    LineChart5.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart5.ChartWidth = (x.Length * 75).ToString();
                    }
                }
                LineChart5.ChartTitle = string.Format("Fever Clinic Statistics");
                LineChart5.Visible = true;
            }


            else if (ddl_docdur.SelectedItem.Text == "By Year and Month")
            {
                SqlCommand cmd = new SqlCommand("select count(*),Year(dateofdiag) from Diagnosis s  where s.deptid='" + deptid + "'  and prodiagnosis like '" + txt_dis.Text.Trim() + "' and datepart(mm,dateofdiag)=@mon group by Year(dateofdiag), datepart(mm,dateofdiag)", c.Con);
                cmd.Parameters.AddWithValue("@mon", ddl_mondoc.SelectedItem.Value);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cmd.ExecuteNonQuery();
                if (dt.Rows.Count > 0)
                {
                    string[] x = new string[dt.Rows.Count];
                    decimal[] y = new decimal[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        int count = Convert.ToInt32(dt.Rows[i][0]);
                        int date = Convert.ToInt32(dt.Rows[i][1]);
                        //int year = date.Year;
                        x[i] = Convert.ToString(date + " " + ddl_mondoc.SelectedItem.Text);
                        y[i] = count;
                    }
                    //LineChart1.Series[0].Points.DataBindXY(x, y);
                    //lbl_est.Text = amt.ToString();
                    LineChart5.Series.Add(new AjaxControlToolkit.LineChartSeries { Name = "Disease", Data = y });

                    LineChart5.CategoriesAxis = string.Join(",", x);
                    if (x.Length > 3)
                    {
                        LineChart5.ChartWidth = (x.Length * 85).ToString();
                    }
                    LineChart5.ChartTitle = string.Format("Fever Clinic Statistics");
                    LineChart5.Visible = true;
                }

            }
        }
        c.Con.Close();
    }

    protected void link_disease_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 5;
    }
    protected void link_dischart_Click(object sender, EventArgs e)
    {
        ModalPopupExtender3.Show();
    }
}
