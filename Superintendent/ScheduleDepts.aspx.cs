﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Superintendent_ScheduleDepts : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Superintendent/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            lbl_day.Visible = false;
            lbl_dept.Visible = false;
            GridView_create.Visible = false;
            btn_add.Visible = false;
            ddl_day.Visible = false;
            ddl_dept.Visible = false;
            MultiView1.ActiveViewIndex = 0;
            lbl_no.Visible = false;
                    GridView_Deptview_Bind();
            binddept();
            binddays();

        }
    }
    protected void GridView_Deptview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Deptview.PageIndex = e.NewPageIndex;
        GridView_Deptview_Bind();
    }
    protected void GridView_create_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_create.PageIndex = e.NewPageIndex;
       GridView_create_Bind();
    }
    protected void btn_yes_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s = "update DeptSchedule set todate='"+DateTime.Today+"'  where todate=' '";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();

        c.Con.Close();
        lbl_day.Visible = true;
        lbl_dept.Visible = true;
        GridView_create.Visible = true;
        btn_add.Visible = true;
        lbl_sure.Visible = false;
        btn_yes.Visible = false;

        ddl_day.Visible = true;
        ddl_dept.Visible = true;
    }
    public void GridView_Deptview_Bind()
    {
      
        c.getCon();

        SqlCommand cmd = new SqlCommand("select * from DeptSchMaster d inner join DeptSchedule e on e.schmasterid=d.schmasterid inner join Department m on m.deptid=e.deptid where e.todate=' ' order by m.deptname", c.Con);
        
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //300
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Deptview.DataSource = dt;
            GridView_Deptview.DataBind();
        }
        else
                lbl_no.Visible = true;
            c.Con.Close();
        
    }



    protected void link_view_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void link_create_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;

    }
    public void binddept()
    {
        c.getCon();
         SqlCommand cmd = new SqlCommand("select * from Department d inner join EmpTypeMaster m on d.emptype=m.emptypeid where emptypename like '%Doctor%' ", c.Con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            int i = cmd.ExecuteNonQuery();
            int j = dt.Rows.Count;

            if (j > 0)
            {

                ddl_dept.DataSource = dt;
                ddl_dept.DataValueField = "deptid";
                ddl_dept.DataTextField = "deptname";
                ddl_dept.DataBind();
            }
            ddl_dept.Items.Insert(0, "[Select]");
        
        c.Con.Close();
    }
    public void binddays()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from DeptSchMaster ", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {

            ddl_day.DataSource = dt;
            ddl_day.DataValueField = "schmasterid";
            ddl_day.DataTextField = "day";
            ddl_day.DataBind();
        }
        ddl_day.Items.Insert(0, "[Select]");

        c.Con.Close();
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        String str = "insert into DeptSchedule values('" + ddl_dept.SelectedItem.Value + "','" + ddl_day.SelectedItem.Value+ "','"+DateTime.Today+"',' ')";
        SqlCommand cmdstr = new SqlCommand(str, c.Con);
        cmdstr.ExecuteNonQuery();
                c.Con.Close();
                GridView_create_Bind();
                ddl_day.SelectedIndex = 0;
                ddl_dept.SelectedIndex = 0;
    }
    public void GridView_create_Bind()
    {

        c.getCon();

        SqlCommand cmd = new SqlCommand("select * from DeptSchMaster d inner join DeptSchedule e on e.schmasterid=d.schmasterid inner join Department m on m.deptid=e.deptid where e.todate=' ' order by m.deptname", c.Con);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //300
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_create.DataSource = dt;
            GridView_create.DataBind();
        }
        
        c.Con.Close();

    }


}