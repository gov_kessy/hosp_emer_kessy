﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/Registration/Regmaster.master" AutoEventWireup="true" CodeFile="changeusername.aspx.cs" Inherits="Registration_changeusername" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
   
    <link href="style.css" rel="stylesheet" />
    
    <style type="text/css">
        .auto-style1 {
            height: 28px;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">Enter New UserName</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_usr" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_usr" ErrorMessage="*Invalid Username:length must be between 7 to 10 characters" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z0-9'@&amp;#.\s]{7,10}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">Enter Password</td>
                <td class="auto-style1">
                    <asp:TextBox ID="txt_pass" runat="server" TextMode="Password" CssClass="twitter"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pass" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_usr" runat="server" OnClick="btn_usr_Click" Text="Change Username" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel"  OnClick="btn_cancel_Click" CssClass="btn" /></td>
            </tr>
            
            
            
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            
            
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            
            
        </table>
    <div>
    
    </div>
    </form>

</asp:Content>

