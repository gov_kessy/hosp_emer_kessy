﻿//Allocated bed of patient and view injections and prescription and discharge patient
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Casuality_HeadNurse_AllocatedBed : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            bindlabel();
            bindddl();
            lbl_norec.Visible = false;
                BindGridView();
            if(lbl_status.Text=="Completed")
                mulview_headnurse.ActiveViewIndex = 2;
            else
                mulview_headnurse.ActiveViewIndex = 0;


        }
    }
    public void bindlabel()
    {
        c.getCon();
              int  bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

         SqlCommand cmd_pat = new SqlCommand("select p.patientid,p.pname,p.dob,p.gender,c.status,c.bedid from patientdetails p inner join CasualityBedAllocation c on p.patientid=c.patientid where c.bedid ='"+bid+"' and c.timeout=' ' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            String name = Convert.ToString(dt_pat.Rows[0][1]);
            String pid = Convert.ToString(dt_pat.Rows[0][0]);
            String bedid = Convert.ToString(dt_pat.Rows[0][5]);
            string status = Convert.ToString(dt_pat.Rows[0][4]);
            DateTime dob = Convert.ToDateTime(dt_pat.Rows[0][2]);
            String gender = Convert.ToString(dt_pat.Rows[0][3]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_age.Text = age.ToString();
            lbl_gen.Text = gender;
            lbl_patno.Text = pid;
            lbl_pname.Text = name;
            lbl_status.Text = status;
        }
        c.Con.Close();
    }
    protected void link_inj_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 1;
    }
    private void BindGridView()
    {
        c.getCon();
        //SqlCommand cmd_pat = new SqlCommand("select deptid from Department where deptname in ('Casuality','CASUALITY','casuality') ", c.Con);
        //SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        //DataTable dt_pat = new DataTable();
        //sda_pat.Fill(dt_pat);


        //int k_pat = cmd_pat.ExecuteNonQuery();
        //int j = dt_pat.Rows.Count;
        //if (j > 0)
        //{
        //    DataRow row_pat = dt_pat.Rows[j - 1];

        //    int deptid = Convert.ToInt32(row_pat[0]);
        int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

        SqlCommand cmd = new SqlCommand("select * from CasualityInjectionTable i inner Join InjectionMaster m on i.injectionid=m.injectionid inner join patientdetails p on p.patientid=i.patientid inner join CasualityEmployeeDetails e on i.docid=e.empid where (i.status=@stat and i.patientid=@pno ) or ( i.status=@status and i.patientid=@pno )  order by i.injecreqid", c.Con);
        cmd.Parameters.AddWithValue("@pno", lbl_patno.Text);
        cmd.Parameters.AddWithValue("@bid", bid);
        cmd.Parameters.AddWithValue("@stat", "Requested");
        cmd.Parameters.AddWithValue("@status", "In Progress");
        DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                GridView_InjReq.DataSource = dt;
                GridView_InjReq.DataBind();
            }
            else
                lbl_norec.Visible = true;
        //}
        c.Con.Close();
    }
    protected void GridView_InjReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_InjReq.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    protected void GridView_InjReq_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_InjReq.Rows[index];
            Response.Redirect("InjectionPage.aspx?InjReqNo=" + row.Cells[0].Text);
        }
    }
    protected void link_observ_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 0;
    }
    protected void link_dis_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 2;
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        String pno = lbl_patno.Text;
        int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());
        String s = "insert into CasualityObservations values('" + pno + "','" + bid + "','" + txt_obspart.Text + "','" + txt_observ.Text + "','" + txt_rem.Text + "','" + Session["docid"] + "','"+DateTime.Now+"')";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();

        c.Con.Close();
        txt_obspart.Text = " ";
        txt_observ.Text = " ";
        txt_rem.Text = " ";
    
    }
    protected void btn_dis_Click(object sender, EventArgs e)
    {
        c.getCon();
        int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

        String s = "update CasualityBedAllocation set timeout='" + DateTime.Now + "',nurseid='"+Session["uid"]+"' where bedid=@bid and patientid=@pid and timeout=@date";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.Parameters.AddWithValue("@pid", lbl_patno.Text);
        cmds.Parameters.AddWithValue("@bid", bid);
        cmds.Parameters.AddWithValue("@date", ' ');
        cmds.ExecuteNonQuery();
        c.Con.Close();
        Response.Redirect("CasualityHome.aspx");
    }
    protected void link_vit_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 3;
    }
    protected void btn_addvit_Click(object sender, EventArgs e)
    {
        c.getCon();
        int pno = Convert.ToInt32(lbl_patno.Text);
        //SqlCommand cmd_vid = new SqlCommand("select * from VitalMaster where vital='"+txt_particulars.Text+"' ", c.Con);
        //SqlDataAdapter sda_vid = new SqlDataAdapter(cmd_vid);
        //DataTable dt_vid = new DataTable();
        //sda_vid.Fill(dt_vid);


        //int k_vid = cmd_vid.ExecuteNonQuery();

        //if (dt_vid.Rows.Count > 0)
        //{
        //    DataRow row_vid = dt_vid.Rows[dt_vid.Rows.Count - 1];

        //    int vid = Convert.ToInt32(row_vid[0]);
        String s = "insert into CasualityVitalTable values('" + txt_value.Text + "','" + txt_remarks.Text + "','" + DateTime.Today + "','" + lbl_patno.Text + "','" + Session["uid"] + "','" + ddl_vital.SelectedItem.Value + "')";
            SqlCommand cmds = new SqlCommand(s, c.Con);
            cmds.ExecuteNonQuery();
        //}
        c.Con.Close();
        txt_remarks.Text = " ";
        txt_value.Text = " ";
       // txt_particulars.Text = " ";
        
        BindGridView_Vitals();
    }
    public void bindddl()
    {
        c.getCon();
        SqlCommand cmd_vid = new SqlCommand("select * from VitalMaster ", c.Con);
        SqlDataAdapter sda_vid = new SqlDataAdapter(cmd_vid);
        DataTable dt_vid = new DataTable();
        sda_vid.Fill(dt_vid);


        int k_vid = cmd_vid.ExecuteNonQuery();

        if (dt_vid.Rows.Count > 0)
        {
            ddl_vital.DataSource = dt_vid;
            ddl_vital.DataTextField = "vital";
            ddl_vital.DataValueField = "vitalmasterid";
            ddl_vital.DataBind();
        }
        ddl_vital.Items.Insert(0, "[Select]");
        c.Con.Close();
    }
    public void BindGridView_Vitals()
    {
        c.getCon();
        int pno = Convert.ToInt32(lbl_patno.Text);

        SqlCommand cmd = new SqlCommand("select * from VitalMaster vm inner join CasualityVitalTable v on vm.vitalmasterid=v.vitalmasterid inner join CasualityEmployeeDetails e on e.empid=v.nurseid  where patientid=@pno and nurseid=@nurse and dateofvital=@date order by vitalid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@nurse", Session["uid"]);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Vitals.DataSource = dt;
            GridView_Vitals.DataBind();
        }
        c.Con.Close();

    }
    protected void GridView_Vitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Vitals.PageIndex = e.NewPageIndex;
        BindGridView_Vitals();
    }
    
}