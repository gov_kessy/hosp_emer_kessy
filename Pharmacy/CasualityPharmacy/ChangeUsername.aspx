﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/CasualityPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="ChangeUsername.aspx.cs" Inherits="Pharmacy_CasualityPharmacy_ChangeUsername" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
   
    <style type="text/css">

        .auto-style1 {
            height: 28px;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="lbl_title" runat="server" CssClass="auto-style3"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">Enter New UserName</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_usr" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_usr" ErrorMessage="*Invalid Username:length must be between 7 to 10 characters" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z0-9'@&amp;#.\s]{7,10}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Enter Password</td>
                <td>
                    <asp:TextBox ID="txt_pass" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pass" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_usr" runat="server" OnClick="btn_usr_Click" Text="Change Username"  CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btn_cancel_Click" /></td>
            </tr>
            
            
            
        </table>
    <div>
    
    </div>
    </form>

</asp:Content>

