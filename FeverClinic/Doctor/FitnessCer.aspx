﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Doctor/FeverClinicDocMaster.master" AutoEventWireup="true" CodeFile="FitnessCer.aspx.cs" Inherits="FeverClinic_Doctor_FitnessCer" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <style type="text/css">
        .auto-style1 {
            height: 25px;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table><tr><td>&nbsp;</td><td><asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager></td><td>&nbsp;</td><td>
            <br />
            </td></tr>
            <tr><td class="auto-style1">&nbsp;</td><td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Registered Patient? &nbsp;</td><td class="auto-style1">&nbsp;</td><td class="auto-style1">&nbsp;&nbsp;&nbsp;
                <asp:DropDownList ID="ddl_reg" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_reg_SelectedIndexChanged">
                    <asp:ListItem>[Select]</asp:ListItem>
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:DropDownList>
                &nbsp;</td></tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lbl_pno" runat="server" Text="Enter Patient Card Number"></asp:Label>
                &nbsp;</td><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txt_pno" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_search" runat="server" CssClass="btn" OnClick="btn_search_Click" Text="Search" />
                    &nbsp;</td></tr>
            <tr><td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Doctor Name</td>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp&nbsp;&nbsp<asp:TextBox ID="txt_docfit" runat="server" CssClass="twitter" ReadOnly="True"></asp:TextBox></td></tr>
            <tr><td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td></tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Patient Name</td>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp&nbsp;&nbsp<asp:TextBox ID="txt_patfit" CssClass="twitter" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_gen" runat="server" Text="F/M"></asp:Label>
                </td>

            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>

            </tr>
            <tr><td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Diagnosis</td>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_diagfit" runat="server" CssClass="twitter"></asp:TextBox>                    

</td></tr>
            <tr><td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td></tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Office of Applicant</td>
                <td>&nbsp;</td>
                <td> &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txt_offpat" CssClass="twitter" runat="server"></asp:TextBox>

</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>        &nbsp;</td></tr>
            
<tr><td>&nbsp;</td><td><asp:Button ID="btn_genfitrep" runat="server" Text="Generate" CssClass="btn" OnClick = "genfitrep_click" /></td><td>&nbsp;</td><td></td></tr>
       
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>
