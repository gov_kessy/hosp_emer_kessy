﻿//Message board for receptionist in casuality
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Casuality_Registration_BulletinBoard : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindGridView();

            GridView1.Columns[4].Visible = false;
            link_del.Visible = false;
            link_can.Visible = false;
            GridView1.Columns[0].Visible = false;
        }
    }
    private void BindGridView()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from MessageTable m inner Join CasualityEmployeeMessages c on m.msgid=c.msgid where c.empid='" + Session["uid"] + "'  order by c.receiptid desc", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            lbl_msg.Visible = false;

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            lbl_msg.Visible = true;
        }
        c.Con.Close();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (String.Compare(Convert.ToString(e.Row.Cells[4].Text), "Unread") == 0)
            {
                e.Row.ForeColor = System.Drawing.Color.Blue;
                e.Row.BackColor = System.Drawing.Color.Bisque;


            }
            else
            {
                e.Row.BackColor = System.Drawing.Color.White;
                e.Row.ForeColor = System.Drawing.Color.Black;
            }
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    protected void link_delmes_Click(object sender, EventArgs e)
    {
        link_del.Visible = true;
        link_can.Visible = true;
        link_delmes.Visible = false;
        GridView1.Columns[0].Visible = true;
    }
    protected void link_can_Click(object sender, EventArgs e)
    {
        link_delmes.Visible = true;
        link_del.Visible = false;
        link_can.Visible = false;
        GridView1.Columns[0].Visible = false;

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        c.getCon();
        foreach (GridViewRow gvrow in GridView1.Rows)
        {
            CheckBox chkdelete = (CheckBox)gvrow.FindControl("chkSelect");
            if (chkdelete.Checked)
            {
                int msgid = Convert.ToInt32(GridView1.DataKeys[gvrow.RowIndex].Value);

                SqlCommand cmd = new SqlCommand("delete from CasualityEmployeeMessages where receiptid=" + msgid, c.Con);
                cmd.ExecuteNonQuery();
            }
        }
        c.Con.Close();
        BindGridView();
    
    }
}
