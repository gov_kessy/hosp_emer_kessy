﻿//Patient consultation page
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Doctor_PatientConsult : System.Web.UI.Page
{
    conclass c = new conclass();
    int pno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            lbl_resp30.Visible = false;
            lbl_caspres.Visible = false;
            lbl_casvit.Visible = false;
            lbl_casinj.Visible = false;
            lbl_caslab.Visible = false;
            lbl_casdiag.Visible = false;
            lbl_response.Visible = false;
            lbl_response0.Visible = false;
            lbl_response1.Visible = false;
            lbl_response2.Visible = false;
            lbl_response3.Visible = false;
            lbl_response4.Visible = false;
            lbl_response5.Visible = false;
            lbl_response6.Visible = false;
            lbl_response7.Visible = false;
            lbl_response23.Visible = false;
            lbl_response24.Visible = false;
            lbl_response25.Visible = false;
            Bind_Labtest_ddl();

            lbl_response26.Visible = false;
            c.getCon();
            SqlCommand cmd_pat = new SqlCommand("select deptid from EmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);


            int k_pat = cmd_pat.ExecuteNonQuery();

            if (dt_pat.Rows.Count > 0)
            {
                DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

                int deptid = Convert.ToInt32(row_pat[0]);
                c.Con.Close();
                }

        }
    }
    public void Bind_meds_ddl()
    {
        c.getCon();

        SqlCommand cmd_med = new SqlCommand("select * from MedMaster ", c.Con);
        SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
        DataTable dt_med = new DataTable();
        sda_med.Fill(dt_med);


        int k_med = cmd_med.ExecuteNonQuery();

        if (dt_med.Rows.Count > 0)
        {

            ddl_pres.DataSource = dt_med;
            ddl_pres.DataTextField = "MedName";
            ddl_pres.DataValueField = "MedId";
            ddl_pres.DataBind();
        }
        ddl_pres.Items.Insert(0, "[Select]");
        c.Con.Close();
    }
    public void Bind_inj_ddl()
    {
        c.getCon();

        SqlCommand cmdcat = new SqlCommand("select * from InjectionMaster ", c.Con);
        // cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
        SqlDataAdapter sdacat = new SqlDataAdapter(cmdcat);
        DataTable dtcat = new DataTable();
        sdacat.Fill(dtcat);

        int icat = cmdcat.ExecuteNonQuery();

        if (dtcat.Rows.Count > 0)
        {

            ddl_injname.DataSource = dtcat;
            ddl_injname.DataTextField = "injectionname";
            ddl_injname.DataValueField = "injectionid";
            ddl_injname.DataBind();
        }
        ddl_injname.Items.Insert(0, "[Select]");
        c.Con.Close();
    }
   
    public void GridView_caslab_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);


        SqlCommand cmd = new SqlCommand("select * from CasualityLabResult lr inner join CasualityLabRequest r on lr.labreqid=r.labreqid inner join CasualityEmployeeDetails e on e.empid=r.docid inner join LabSubTestMaster l on lr.labmasterid=l.testmasterid where patientid=@pno order by r.dateofreq desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_CasLab_Result.DataSource = dt;
            GridView_CasLab_Result.DataBind();
        }
        else
            lbl_caslab.Visible = true;
        c.Con.Close();

    }
    public void Gridview_oplab_bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);


        SqlCommand cmd = new SqlCommand("select * from LabResult lr inner join LabRequest r on lr.labreqid=r.labreqid inner join EmployeeDetails e on e.empid=r.docid inner join LabSubTestMaster l on lr.labmasterid=l.testmasterid where patientid=@pno order by r.dateofreq desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_opLab_Result.DataSource = dt;
            GridView_opLab_Result.DataBind();
        }
        else
            lbl_response25.Visible = true;
        c.Con.Close();
    }

    public void GridView_Lab_Result_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from NCDLabResult lr inner join NCDLabRequest r on lr.labreqid=r.labreqid inner join NCDEmployeeDetails e on e.empid=r.docid inner join LabSubTestMaster l on lr.labmasterid=l.testmasterid where patientid=@pno order by r.dateofreq desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Lab_Result.DataSource = dt;
            GridView_Lab_Result.DataBind();
        }
        else
            lbl_response1.Visible = true;
        c.Con.Close();

    }
    //public void Bind_Labtest_ddl()
    //{
    //    c.getCon();

    //    SqlCommand cmd_pat = new SqlCommand("select * from LabTestMaster ", c.Con);
    //    SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
    //    DataTable dt_pat = new DataTable();
    //    sda_pat.Fill(dt_pat);


    //    int k_pat = cmd_pat.ExecuteNonQuery();

    //    if (dt_pat.Rows.Count > 0)
    //    {
    //        ddl_test.DataSource = dt_pat;
    //        ddl_test.DataTextField = "testname";
    //        ddl_test.DataValueField = "testmasterid";
    //        ddl_test.DataBind();
    //    }
    //    c.Con.Close();
    //}


    public void GridView_Lab_Request_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        int id = Convert.ToInt32(Session["docid"]);
        SqlCommand cmd = new SqlCommand("select * from LabRequest r inner join LabTestMaster l on r.testmasterid=l.testmasterid  where patientid=@pno and dateofreq=@date and docid=@docid", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", id);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_lab_request.DataSource = dt;
            GridView_lab_request.DataBind();
        }
        c.Con.Close();

    }

    protected void GridView_opPre_Diag_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_opPre_Diag.PageIndex = e.NewPageIndex;
        GridView_opPre_Diag_Bind();
    }
    public void GridView_opPre_Diag_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select d.symptoms,d.prodiagnosis,d.remarks,e.empname,dp.deptname,CONVERT(VARCHAR(10),d.dateofdiag,101) as dateofdiaganosis from Diagnosis d inner join EmployeeDetails e on e.empid=d.docid inner join Department dp on dp.deptid=e.deptid where patientid=@pno order by d.diagid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", Session["docid"]);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //300
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_opPre_Diag.DataSource = dt;
            GridView_opPre_Diag.DataBind();
        }
        else
            lbl_response26.Visible = true;
        c.Con.Close();

    }
    public void GridView_opPre_Pres_Bind()
    {

        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from Prescription p inner join EmployeeDetails e on e.empid=p.docid inner join MedMaster m on m.MedId=p.medid where patientid=@pno order by p.presid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", Session["docid"]);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_opPre_Pres.DataSource = dt;
            GridView_opPre_Pres.DataBind();
        }
        else
            lbl_response24.Visible = true;
        c.Con.Close();
    }
    public void GridView_vital_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from VitalMaster vm inner join NCDVitalTable v on vm.vitalmasterid=v.vitalmasterid inner join NCDEmployeeDetails e on e.empid=v.nurseid  where patientid=@pno order by vitalid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_vitals.DataSource = dt;
            GridView_vitals.DataBind();
        }
        else
            lbl_response2.Visible = true;
        c.Con.Close();

    }


    public void Gridview_Allergy_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select top 8 * from AllergyHistory p inner join AllergentMaster h on h.allergentid=p.allergentid where patientid=@pno order by p.allergyid desc ", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        int j = dt.Rows.Count;
        if (j > 0)
        {
            GridView_Allergy.DataSource = dt;
            GridView_Allergy.DataBind();


        }
        else
            lbl_response6.Visible = true;

        c.Con.Close();

    }
    public void GridView_Social_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select top 4 * from SubstanceUsageHistory p inner join SubstanceMaster h on h.SubId=p.subid where patientid=@pno order by p.usageid desc ", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Social.DataSource = dt;
            GridView_Social.DataBind();
        }
        else
            lbl_response5.Visible = true;
        c.Con.Close();
    }
    public void GridView_Vaccination_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select top 15 * from ImmunizationHistory p inner join ImmunizationMaster h on h.ImmuId=p.ImmuId where patientid=@pno order by p.ImId desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Vaccination.DataSource = dt;
            GridView_Vaccination.DataBind();
        }
        else
            lbl_response7.Visible = true;
        c.Con.Close();
    }

    public void GridView_Family_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select top 12 * from FamilyHistory p inner join FamilyHistoryMaster h on h.famid=p.famid where patientid=@pno order by p.fhistoryid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Family.DataSource = dt;
            GridView_Family.DataBind();
        }
        else
            lbl_response3.Visible = true;
        c.Con.Close();
    }
    public void gridview_past_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select top 15 * from PastHistory p inner join PastHistoryMaster h on h.phistoryid=p.pmasterid where patientid=@pno order by h.phistoryid desc ", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_past.DataSource = dt;
            gridview_past.DataBind();
        }
        else
            lbl_response4.Visible = true;
        c.Con.Close();
    }
    public void gridview_surgery_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select top 21 * from MedicalProblemMaster p inner join MedicalProblemHistory h on h.medicalid=p.MedicalId where patientid=@pno order by medpblmid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_surgery.DataSource = dt;
            gridview_surgery.DataBind();
        }
        c.Con.Close();
    }
    public void GridView_Pre_Pres_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        SqlCommand cmddept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like 'NCD%'", c.Con);
        SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
        DataTable dtdept = new DataTable();
        sdadept.Fill(dtdept);

        int idept = cmddept.ExecuteNonQuery();
        int j = dtdept.Rows.Count;
        if (j > 0)
        {
            DataRow row = dtdept.Rows[dtdept.Rows.Count - 1];

            int did = Convert.ToInt32(row[0]);

            SqlCommand cmd = new SqlCommand("select * from Prescription p inner join NCDEmployeeDetails e on e.empid=p.docid inner join MedMaster m on m.MedId=p.medid where p.patientid='" + pno + "' and p.deptid='" + did + "' order by p.presid desc", c.Con);
            cmd.Parameters.AddWithValue("@pno", pno);
            cmd.Parameters.AddWithValue("@deptid", did);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                GridView_Pre_Pres.DataSource = dt;
                GridView_Pre_Pres.DataBind();
            }

            else
                lbl_response0.Visible = true;
        }
        c.Con.Close();
    }
    public void GridView_Pre_Diag_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        SqlCommand cmd = new SqlCommand("select d.symptoms,m.diagnosis,d.remarks,d.complications,e.empname,CONVERT(VARCHAR(10),d.dateofdiag,101) as dateofdiaganosis from NCDClinicDiagnosis d inner join NCDEmployeeDetails e on e.empid=d.docid inner join NCDDiagMaster m on m.diagmasterid=d.prodiagnosis where patientid=@pno order by d.diagid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", Session["docid"]);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Pre_Diag.DataSource = dt;
            GridView_Pre_Diag.DataBind();
        }
        else
            lbl_response.Visible = true;
        c.Con.Close();
    }
    private void setdata()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        SqlCommand cmd_pat = new SqlCommand("select * from patientdetails where patientid= '" + pno + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            String name = Convert.ToString(row_pat[1]);
            String pid = Convert.ToString(row_pat[0]);

            DateTime dob = Convert.ToDateTime(row_pat[7]);
            String gender = Convert.ToString(row_pat[14]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_age.Text = Convert.ToString(age) + " years";
            lbl_gen.Text = gender;
            lbl_name.Text = name;
           // lbl_pno.Text = pid;
        }
        c.Con.Close();
    }
    protected void gridview_pres_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getCon();
        String id = gridview_pres.DataKeys[e.RowIndex].Values[0].ToString();
        String s = "delete Prescription where presid='" + Convert.ToInt32(id) + "'";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();
        gridview_pres.EditIndex = -1;
        c.Con.Close();
        BindGridView();

    }
    protected void gridview_pres_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gridview_pres.EditIndex = e.NewEditIndex;

        BindGridView();

    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from CasualityEmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);
            //SqlCommand cmd_med = new SqlCommand("select * from MedMaster where MedName='" + txt_dname.Text + "' ", c.Con);
            //SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
            //DataTable dt_med = new DataTable();
            //sda_med.Fill(dt_med);


            //int k_med = cmd_med.ExecuteNonQuery();

            //if (dt_med.Rows.Count > 0)
            //{
            //    DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

            //    int medid = Convert.ToInt32(row_med[0]);
                pno = Convert.ToInt32(txt_pno.Text);
                String s = "insert into Prescription values('" + pno + "','" + Session["docid"] + "','" + ddl_pres.SelectedItem.Value + "','" + txt_potency.Text + "','" + txt_dose.Text + "','" + txt_freq.Text + "','" + txt_days.Text + "','" + txt_remarks.Text + "','" + DateTime.Today + "','" + deptid + "','Prescribed')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();
                c.Con.Close();
                txt_remarks.Text = " ";
                txt_potency.Text = " ";
                txt_freq.Text = " ";
                txt_dose.Text = " ";
               // txt_dname.Text = " ";
                txt_days.Text = " ";
                BindGridView();
            //}
        }
    }
    public void BindGridView()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        SqlCommand cmd_pat = new SqlCommand("select deptid from CasualityEmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);
            SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and docid=@docid and deptid=@deptid order by presid", c.Con);
            cmd.Parameters.AddWithValue("@pno", pno);
            cmd.Parameters.AddWithValue("@date", DateTime.Today);
            cmd.Parameters.AddWithValue("@docid", Session["docid"]);
            cmd.Parameters.AddWithValue("@deptid", deptid);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                gridview_pres.DataSource = dt;
                gridview_pres.DataBind();
            }
        }
        c.Con.Close();
    }

    protected void btn_ref_Click(object sender, EventArgs e)
    {
        txt_remarks.Text = " ";
        txt_potency.Text = " ";
        txt_freq.Text = " ";
        txt_dose.Text = " ";
     //   txt_dname.Text = " ";
        txt_days.Text = " ";
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        SqlCommand cmd_pat = new SqlCommand("select deptid from EmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_pat[0]);
            SqlCommand cmd = new SqlCommand("select * from Prescription where patientid=@pno and dateofpres=@date and docid=@docid and deptid=@deptid order by presid", c.Con);
            cmd.Parameters.AddWithValue("@pno", pno);
            cmd.Parameters.AddWithValue("@date", DateTime.Today);
            cmd.Parameters.AddWithValue("@docid", Session["docid"]);
            cmd.Parameters.AddWithValue("@deptid", deptid);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                gridview_pres.DataSource = dt;
                gridview_pres.DataBind();
            }
        }
        c.Con.Close();
    }
    protected void gridview_pres_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        c.getCon();
        string pid = gridview_pres.DataKeys[e.RowIndex].Values[0].ToString();
        string dname = ((TextBox)gridview_pres.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
        string potency = (gridview_pres.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text;
        string dosage = (gridview_pres.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text;
        string freq = (gridview_pres.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text;
        string noofdays = (gridview_pres.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text;
        string upd = "update Prescription set potency='" + potency + "',frequency='" + freq + "',dosage='" + dosage + "',no_of_days='" + noofdays + "' where presid='" + pid + "'";
        SqlCommand cmd = new SqlCommand(upd, c.Con);
        cmd.ExecuteNonQuery();
        gridview_pres.EditIndex = -1;
        c.Con.Close();
        BindGridView();

    }
    protected void gridview_pres_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gridview_pres.EditIndex = -1;
        BindGridView();
    }
    protected void btn_adddiag_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd_pat = new SqlCommand("select deptid from CasualityEmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];
            int deptid = Convert.ToInt32(row_pat[0]);
            String s = "insert into Diagnosis values('" + pno + "','" + Session["docid"] + "','" + txt_present.Text + "','" + txt_diag.Text + "','" + txt_remarkdiag.Text + "','" + DateTime.Today + "','" + deptid + "')";
            SqlCommand cmds = new SqlCommand(s, c.Con);
            cmds.ExecuteNonQuery();
            txt_remarkdiag.Text = " ";
            txt_present.Text = " ";
            txt_diag.Text = " ";
            c.Con.Close();

        }
    }
    protected void btn_refreshdiag_Click(object sender, EventArgs e)
    {
        txt_remarkdiag.Text = " ";
        txt_present.Text = " ";
        txt_diag.Text = " ";

    }

    protected void btn_past_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        Label[] lbl = { Label1, Label2, Label3, Label4, Label5, Label6, Label7, Label8, Label9, Label10, Label11, Label12, Label13, Label14, Label15 };
        TextBox[] tbs = { textBox1, textBox2, textBox3, textBox4, textBox5, textBox6, textBox7, textBox8, textBox9, textBox10, textBox11, textBox12, textBox13, textBox14, textBox15 }; //put all into this array
        CheckBoxList[] chk = { CheckBoxList1, CheckBoxList2, CheckBoxList3, CheckBoxList4, CheckBoxList5, CheckBoxList6, CheckBoxList7, CheckBoxList8, CheckBoxList9, CheckBoxList10, CheckBoxList11, CheckBoxList12, CheckBoxList13, CheckBoxList14, CheckBoxList15 };
        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_past = new SqlCommand("select * from PastHistoryMaster where history='" + txt + "' ", c.Con);
            SqlDataAdapter sda_past = new SqlDataAdapter(cmd_past);
            DataTable dt_past = new DataTable();
            sda_past.Fill(dt_past);


            int k_past = cmd_past.ExecuteNonQuery();

            if (dt_past.Rows.Count > 0)
            {
                DataRow row_past = dt_past.Rows[dt_past.Rows.Count - 1];

                int pastid = Convert.ToInt32(row_past[0]);

                String s = "insert into PastHistory values('" + pastid + "','" + chk[i].SelectedItem.Text + "','" + tbs[i].Text + "', '" + pno + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();
            }
        }
        Label[] labmed = { lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, lbl11, lbl12, lbl13, lbl14, lbl15, lbl16, lbl17, lbl18, lbl19, lbl20 };
        TextBox[] txtmed = { txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18, txt19, txt20 };
        RadioButtonList[] rblmed = { RadioButtonList1, RadioButtonList2, RadioButtonList3, RadioButtonList4, RadioButtonList5, RadioButtonList6, RadioButtonList7, RadioButtonList8, RadioButtonList9, RadioButtonList10, RadioButtonList11, RadioButtonList12, RadioButtonList13, RadioButtonList14, RadioButtonList15, RadioButtonList16, RadioButtonList17, RadioButtonList18, RadioButtonList19, RadioButtonList20 };
        for (int i = 0; i <= txtmed.Length - 1; i++)
        {
            string txt = labmed[i].Text;


            SqlCommand cmd_med = new SqlCommand("select * from MedicalProblemMaster where MedicalProblem='" + txt + "' ", c.Con);
            SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
            DataTable dt_med = new DataTable();
            sda_med.Fill(dt_med);


            int k_med = cmd_med.ExecuteNonQuery();

            if (dt_med.Rows.Count > 0)
            {
                DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

                int medid = Convert.ToInt32(row_med[0]);

                String s = "insert into MedicalProblemHistory values('" + medid + "','" + rblmed[i].SelectedItem.Text + "','" + txtmed[i].Text + "', '" + pno + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();
            }
        }
        MultiView_Main.ActiveViewIndex = 1;

        c.Con.Close();
    }
    protected void btn_addfam_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        Label[] lbl = { lbfamily1, lbfamily2, lbfamily3, lbfamily4, lbfamily5, lbfamily6, lbfamily7, lbfamily8, lbfamily9, lbfamily10, lbfamily11, lbfamily12 };
        TextBox[] tbs = { txtfam1, txtfam2, txtfam3, txtfam4, txtfam5, txtfam6, txtfam7, txtfam8, txtfam9, txtfam10, txtfam11, txtfam12 };
        CheckBoxList[] chk = { chkfam, chkfam0, chkfam1, chkfam2, chkfam3, chkfam4, chkfam5, chkfam6, chkfam7, chkfam8, chkfam9, chkfam10 };
        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_fam = new SqlCommand("select * from FamilyHistoryMaster where famillness='" + txt + "' ", c.Con);
            SqlDataAdapter sda_fam = new SqlDataAdapter(cmd_fam);
            DataTable dt_fam = new DataTable();
            sda_fam.Fill(dt_fam);


            int k_fam = cmd_fam.ExecuteNonQuery();

            if (dt_fam.Rows.Count > 0)
            {
                DataRow row_fam = dt_fam.Rows[dt_fam.Rows.Count - 1];

                int famid = Convert.ToInt32(row_fam[0]);
                string gp = "No", fat = "No", mot = "No", bro = "No", sis = "No", son = "No", dau = "No", oth = "No";

                if (chk[i].Items[0].Selected == true)
                    gp = "yes";
                if (chk[i].Items[1].Selected == true)
                    fat = "yes";
                if (chk[i].Items[2].Selected == true)
                    mot = "yes";
                if (chk[i].Items[3].Selected == true)
                    bro = "yes";
                if (chk[i].Items[4].Selected == true)
                    sis = "yes";
                if (chk[i].Items[5].Selected == true)
                    son = "yes";
                if (chk[i].Items[6].Selected == true)
                    dau = "yes";
                if (chk[i].Items[7].Selected == true)
                    oth = "yes";



                String s = "insert into FamilyHistory values('" + famid + "','" + gp + "','" + fat + "','" + mot + "','" + bro + "','" + sis + "','" + son + "','" + dau + "','" + oth + "','" + tbs[i].Text + "' ,'" + pno + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView_Main.ActiveViewIndex = 1;

        c.Con.Close();
    }
    protected void btn_addvac_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        Label[] lbl = { labvac1, labvac2, labvac3, labvac4, labvac5, labvac6, labvac7, labvac8, labvac9, labvac10, labvac11, labvac12, labvac13, labvac14, labvac15 };
        TextBox[] tbs = { txtvac1, txtvac2, txtvac3, txtvac4, txtvac5, txtvac6, txtvac7, txtvac8, txtvac9, txtvac10, txtvac11, txtvac12, txtvac13, txtvac14, txtvac15 };
        RadioButtonList[] rbl = { rblvac1, rblvac2, rblvac3, rblvac4, rblvac5, rblvac6, rblvac7, rblvac8, rblvac9, rblvac10, rblvac11, rblvac12, rblvac13, rblvac14, rblvac15 };
        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_vac = new SqlCommand("select * from ImmunizationMaster where Immunization='" + txt + "' ", c.Con);
            SqlDataAdapter sda_vac = new SqlDataAdapter(cmd_vac);
            DataTable dt_vac = new DataTable();
            sda_vac.Fill(dt_vac);


            int k_vac = cmd_vac.ExecuteNonQuery();

            if (dt_vac.Rows.Count > 0)
            {
                DataRow row_vac = dt_vac.Rows[dt_vac.Rows.Count - 1];

                int vacid = Convert.ToInt32(row_vac[0]);
                String s = "insert into ImmunizationHistory values('" + vacid + "','" + rbl[i].SelectedItem.Text + "','" + tbs[i].Text + "' ,'" + pno + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView_Main.ActiveViewIndex = 1;

        c.Con.Close();
    }
    protected void btn_add_sub_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        Label[] lbl = { lblsub1, lblsub2, lblsub3, lblsub4 };
        TextBox[] tbsfreq = { txt_freq_sub1, txt_freq_sub2, txt_freq_sub3, txt_freq_sub1 };
        TextBox[] tbslong = { txt_long_sub1, txt_long_sub2, txt_long_sub3, txt_long_sub1 };

        TextBox[] tbsstop = { txt_stop_sub1, txt_stop_sub2, txt_stop_sub3, txt_stop_sub1 };

        RadioButtonList[] rblcur = { rbl_cur_sub1, rbl_cur_sub2, rbl_cur_sub3, rbl_cur_sub4 };
        RadioButtonList[] rblpre = { rbl_pre_sub1, rbl_pre_sub2, rbl_pre_sub3, rbl_pre_sub4 };

        for (int i = 0; i <= tbsfreq.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_sub = new SqlCommand("select * from SubstanceMaster where Substance='" + txt + "' ", c.Con);
            SqlDataAdapter sda_sub = new SqlDataAdapter(cmd_sub);
            DataTable dt_sub = new DataTable();
            sda_sub.Fill(dt_sub);


            int k_sub = cmd_sub.ExecuteNonQuery();

            if (dt_sub.Rows.Count > 0)
            {
                DataRow row_sub = dt_sub.Rows[dt_sub.Rows.Count - 1];

                int subid = Convert.ToInt32(row_sub[0]);
                String s = "insert into SubstanceUsageHistory values('" + subid + "','" + rblcur[i].SelectedItem.Text + "','" + rblpre[i].SelectedItem.Text + "','" + tbsfreq[i].Text + "' ,'" + tbslong[i].Text + "' ,'" + tbsstop[i].Text + "' ,'" + pno + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView_Main.ActiveViewIndex = 1;

        c.Con.Close();
    }
    protected void btn_add_allergent_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        Label[] lbl = { lblallergy1, lblallergy2, lblallergy3, lblallergy4, lblallergy5, lblallergy6, lblallergy7, lblallergy8 };
        TextBox[] tbs = { txt_allergent1, txt_allergent2, txt_allergent3, txt_allergent4, txt_allergent5, txt_allergent6, txt_allergent7, txt_allergent8 };
        RadioButtonList[] rbl = { rbl_allergent1, rbl_allergent2, rbl_allergent3, rbl_allergent4, rbl_allergent5, rbl_allergent6, rbl_allergent7, rbl_allergent8 };

        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_aller = new SqlCommand("select * from AllergentMaster where allergent='" + txt + "' ", c.Con);
            SqlDataAdapter sda_aller = new SqlDataAdapter(cmd_aller);
            DataTable dt_aller = new DataTable();
            sda_aller.Fill(dt_aller);


            int k_aller = cmd_aller.ExecuteNonQuery();

            if (dt_aller.Rows.Count > 0)
            {
                DataRow row_aller = dt_aller.Rows[dt_aller.Rows.Count - 1];

                int allerid = Convert.ToInt32(row_aller[0]);
                String s = "insert into AllergyHistory values('" + allerid + "','" + rbl[i].SelectedItem.Text + "','" + tbs[i].Text + "' ,'" + pno + "','" + DateTime.Today + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView_Main.ActiveViewIndex = 0;

        c.Con.Close();
    }
    protected void GridView_Pre_Pres_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Pre_Pres.PageIndex = e.NewPageIndex;
        GridView_Pre_Pres_Bind();
    }
    protected void GridView_Pre_Diag_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Pre_Diag.PageIndex = e.NewPageIndex;
        GridView_Pre_Diag_Bind();
    }


    protected void GridView_lab_request_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getCon();
        String id = GridView_lab_request.DataKeys[e.RowIndex].Values[0].ToString();
        String s = "delete LabRequest where labreqid='" + Convert.ToInt32(id) + "'";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();
        GridView_lab_request.EditIndex = -1;
        c.Con.Close();
        GridView_Lab_Request_Bind();
    }


    protected void link_completed_Click(object sender, EventArgs e)
    {

        c.getCon();
        int docid = Convert.ToInt32(Session["docid"]);

        SqlCommand cmdcat = new SqlCommand("select deptid from CasualityEmployeeDetails where empid='" + docid + "'", c.Con);
        SqlDataAdapter sdacat = new SqlDataAdapter(cmdcat);
        DataTable dtcat = new DataTable();
        sdacat.Fill(dtcat);

        int icat = cmdcat.ExecuteNonQuery();
        if (dtcat.Rows.Count > 0)
        {
            DataRow row1 = dtcat.Rows[dtcat.Rows.Count - 1];

            int dept = Convert.ToInt32(row1[0]);
            pno = Convert.ToInt32(txt_pno.Text);

            String s = "update CasualityBedAllocation set docid='" + docid + "', status='Completed' where patientid=@pat and timeout=@date";
            SqlCommand cmds = new SqlCommand(s, c.Con);
            cmds.Parameters.AddWithValue("@pat", pno);
            cmds.Parameters.AddWithValue("@date", ' ');
            cmds.ExecuteNonQuery();
            Response.Redirect("homepagedoctor.aspx");
        }
        c.Con.Close();

    }
    protected void GridView_vitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_vitals.PageIndex = e.NewPageIndex;
        GridView_vital_Bind();
    }

    public void bindDepts()
    {
        c.getCon();

        SqlCommand cmdcat = new SqlCommand("select * from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname not in ('Casuality','Fever Clinic') ", c.Con);
        // cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
        SqlDataAdapter sdacat = new SqlDataAdapter(cmdcat);
        DataTable dtcat = new DataTable();
        sdacat.Fill(dtcat);

        int icat = cmdcat.ExecuteNonQuery();

        if (dtcat.Rows.Count > 0)
        {
            ddl_depts.DataSource = dtcat;
            ddl_depts.DataValueField = "deptid";
            ddl_depts.DataTextField = "deptname";
            ddl_depts.DataBind();

        }
        c.Con.Close();
        ddl_depts.Items.Insert(0, "[SELECT]");
    }

    protected void ddl_depts_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        string s;
        if (ddl_depts.SelectedItem.Text == "NCD Clinic")
            s = "select * from NCDEmployeeDetails  where deptid='" + ddl_depts.SelectedItem.Value + "' order by empname";
        else
            s = "select * from EmployeeDetails  where deptid='" + ddl_depts.SelectedItem.Value + "' order by empname";
        SqlCommand cmdcat = new SqlCommand(s, c.Con);
        // cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
        SqlDataAdapter sdacat = new SqlDataAdapter(cmdcat);
        DataTable dtcat = new DataTable();
        sdacat.Fill(dtcat);

        int icat = cmdcat.ExecuteNonQuery();

        if (dtcat.Rows.Count > 0)
        {
            ddl_docs.DataSource = dtcat;
            ddl_docs.DataValueField = "empid";
            ddl_docs.DataTextField = "empname";
            ddl_docs.DataBind();

        }
        c.Con.Close();
        ddl_docs.Items.Insert(0, "[SELECT]");
        ddl_docs.Items.Insert(1, "No Preference");



    }
    protected void btn_refreshref_Click(object sender, EventArgs e)
    {
        ddl_depts.SelectedIndex = 0;
        ddl_docs.SelectedIndex = 0;
        txt_remarkref.Text = " ";
    }
    protected void btn_addref_Click(object sender, EventArgs e)
    {
        c.getCon();

        pno = Convert.ToInt32(txt_pno.Text);
        if (ddl_docs.SelectedIndex == 1)
            ddl_docs.SelectedItem.Value = "0";
        String s = "insert into ReferralTableCasuality values('" + pno + "','" + DateTime.Today + "','" + Session["docid"] + "','" + ddl_docs.SelectedItem.Value + "','" + ddl_depts.SelectedItem.Value + "','" + txt_remarkref.Text + "','Referred','0')";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();

        c.Con.Close();
        ddl_depts.SelectedIndex = 0;
        ddl_docs.SelectedIndex = 0;
        txt_remarkref.Text = " ";
    }
    protected void link_diagnosis_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 0;
    }
    protected void link_prescription_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 1;
    }
    protected void link_injection_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 2;
    }
    protected void link_lab_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 2;
    }
    protected void link_prediag_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 3;
    }
    protected void link_prepres_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 4;
    }
    protected void link_prelab_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 5;
    }
    protected void link_vitals_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 6;
    }
    protected void link_family_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 7;
    }
    protected void link_personal_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 9;
    }
    protected void link_past_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 8;
    }
    protected void link_allergies_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 10;
    }
    protected void link_vaccinations_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 11;
    }
    protected void link_take_fam_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 12;
    }
    protected void link_take_pers_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 14;
    }
    protected void link_take_past_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 13;
    }
    protected void link_take_aller_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 16;
    }
    protected void link_take_vacc_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 15;
    }
    protected void link_refer_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 17;
    }

    protected void link_casdiag_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 18;

    }
    protected void link_caspres_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 19;

    }
    protected void link_labres_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 20;

    }
    protected void link_inj_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 21;

    }
    protected void link_vit_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 22;

    }
    protected void GridView_CasLab_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_CasLab_Result.PageIndex = e.NewPageIndex;
        GridView_caslab_Bind();
    }
    public void GridView_CasInjection_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);


        SqlCommand cmd = new SqlCommand("select * from CasualityInjectionTable i inner join InjectionMaster m on i.injectionid=m.injectionid inner join CasualityEmployeeDetails e on e.empid=i.docid  where patientid=@pno order by injecreqid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_CasInjection.DataSource = dt;
            GridView_CasInjection.DataBind();
        }
        else
            lbl_casinj.Visible = true;
        c.Con.Close();

    }

    public void GridView_Casvital_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from VitalMaster vm inner join CasualityVitalTable v on vm.vitalmasterid=v.vitalmasterid inner join CasualityEmployeeDetails e on e.empid=v.nurseid  where patientid=@pno order by vitalid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Casvitals.DataSource = dt;
            GridView_Casvitals.DataBind();
        }
        else
            lbl_casvit.Visible = true;
        c.Con.Close();

    }

    protected void GridView_caspre_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_caspre.PageIndex = e.NewPageIndex;
        GridView_caspre_Bind();
    }
    protected void GridView_casdiag_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_casdiag.PageIndex = e.NewPageIndex;
        GridView_casdiag_Bind();
    }
    public void GridView_caspre_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from Prescription p inner join CasualityEmployeeDetails e on e.empid=p.docid inner join MedMaster m on m.MedId=p.medid inner join Department d on d.deptid=e.deptid where patientid=@pno and d.deptname like 'Casuality' order by p.presid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_caspre.DataSource = dt;
            GridView_caspre.DataBind();
        }
        else
            lbl_caspres.Visible = true;
        c.Con.Close();
    }
    public void GridView_casdiag_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        SqlCommand cmd = new SqlCommand("select * from Diagnosis d inner join CasualityEmployeeDetails e on e.empid=d.docid inner join Department dp on dp.deptid=e.deptid where patientid=@pno and dp.deptname like 'Casuality' order by d.diagid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@docid", Session["docid"]);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_casdiag.DataSource = dt;
            GridView_casdiag.DataBind();
        }
        else
            lbl_casdiag.Visible = true;
        c.Con.Close();
    }


    protected void GridView_Casvitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Casvitals.PageIndex = e.NewPageIndex;
        GridView_Casvital_Bind();
    }
    protected void GridView_CasInjection_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_CasInjection.PageIndex = e.NewPageIndex;
        GridView_CasInjection_Bind();
    }



    protected void link_opdiag_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 23;
    }
    protected void link_oppres_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 24;

    }
    protected void link_oplabres_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 25;

    }
    protected void link_opinj_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 27;

    }
    protected void link_opvit_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 26;

    }
    protected void GridView_opPre_Pres_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        GridView_opPre_Pres.PageIndex = e.NewPageIndex;
        GridView_opPre_Pres_Bind();
    }
    protected void GridView_opLab_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_opLab_Result.PageIndex = e.NewPageIndex;
        // GridView_opLab_Result_Bind();
    }
    protected void GridView_opvitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_opvitals.PageIndex = e.NewPageIndex;
        GridView_opvitals_Bind();
    }
    public void GridView_opvitals_Bind()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from VitalMaster vm inner join VitalTable v on vm.vitalmasterid=v.vitalmasterid inner join EmployeeDetails e on e.empid=v.nurseid  where patientid=@pno order by vitalid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_opvitals.DataSource = dt;
            GridView_opvitals.DataBind();
        }
        else
            lbl_response23.Visible = true;
        c.Con.Close();
    }
    protected void GridView_opInj_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_opInj.PageIndex = e.NewPageIndex;
        GridView_opInj_Bind();
    }
    public void GridView_opInj_Bind()
    {
        c.getCon();


        pno = Convert.ToInt32(txt_pno.Text);


        SqlCommand cmd = new SqlCommand("select * from InjectionTable i inner join InjectionMaster m on i.injectionid=m.injectionid inner join EmployeeDetails e on e.empid=i.docid  where patientid=@pno order by injecreqid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_opInj.DataSource = dt;
            GridView_opInj.DataBind();
        }
        else
            lbl_casinj.Visible = true;

        c.Con.Close();

    }
    protected void GridView_RBS_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_RBS.PageIndex = e.NewPageIndex;
        BindGridView_RBS();
    }
    public void BindGridView_RBS()
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);

        SqlCommand cmd = new SqlCommand("select * from NCDRBSTable n inner join NCDEmployeeDetails e on e.empid=n.nurseid where patientid=@pno order by rbsid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_RBS.DataSource = dt;
            GridView_RBS.DataBind();
        }
        else
            lbl_resp30.Visible = true;
        c.Con.Close();

    }
    protected void link_rbs_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 28;
    }
    protected void GridView_Lab_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Lab_Result.PageIndex = e.NewPageIndex;
        GridView_Lab_Result_Bind();
    }
    protected void btn_lab_req_Click(object sender, EventArgs e)
    {
        c.getCon();
        pno = Convert.ToInt32(txt_pno.Text);
        String s = "insert into CasualityLabRequest values( '" + ddl_test.SelectedItem.Value + "','" + txt_lab_remark.Text + "','" + pno + "','" + Session["uid"] + "','" + DateTime.Today + "','Requested')";
        SqlCommand cmdtest = new SqlCommand(s, c.Con);
        cmdtest.ExecuteNonQuery();

        c.Con.Close();
        GridView_Lab_Request_Bind();
    }
    //public void GridView_Lab_Request_Bind()
    //{
    //    c.getCon();
    //    pno = Convert.ToInt32(Request.QueryString["PatNo"].ToString());
    //    int id = Convert.ToInt32(Session["docid"]);
    //    SqlCommand cmd = new SqlCommand("select * from LabRequest r inner join LabTestMaster l on r.testmasterid=l.testmasterid  where patientid=@pno and dateofreq=@date and docid=@docid", c.Con);
    //    cmd.Parameters.AddWithValue("@pno", pno);
    //    cmd.Parameters.AddWithValue("@date", DateTime.Today);
    //    cmd.Parameters.AddWithValue("@docid", id);
    //    DataTable dt = new DataTable();
    //    SqlDataAdapter da = new SqlDataAdapter(cmd);
    //    da.Fill(dt);


    //    if (dt.Rows.Count > 0)
    //    {
    //        GridView_lab_request.DataSource = dt;
    //        GridView_lab_request.DataBind();
    //    }
    //    c.Con.Close();

    //}
    public void Bind_Labtest_ddl()
    {
        c.getCon();

        SqlCommand cmd_pat = new SqlCommand("select * from LabTestMaster ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            ddl_test.DataSource = dt_pat;
            ddl_test.DataTextField = "testname";
            ddl_test.DataValueField = "testmasterid";
            ddl_test.DataBind();
        }
        c.Con.Close();
    }

    protected void btn_injopadd_Click(object sender, EventArgs e)
    {
        c.getCon();
        //SqlCommand cmdcat = new SqlCommand("select * from InjectionMaster where injectionname='" + txt_injname.Text + "'", c.Con);
        //// cmd.Parameters.AddWithValue("@word", txt_pswd.Text);
        //SqlDataAdapter sdacat = new SqlDataAdapter(cmdcat);
        //DataTable dtcat = new DataTable();
        //sdacat.Fill(dtcat);

        //int icat = cmdcat.ExecuteNonQuery();
        pno = Convert.ToInt32(txt_pno.Text);
        int docid = Convert.ToInt32(Session["docid"]);
        //if (dtcat.Rows.Count > 0)
        //{
            //DataRow row1 = dtcat.Rows[dtcat.Rows.Count - 1];

            //int injid = Convert.ToInt32(row1[0]);
            SqlCommand cmd_pat = new SqlCommand("select deptid from CasualityEmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);


            int k_pat = cmd_pat.ExecuteNonQuery();

            if (dt_pat.Rows.Count > 0)
            {
                DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

                int deptid = Convert.ToInt32(row_pat[0]);
                String s = "insert into CasualityInjectionTable values('" + ddl_injname.SelectedItem.Value + "','" + txt_injdos.Text + "','" + txt_injnodos.Text + "' ,'" + txt_injins.Text + "','Requested','" + pno + "','" + docid + "','" + DateTime.Now + "','" + deptid + "')";
                SqlCommand cmds = new SqlCommand(s, c.Con);
                cmds.ExecuteNonQuery();

            }
            c.Con.Close();
            GridView_Injection_Bind();
            txt_injdos.Text = " ";
            txt_injnodos.Text = " ";
            txt_injins.Text = " ";
            //txt_injname.Text = " ";
        //}

    }
    public void GridView_Injection_Bind()
    {
        c.getCon();


        pno = Convert.ToInt32(txt_pno.Text);


        SqlCommand cmd = new SqlCommand("select * from CasualityInjectionTable i inner join InjectionMaster m on i.injectionid=m.injectionid inner join EmployeeDetails e on e.empid=i.docid  where patientid=@pno order by injecreqid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            grid_inj.DataSource = dt;
            grid_inj.DataBind();
        }

        c.Con.Close();


    }

    protected void link_injop_Click(object sender, EventArgs e)
    {
        MultiView_Main.ActiveViewIndex = 29;
    }

    protected void grid_inj_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        grid_inj.PageIndex = e.NewPageIndex;
        GridView_Injection_Bind();
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {


        //link_completed.Visible = true;
        lbl_age.Visible = true;
        lbl_gen.Visible = true;
        lbl_name.Visible = true;

        c.getCon();
        SqlCommand cmdpat = new SqlCommand("select * from patientdetails where patientid= '" + txt_pno.Text + "' ", c.Con);
        SqlDataAdapter sdapat = new SqlDataAdapter(cmdpat);
        DataTable dtpat = new DataTable();
        sdapat.Fill(dtpat);

        int k = cmdpat.ExecuteNonQuery();

        if (dtpat.Rows.Count > 0)
        {
            DataRow rowpat = dtpat.Rows[dtpat.Rows.Count - 1];
            DateTime todaydate = DateTime.Now;
            string pname = Convert.ToString(rowpat[1]);
            string gen = Convert.ToString(rowpat[14]);
            DateTime dob = Convert.ToDateTime(rowpat[7]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_name.Text = pname;
            lbl_gen.Text = gen;
            lbl_age.Text = age.ToString();
        }
        c.Con.Close();
        Bind_Labtest_ddl();
        setdata();
        Bind_inj_ddl();
        Bind_meds_ddl();
        bindDepts();
        BindGridView_RBS();
        GridView_opvitals_Bind();
        GridView_opPre_Pres_Bind();
        GridView_opPre_Diag_Bind();
        GridView_vital_Bind();
        GridView_opInj_Bind();
        GridView_Pre_Pres_Bind();
        GridView_opPre_Pres_Bind();
        GridView_Pre_Diag_Bind();
        gridview_past_Bind();
        GridView_caspre_Bind();
        Bind_Labtest_ddl();

        GridView_casdiag_Bind();
        GridView_Casvital_Bind();
        GridView_CasInjection_Bind();
        GridView_caslab_Bind();

        GridView_Family_Bind();
        GridView_Social_Bind();
        gridview_surgery_Bind();
        GridView_Vaccination_Bind();
        Gridview_Allergy_Bind();
        GridView_Lab_Request_Bind();
        ddl_test.Items.Insert(0, "[Select]");
        GridView_Lab_Result_Bind();
        BindGridView();
        Gridview_oplab_bind();
        GridView_Injection_Bind();
        MultiView_Main.ActiveViewIndex = 0;
        if (lbl_gen.Text == "Female")
            lbl20.Text = "Females: uterus, tubes, ovaries";
        else
            lbl20.Text = "Males: prostate, penis, testes, vasectomy";
            
    }
}