﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Superintendent/SuperintendentHomeMaster.master" AutoEventWireup="true" CodeFile="ChangeUsername.aspx.cs" Inherits="Superintendent_ChangeUsername" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
   
    
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
        <table >
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_title" runat="server" ></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Enter New UserName</td>
                <td >
                    <asp:TextBox ID="txt_usr" CssClass="twitter" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_usr" ErrorMessage="*Invalid Username:length must be between 7 to 10 characters" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z0-9'@&amp;#.\s]{7,10}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td >
                    &nbsp;</td>
            </tr>
            <tr>
                <td>Enter Password</td>
                <td>
                    <asp:TextBox ID="txt_pass" CssClass="twitter" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pass" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_usr" CssClass="btn" runat="server" OnClick="btn_usr_Click" Text="Change Username" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" CssClass="btn" runat="server" Text="Cancel" OnClick="btn_cancel_Click" /></td>
            </tr>
            
            
            
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            
            
        </table>
    <div>
    
    </div>
    </form>

</asp:Content>

