﻿//update injection stock
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Pharmacy_CasualityPharmacy_UpdateStockInj : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridView();
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
    }
    public void BindGridView()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from CasualityInjectionOrder i inner join InjectionMaster m on i.injectionid=m.injectionid where i.status='Pending'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            lbl_med.Visible = false;

        }
        else
        {
            lbl_med.Visible = true;
            lbl_med.Text = "NO PENDING ORDERS";
        }
        c.Con.Close();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView();

    }
    protected void GridView1_RowCommand1(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("TallyOrderInj.aspx?OrderNo=" + GridView1.DataKeys[index].Value.ToString());
        }
    }
}