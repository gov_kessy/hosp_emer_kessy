﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Reception/FeverClinicReceptionMaster.master" AutoEventWireup="true" CodeFile="PrintToken.aspx.cs" Inherits="FeverClinic_Reception_PrintToken" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
     <%--<script language="javascript" type="text/javascript">
         function CallPrint(strid) {
             var prtContent = document.getElementById(strid);
             var WinPrint = window.open('', '', 'letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
             WinPrint.document.write(prtContent.innerHTML);
             WinPrint.document.close();
             WinPrint.focus();
             WinPrint.print();
             WinPrint.close();
             prtContent.innerHTML = strOldOne;
         }
</script>--%>
     <style type="text/css">
         .auto-style1 {
             height: 25px;
         }
         .auto-style2 {
             height: 20px;
         }
     </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table >
             <tr>
            <td> <ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">Enter Patient No:</td>
                <td class="auto-style1">
                    <asp:TextBox ID="txt_pno" CssClass="twitter" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pno" ErrorMessage="*Enter Card No" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
            <tr>
                <td>Select Date of Consultation:&nbsp; &nbsp; </td>
                <td><asp:TextBox ID="txtDate" CssClass="twitter" runat="server" />
<ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDate" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate" ErrorMessage="*Enter Date" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
            <tr>
                <td>
                    <asp:Button ID="btn_search" cssclass="btn" runat="server" Text="Search Token" OnClick="btn_search_Click" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <div id="bill">
           
                <table id="tab_print" runat="server" style="font-size: medium; width: 640px;"> 
                    <tr>
                    <td > &nbsp;</td>
                    <td> &nbsp;</td>
                        </tr>
                    <tr>
                    <td >
                        <asp:Image ID="img_logo" runat="server" height="100" Width="100" ImageUrl="~/OPD/Registration/image/gov.png"/></td>
                                        <td >Government General Hospital
                                            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Changanacherry</td>    </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                     <tr>
                    <td >Date:</td>
                    <td >
                        <asp:Label ID="lbl_date" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                     <tr>
                    <td >Patient No:</td>
                    <td >
                        <asp:Label ID="lbl_patno" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                     <tr>
                     <td >Patient Name:</td>
                    <td >
                        <asp:Label ID="lbl_pname" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                      <tr>
                    <td >Age:</td>
                    <td>
                        <asp:Label ID="lbl_age" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                      <tr>
                    <td>Token No:</td>
                    <td>
                        <asp:Label ID="lbl_token" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                    <tr>
                    <td >&nbsp;&nbsp;&nbsp;</td>
                        <td >&nbsp;&nbsp;&nbsp;</td>
                    <td> Posted By: </td>
                       <td> <asp:Label ID="lbl_postedby" runat="server"></asp:Label></td>
                        </tr>
</table>
                
              </div>
        <p><asp:Button ID="btn_print" runat="server" Text="Print" cssclass="btn" onclientclick="javascript:CallPrint('bill');" xmlns:asp="#unknown" ForeColor="Gray" OnClick="btn_print_Click"  /></p>
    <br /><br />
    </form>
</asp:Content>

