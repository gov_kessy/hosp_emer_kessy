﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="AllocatedBed.aspx.cs" Inherits="Casuality_Doctor_AllocatedBed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
  
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server" >
        <table >
            <tr>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td >Patient Id:</td>
                <td >
                    <asp:Label ID="lbl_patno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Patient Name:</td>
                <td>
                    <asp:Label ID="lbl_pname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td >
                    <asp:Label ID="lbl_gen" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Age:</td>
                <td>
                    <asp:Label ID="lbl_age" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Treatment Status:</td>
                <td>
                    <asp:LinkButton ID="link_dis" runat="server" OnClick="link_dis_Click">Discharge</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"> <div class="drop">
<ul class="drop_menu">

     <li>
         <asp:LinkButton ID="link_observ" runat="server" OnClick="link_observ_Click">Observations</asp:LinkButton>
     </li>
     <li>
         <asp:LinkButton ID="link_inj" runat="server" OnClick="link_inj_Click">Injections</asp:LinkButton>
     </li>
    <li>
         <asp:LinkButton ID="link_vitals" runat="server" OnClick="link_vit_Click">Vitals</asp:LinkButton>
     </li>
    <li>
         <asp:LinkButton ID="link_lab" runat="server" OnClick="link_lab_Click">Lab Results</asp:LinkButton>
     </li>
    <li>
         <asp:LinkButton ID="link_pres" runat="server" OnClick="link_pres_Click">Prescribe Medicines</asp:LinkButton>
     </li>
</ul>
</div>
</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="mulview_doctor" runat="server">
                        <table class="auto-style1">
                            <tr>
                                <td>
                                    <asp:View ID="view_observations" runat="server">
                                        <table class="auto-style2">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_observ" runat="server" Font-Size="Large" ForeColor="#FFCCCC">No Records Found</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridView_Obsv" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" DataKeyNames="observid" OnPageIndexChanging="GridView_Obsv_PageIndexChanging" Width="800px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                        <RowStyle ForeColor="#000066" />
                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                        <Columns>
                                                            <asp:BoundField DataField="particulars" HeaderStyle-HorizontalAlign="Center" HeaderText="Particulars " ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="observation" HeaderStyle-HorizontalAlign="Center" HeaderText="Observation" ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                           <asp:BoundField DataField="remarks" HeaderStyle-HorizontalAlign="Center" HeaderText="Remarks" ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="obserdate" HeaderStyle-HorizontalAlign="Center" HeaderText="Date of Observation" ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="empname" HeaderStyle-HorizontalAlign="Center" HeaderText="Observed By" ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_injections" runat="server">
                                        <table class="auto-style1">
                                            <tr><td>
                                                <asp:Label ID="lbl_inj" runat="server" Font-Size="Large" ForeColor="#FFCCCC">No Records Found</asp:Label>
                                                </td></tr>
                                            <tr>
                                                <td><asp:GridView ID="GridView_InjReq" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" Width="800px" DataKeyNames="injecreqid" OnPageIndexChanging="GridView_InjReq_PageIndexChanging" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" >
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                        <Columns>
                                    <asp:BoundField DataField="injectionname" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Particulars ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                 <asp:BoundField DataField="dateofinjres" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Time of Injection" >
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                 <asp:BoundField DataField="empname" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Administered By" >
                                                               <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                                               </Columns>
                    </asp:GridView></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_vitals" runat="server">
                                        <table>
                                             <tr>
            <td class="auto-style1">
                <asp:Label ID="lbl_vit" runat="server" Font-Size="Large" ForeColor="#FFCCCC">No Records Found</asp:Label>
                                                 </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:GridView ID="GridView_Vitals" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" OnPageIndexChanging="GridView_Vitals_PageIndexChanging" PageSize="50" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                    <Columns>
                        <asp:BoundField DataField="vital" HeaderStyle-HorizontalAlign="Center" HeaderText="Particulars" ItemStyle-HorizontalAlign="Center" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="value" HeaderStyle-HorizontalAlign="Center" HeaderText="Value" ItemStyle-HorizontalAlign="Center" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="empname" HeaderStyle-HorizontalAlign="Center" HeaderText="Taken By" ItemStyle-HorizontalAlign="Center" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dateofvital" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center" >

                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:BoundField DataField="remarks" HeaderStyle-HorizontalAlign="Center" HeaderText="Remarks" ItemStyle-HorizontalAlign="Center" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
        </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr><td>
                                                    <asp:View ID="view_prescription" runat="server">
                                                        
                   <table >
                       <tr>
                           <td >&nbsp;</td>
                           <td >
                               &nbsp;</td>

                       </tr>
                       <tr>
                           <td>Drug/Ointment Name:</td>
                           <td>
                               <asp:DropDownList ID="ddl_pres" runat="server">
                               </asp:DropDownList>
                           </td>
                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td >Potency (in ml):</td>
                           <td >
                               <asp:TextBox ID="txt_potency" CssClass="twitter" runat="server"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td >Dosage (no of pills per intake):</td>
                           <td >
                               <asp:TextBox ID="txt_dose" CssClass="twitter" runat="server"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td>Frequency (no of times a day):</td>
                           <td>
                               <asp:TextBox ID="txt_freq" cssclass="twitter" runat="server"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td>No of days:</td>
                           <td>
                               <asp:TextBox ID="txt_days" CssClass="twitter" runat="server"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td >Remarks:</td>
                           <td>
                               <asp:TextBox ID="txt_remarks" runat="server" CssClass="twitter" Height="58px" TextMode="MultiLine" Width="203px"></asp:TextBox>

                           </td>

                       </tr>
                       <tr>
                           <td></td>
                           <td></td>

                       </tr>
                       <tr>
                           <td>
               <asp:Button ID="btn_add" runat="server" CssClass="btn" OnClick="btn_add_Click" Text="Add"  />

                           </td>
                           <td>
                               <asp:Button ID="btn_ref" CssClass="btn" runat="server" OnClick="btn_ref_Click" Text="Refresh" />

                           </td>

                       </tr>
                       <tr>
                           <td ></td>
                           <td></td>

                       </tr>
                       <tr>
                           <td ></td>
                           <td></td>

                       </tr>

                   </table>
                   <table>
                       <tr>
                           <td>
                               <asp:GridView ID="gridview_pres" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="auto-style2" DataKeyNames="presid" Height="182px" 
                                   OnRowCancelingEdit="gridview_pres_RowCancelingEdit" 
                                   OnRowDeleting="gridview_pres_RowDeleting" 
                                   OnRowEditing="gridview_pres_RowEditing" 
                                   OnRowUpdating="gridview_pres_RowUpdating" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                   <Columns>
                                       <asp:BoundField DataField="presid" HeaderText="Prescription Id" />
                                       <asp:BoundField DataField="MedName" HeaderText="Medicine Name" />
                                       <asp:BoundField DataField="potency" HeaderText="Potency" />
                                       <asp:BoundField DataField="dosage" HeaderText="Dosage" />
                                       <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                       <asp:BoundField DataField="no_of_days" HeaderText="No of Days" />
                                       <asp:CommandField ShowDeleteButton="True" />
                                       <asp:CommandField ShowEditButton="True" />
               </Columns>
               <FooterStyle BackColor="White" ForeColor="#000066" />
               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"></HeaderStyle>
               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               <RowStyle ForeColor="#000066" />
               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
               <sortedascendingcellstyle backcolor="#F1F1F1" />
               <sortedascendingheaderstyle backcolor="#007DBB" />
               <sorteddescendingcellstyle backcolor="#CAC9C9" />
               <sorteddescendingheaderstyle backcolor="#00547E" />
               </asp:GridView></td></tr></table>
                                                    </asp:View></td></tr>
                            <tr><td>
                                <asp:View ID="View_caslab" runat="server">
                                    <table class="auto-style46">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_caslab" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GridView_CasLab_Result" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_CasLab_Result_PageIndexChanging" Width="1000px">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <sortedascendingcellstyle backcolor="#F1F1F1" />
                                                    <sortedascendingheaderstyle backcolor="#007DBB" />
                                                    <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                                    <sorteddescendingheaderstyle backcolor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="testname" HeaderText="Test" />
                                                        <asp:BoundField DataField="result" HeaderText="Result" />
                                                        <asp:BoundField DataField="dateofres" HeaderText="Date Of Test" />
                                                        <asp:BoundField DataField="empname" HeaderText="Requested By" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                                </td></tr>
                        </table>
                    </asp:MultiView>
                </td>
            </tr>
            </table>
    </form>
</asp:Content>


