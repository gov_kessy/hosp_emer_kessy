﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOHomeMaster.master" AutoEventWireup="true" CodeFile="ScheduleDepts.aspx.cs" Inherits="RMO_ScheduleDepts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table class="nav-justified">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div class="drop">
<ul class="drop_menu">
<li><asp:LinkButton ID="link_view" runat="server" OnClick="link_view_Click">View Department Schedule</asp:LinkButton></li>
 <li>   <asp:LinkButton ID="link_create" runat="server" OnClick="link_create_Click">Create New Schedule</asp:LinkButton></li>
  </ul>
</div></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:MultiView ID="MultiView1" runat="server">
                    <table class="nav-justified">
                        <tr>
                            <td>
                                <asp:View ID="View1" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_no" runat="server" Font-Size="Large" ForeColor="#FF0066" Text="No Schedules Found"></asp:Label>
                                                <asp:GridView ID="GridView_Deptview" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_Deptview_PageIndexChanging" Width="800px">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="deptname" HeaderStyle-HorizontalAlign="Center" HeaderText="Department Name" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day Of Week" ItemStyle-HorizontalAlign="Center" />
                                                        
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View2" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl_sure" runat="server" Font-Size="Large" ForeColor="#CC0000" Text="Are you sure you want to change all the prevoius schedules??"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Button ID="btn_yes" runat="server" CssClass="btn" OnClick="btn_yes_Click" Text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <div id="invisible">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_dept" runat="server" Text="Select Department"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddl_dept" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_day" runat="server" Text="Select Day Of Week"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddl_day" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1"></td>
                                            <td class="auto-style1"></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">&nbsp;</td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                                                                <asp:Button ID="btn_add" runat="server" CssClass="btn" Text="Add" OnClick="btn_add_Click" />

                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="GridView_create" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_create_PageIndexChanging" Width="800px">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="deptname" HeaderStyle-HorizontalAlign="Center" HeaderText="Department Name" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="day" HeaderStyle-HorizontalAlign="Center" HeaderText="Day Of Week" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                            </div>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                    </table>
                </asp:MultiView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
</asp:Content>

