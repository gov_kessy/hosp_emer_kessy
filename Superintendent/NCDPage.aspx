﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Superintendent/SuperintendentHomeMaster.master" AutoEventWireup="true" CodeFile="NCDPage.aspx.cs" Inherits="Superintendent_NCDPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
        .auto-style2 {
            height: 53px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table class="nav-justified">
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Duration of Data needed&nbsp;</td>
            <td>
                                                <asp:DropDownList ID="ddl_docdur" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_docdur_SelectedIndexChanged1" >
                                                </asp:DropDownList>
                                            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl_selyr" runat="server" Text="Select Year"></asp:Label>
                                            </td>
            <td>
                                                <asp:DropDownList ID="ddl_docyear" runat="server">
                                                </asp:DropDownList>
                                            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl_selmon" runat="server" Text="Select Month"></asp:Label>
                                            &nbsp;</td>
            <td>
                                                <asp:DropDownList ID="ddl_mondoc" runat="server">
                                                </asp:DropDownList>
                                            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">  <div class="drop">
<ul class="drop_menu">
   <li>
            <asp:LinkButton ID="link_doc" runat="server" OnClick="link_doc_Click">Doctor Statistics</asp:LinkButton></li>
  <li>
            <asp:LinkButton ID="link_dept" runat="server" OnClick="link_dept_Click">Department Statistics</asp:LinkButton></li>
  
    <li><a href="#">Patient Statistics</a>
         <ul>
        <li>
            <asp:LinkButton ID="link_age" runat="server" OnClick="link_age_Click">By Age</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_gen" runat="server" OnClick="link_gen_Click">By Gender</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_dis" runat="server" OnClick="link_dis_Click">By Disease</asp:LinkButton></li>
     <li>
            <asp:LinkButton ID="link_prev" runat="server" OnClick="link_prev_Click">By Disease Detetion</asp:LinkButton></li>
     
        </ul>
    </li>
          
      </ul>
</div></td>
        </tr>
        <tr>
            <td colspan="2"><cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:MultiView ID="MultiView1" runat="server">
                    <table class="nav-justified">
                        <tr>
                            <td>
                                <asp:View ID="View1" runat="server">
                                     <br /><br /><br />
                                    <div>
                                                         <ul id="sti-menu" class="sti-menu">
	        		<li data-hovercolor="#fff">
					<a href="#">
						<h4 data-type="mText" class="sti-item">View Statistics</h4>
						<%--<p data-type="sText" class="sti-item"></p>--%>
						<span data-type="icon" class="sti-icon glyphicon glyphicon-eye-open sti-item"></span>
						<span data-type="icon" class="gly"></span>

					</a>
				</li></ul>
                                    </div>
                                
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View2" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Select Doctor&nbsp;</td>
                                            <td>
                                                <asp:DropDownList ID="ddl_doc" runat="server" AutoPostBack="True" CssClass="twitter"  >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:LinkButton ID="link_viewdoc" runat="server" OnClick="link_viewdoc_Click">View Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number of Medical Certificates issued</td>
                                            <td>
                                                <asp:Label ID="lbl_med" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number of Patients Treated</td>
                                            <td>
                                                <asp:Label ID="lbl_pat" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="link_dispdoc" runat="server" OnClick="link_dispdoc_Click">Display Chart Data</asp:LinkButton>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopupExtender1" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopup" TargetControlID="link_dispdoc"  BackgroundCssClass="modalBackground" CancelControlID = "btn_okay"></cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" >
    <div class="header">
        Staistics by Doctor</div>
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart1" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_okay" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                       <tr>
                            <td>
                                <asp:View ID="View3" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:LinkButton ID="link_deptview0" runat="server" OnClick="link_deptview_Click">View Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number of Medical Certificates issued</td>
                                            <td>
                                                <asp:Label ID="lbl_meddept" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number of Patients Treated</td>
                                            <td>
                                                <asp:Label ID="lbl_patdept" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                       
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="link_dispdept" runat="server" OnClick="link_dispdept_Click">Display Chart Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        
                                       <tr>
                                            <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopupExtender2" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopupdept" TargetControlID="link_dispdept"  BackgroundCssClass="modalBackground" CancelControlID = "btn_okaydept">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopupdept" runat="server" CssClass="modalPopup" >
    <div class="header">
        Staistics by Year
    </div>
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart2" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_okaydept" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View4" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:LinkButton ID="link_genview" runat="server" OnClick="link_genview_Click">View Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Males</td>
                                            <td>
                                                <asp:Label ID="lbl_male" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Female&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lbl_female" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                        </tr>
                                       <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="link_dispgen" runat="server" OnClick="link_dispgen_Click">Display Chart Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        
                                       <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopup3" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopupgen" TargetControlID="link_dispgen"  BackgroundCssClass="modalBackground" CancelControlID = "btn_okaygen">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopupgen" runat="server" CssClass="modalPopup" >
    <div class="header">
        Staistics by Gender</div>
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart3" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_okaygen" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                       <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View5" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr><td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td></tr>
                                      <tr>
                                            <td class="auto-style1">&nbsp;</td>
                                            <td class="auto-style1">
                                                <asp:LinkButton ID="link_ageview" runat="server" OnClick="link_ageview_Click">View Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Patients with&nbsp;&nbsp;</td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age between 1 and 20</td>
                                            <td class="auto-style1">
                                                <asp:Label ID="lbl_bet120" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age between 21 and 40</td>
                                            <td>
                                                <asp:Label ID="lbl_bet2140" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age between 41 and 60</td>
                                            <td>
                                                <asp:Label ID="lbl_bet4160" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age greater than 60</td>
                                            <td>
                                                <asp:Label ID="lbl_greater60" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                               <asp:LinkButton ID="link_dispage" runat="server" OnClick="link_dispage_Click">Display Chart Data</asp:LinkButton>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopupExtender4" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopupagebet" TargetControlID="link_dispage"  BackgroundCssClass="modalBackground" CancelControlID = "btn_agebet">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopupagebet" runat="server" CssClass="modalPopup" >
    <div class="header">
        Staistics by Age</div>
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart4" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_agebet" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                       <tr>
                            <td>
                                <asp:View ID="View6" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                        </tr>
                                      <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:LinkButton ID="link_viewdis" runat="server" OnClick="link_viewdis_Click">View Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BP</td>
                                            <td>
                                                <asp:Label ID="lbl_bp" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Diabetes</td>
                                            <td>
                                                <asp:Label ID="lbl_diabetes" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cholestrol</td>
                                            <td>
                                                <asp:Label ID="lbl_choles" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cerebro Vascular Accidents</td>
                                            <td>
                                                <asp:Label ID="lbl_cva" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hypothyroidism&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lbl_hypo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Coronary Artery Disease&nbsp;&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lbl_cad" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="link_dispdis" runat="server" OnClick="link_dispdis_Click">Display Chart </asp:LinkButton>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            
                                            <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopupExtender5" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopupdisease" TargetControlID="link_dispdis"  BackgroundCssClass="modalBackground" CancelControlID = "btn_disease">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopupdisease" runat="server" CssClass="modalPopup" >
    <div class="header">
        Staistics by Disease</div>
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart5" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_disease" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                       
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <asp:View ID="View7" runat="server">
                                    <table class="nav-justified">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:LinkButton ID="link_viewpre" runat="server" OnClick="link_viewpre_Click">View Data</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Previous Detected Patients</td>
                                            <td>
                                                <asp:Label ID="lbl_pre" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Newly Detected Patients</td>
                                            <td>
                                                <asp:Label ID="lbl_new" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="link_disppre" runat="server" OnClick="link_disppre_Click">Display Chart </asp:LinkButton>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><cc1:ModalPopupExtender ID="ModalPopupExtender6" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopupdet" TargetControlID="link_disppre"  BackgroundCssClass="modalBackground" CancelControlID = "btn_det">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopupdet" runat="server" CssClass="modalPopup" >
    <div class="header">
        Staistics by Disease Detection</div>
    <div class="popup_Body">
    <p>&nbsp;<cc1:LineChart ID="LineChart6" runat="server" ChartHeight="300" ChartWidth = "450"
    ChartType="Basic" ChartTitleColor="#0E426C" Visible = "false"
    CategoryAxisLineColor="#D08AD9" ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB"></cc1:LineChart>
        <p>
        </p>
        <asp:Button ID="btn_det" runat="server" CssClass="btn" Text="Okay" />
        <p>
        </p>
        <p>
        </p>
    </p>         
    </div>
</asp:Panel>

                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                    
                    </table>
                </asp:MultiView>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" colspan="2"></td>
        </tr>
    </table>
    </form>
</asp:Content>

