﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/CasualityPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Pharmacy_CasualityPharmacy_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            font-size: medium;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="lbl_title" runat="server" CssClass="auto-style3"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Enter Old Password:</td>
                <td>
                    <asp:TextBox ID="txt_old" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:Label ID="lbl_old" runat="server" style="font-size: medium"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_old" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Enter New Password:</td>
                <td>
                    <asp:TextBox ID="txt_new" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="#CC0000" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&amp;])[A-Za-z\d$@$!%*#?&amp;]{8,}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Retype New Password:</td>
                <td>
                    <asp:TextBox ID="txt_retype" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:Label ID="lbl_retype" runat="server" style="font-size: medium"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2"></td>
                <td class="auto-style2"></td>
            </tr>
            <tr>
                <td> 
                    <asp:Button ID="btn_chg" runat="server" CssClass="btn" OnClick="btn_chg_Click" Text="Change Password"  />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" CssClass="btn" OnClick="btn_cancel_Click" Text="Cancel" />
                </td>
            </tr>
           
        </table>
    <div>
    
    </div>
    </form>

</asp:Content>

