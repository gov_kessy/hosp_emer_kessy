﻿//Allocate patient to bed in casuality
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class AllocatePage : System.Web.UI.Page
{
    conclass c = new conclass();
    int bid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Casuality/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }           
            
        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

        String s = "insert into CasualityBedAllocation values('" + bid + "','" + txt_patid.Text + "','" + DateTime.Now + "',' ','Treating',' ',' ')";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();
        c.Con.Close();
        Response.Redirect("CasualityHome.aspx");
    }
    protected void btn_patfind_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd111 = new SqlCommand("select * from patientdetails where patientid= '" + txt_patid.Text + "' ", c.Con);
        SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
        DataTable dt111 = new DataTable();
        sda111.Fill(dt111);

        int k = cmd111.ExecuteNonQuery();

        if (dt111.Rows.Count > 0)
        {
            DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];
            String pname = Convert.ToString(row11[1]);
            DateTime dob = Convert.ToDateTime(row11[7]);
            String gender = Convert.ToString(row11[14]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_age.Text = Convert.ToString(age) + " years";
            lbl_gen.Text = gender;
            lbl_name.Text = pname;

        }
    }
}