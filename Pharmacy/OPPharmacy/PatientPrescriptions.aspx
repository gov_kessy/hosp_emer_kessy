﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pharmacy/OPPharmacy/PharmaMaster.master" AutoEventWireup="true" CodeFile="PatientPrescriptions.aspx.cs" Inherits="Pharmacy_OPPharmacy_PatientPrescriptions" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 53px;
        }
        .auto-style3 {
            height: 20px;
        }
        .auto-style4 {
            height: 23px;
        }
    </style>
    <%-- <script language="javascript" type="text/javascript">
         function CallPrint(strid) {
             var prtContent = document.getElementById(strid);
             var WinPrint = window.open('', '', 'letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
             WinPrint.document.write(prtContent.innerHTML);
             WinPrint.document.close();
             WinPrint.focus();
             WinPrint.print();
             WinPrint.close();
             prtContent.innerHTML = strOldOne;
         }
</script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
             <tr><td class="auto-style4"><cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager></td><td class="auto-style4"></td></tr>
           
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Patient Id</td>
                <td>
                    <asp:TextBox ID="txt_patno" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Enter Patient Id" ForeColor="#CC0000" ControlToValidate="txt_patno"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btn_srch" runat="server" Text="Search" OnClick="btn_srch_Click" CssClass="btn" />
                </td>
                <td class="auto-style2"></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
             <tr>
                <td class="auto-style3">
                    <asp:LinkButton ID="link_print" runat="server" onclientclick="javascript:CallPrint('print');" OnClick="link_print_Click">Print Prescriptions</asp:LinkButton>
                </td>
                <td class="auto-style3"></td>
            </tr>
             
            <tr>
                <td colspan="2">
                    <%--<asp:Label ID="lbl_pre" runat="server" ForeColor="#FF5050" Text="No Prescriptions Found"></asp:Label>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                               <asp:GridView ID="gridview_pres" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="auto-style2" DataKeyNames="presid" Height="182px" 
                                    Width="1000px" OnRowCommand="gridview_pres_RowCommand" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" HorizontalAlign="Center">
                                   <Columns>
                                       <asp:BoundField DataField="MedName" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Medicine Name" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                       <asp:BoundField DataField="potency" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Potency" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                       <asp:BoundField DataField="dosage" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Dosage" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                        <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >
                                <ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>   
               </Columns>
               <FooterStyle BackColor="White" ForeColor="#000066" />
               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"></HeaderStyle>
               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               <RowStyle ForeColor="#000066" />
               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
               <sortedascendingcellstyle backcolor="#F1F1F1" />
               <sortedascendingheaderstyle backcolor="#007DBB" />
               <sorteddescendingcellstyle backcolor="#CAC9C9" />
               <sorteddescendingheaderstyle backcolor="#00547E" />
               </asp:GridView></td>
            </tr>
            <tr>
                <td colspan="2">
                               &nbsp;</td>
            </tr></table>
       
        <%-- <div id="bill">
           
                <table id="tab_print" runat="server" style="font-size: medium; width: 640px;"> 
                    <tr>
                    <td > &nbsp;</td>
                    <td> &nbsp;</td>
                        </tr>
                    <tr>
                    <td >
                        <asp:Image ID="img_logo" runat="server" height="100" Width="100" ImageUrl="~/OPD/Registration/image/gov.png"/></td>
                                        <td >Government General Hospital
                                            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Changanacherry</td>    </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                     <tr>
                    <td >Date:</td>
                    <td >
                        <asp:Label ID="lbl_date" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                     <tr>
                    <td >Patient No:</td>
                    <td >
                        <asp:Label ID="lbl_patno" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                     <tr>
                     <td >Patient Name:</td>
                    <td >
                        <asp:Label ID="lbl_pname" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                      <tr>
                    <td >Age:</td>
                    <td>
                        <asp:Label ID="lbl_age" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
                     <tr>
                <td colspan="2">
      <asp:GridView ID="gridview1" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="auto-style2" DataKeyNames="presid" Height="182px" 
                                    Width="1000px" OnRowCommand="gridview_pres_RowCommand" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                   <Columns>
                                       <asp:BoundField DataField="MedName" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Medicine Name" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                       <asp:BoundField DataField="potency" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Potency" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                       <asp:BoundField DataField="dosage" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Dosage" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                        <asp:BoundField DataField="frequency" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Frequency" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                        <asp:BoundField DataField="remarks" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Instructions" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                       </asp:BoundField>
                                          
               </Columns>
               <FooterStyle BackColor="White" ForeColor="#000066" />
               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"></HeaderStyle>
               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               <RowStyle ForeColor="#000066" />
               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
               <sortedascendingcellstyle backcolor="#F1F1F1" />
               <sortedascendingheaderstyle backcolor="#007DBB" />
               <sorteddescendingcellstyle backcolor="#CAC9C9" />
               <sorteddescendingheaderstyle backcolor="#00547E" />
               </asp:GridView></td>
            </tr>
        
</table>
                
              </div>
        
    --%>
    </form>
</asp:Content>

