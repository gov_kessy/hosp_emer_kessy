﻿//view stock of each injection

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Data.SqlClient;
public partial class Pharmacy_CasualityPharmacy_ViewStockMeds : System.Web.UI.Page
{
    conclass c = new conclass();
    int medid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLabels();
            BindGridView();
            BindChart();
            if (Session["Id"] == null)
                Response.Redirect("~/Pharmacy/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }

    }
    private void BindGridView()
    {
        c.getCon();
        medid = Convert.ToInt32(Request.QueryString["MedNo"]);

        SqlCommand cmd = new SqlCommand("select m.MedId,m.MedName,s.stockvalue,b.batchexpdate from MedMaster m inner join CasualityStockMedicines s on s.medid=m.MedId inner join BatchCasuality b on b.batchid=s.batchid where m.MedId='" + medid + "' order by MedName", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        c.Con.Close();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    public void BindChart()
    {
        c.getCon();
        SqlCommand cmd_chart = new SqlCommand("select sum(orderamt),Year(dateoforder)  from CasualityMedOrder where medid='" + lbl_medid.Text + "' and status='Fullfilled' group by Year(dateoforder)", c.Con);
        SqlDataAdapter sda_chart = new SqlDataAdapter(cmd_chart);
        DataTable dt = new DataTable();
        sda_chart.Fill(dt);
        int k_pat = cmd_chart.ExecuteNonQuery();
        int amt = 0;
        if (dt.Rows.Count > 0)
        {
            string[] x = new string[dt.Rows.Count];
            decimal[] y = new decimal[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                int count = Convert.ToInt32(dt.Rows[i][0]);
                string date = Convert.ToString(dt.Rows[i][1]);
                amt = amt + count;

                x[i] = Convert.ToString(date);
                y[i] = count;
            }
            amt = amt / dt.Rows.Count;
            lbl_est.Text = amt.ToString();
            //LineChart1.Series[0].Points.DataBindXY(x, y);
            //lbl_est.Text = amt.ToString();
            LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y });
            LineChart1.CategoriesAxis = string.Join(",", x);
            LineChart1.ChartTitle = string.Format("{0} ", "Previous Orders");
            if (x.Length > 3)
            {
                LineChart1.ChartWidth = (x.Length * 75).ToString();
            }
            LineChart1.Visible = true;
        }

    }
    public void BindLabels()
    {
        medid = Convert.ToInt32(Request.QueryString["MedNo"]);
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select i.MedId,i.MedName  from MedMaster i where MedId= '" + medid + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);
        int k_pat = cmd_pat.ExecuteNonQuery();
        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];
            String iname = Convert.ToString(row_pat[1]);
            int id = Convert.ToInt32(row_pat[0]);
            lbl_medid.Text = id.ToString();
            lbl_medname.Text = iname;


        }
        c.Con.Close();
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s_in = "insert into CasualityMedOrder values('" + lbl_medid.Text + "','" + txt_amount.Text + "','" + DateTime.Today + "','Pending',' ')";
        SqlCommand cmdsin = new SqlCommand(s_in, c.Con);

        cmdsin.ExecuteNonQuery();

        c.Con.Close();
        Response.Redirect("PlaceOrdersMeds.aspx");
    }

    protected void link_previous_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Show();
    }
}