﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FeverClinic/Doctor/FeverClinicDocMaster.master" AutoEventWireup="true" CodeFile="PatientConsult.aspx.cs" Inherits="FeverClinic_Doctor_PatientConsult" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<script runat="server">

    
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    
    
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" >
    <form id="form1" runat="server" >
        <center>
        <table>
            <tr>
                
                <td>

    <div id="Div1" runat="server">  
        
                        <table>
                            
                            <tr><td></td><td></td></tr> <tr>
                                <td style="text-align: left">Patient Number:</td>
                                <td>
                                    <asp:Label ID="lbl_pno" runat="server" ForeColor="#999966"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <asp:Label ID="lbl_name" runat="server" ForeColor="#999966"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Age:</td>
                                <td>
                                    <asp:Label ID="lbl_age" runat="server" ForeColor="#999966"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Gender:</td>
                                <td>
                                    <asp:Label ID="lbl_gen" runat="server" ForeColor="#999966"></asp:Label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td class="auto-style44"></td>
                                <td class="auto-style44">
                                    &nbsp;
                                    <asp:LinkButton ID="link_completed" runat="server" OnClick="link_completed_Click" ForeColor="#999966" CssClass="twitter">Treatment Completed</asp:LinkButton>
                                </td>
                            </tr>
                           
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                           
                            <tr>
                                <td colspan="2">
                                    &nbsp;</td>
                            </tr>
                           
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                           <td colspan="2">
                            <div class="drop">
<ul class="drop_menu">
<li><a href="#">Treat Patient</a>
<ul>
<li>
            <asp:LinkButton ID="link_diagnosis" runat="server" OnClick="link_diagnosis_Click">Diagnose</asp:LinkButton></li>
       
        <li>
            <asp:LinkButton ID="link_prescription" runat="server" OnClick="link_prescription_Click">Prescribe Medicines</asp:LinkButton></li>
        
        <li>
            <asp:LinkButton ID="link_lab" runat="server" OnClick="link_lab_Click">Lab Test</asp:LinkButton></li>
       <li>
            <asp:LinkButton ID="link_injop" runat="server" OnClick="link_injop_Click">Injections</asp:LinkButton></li>
  </ul>
    </li>
    <li><a href="#">Previous Visits</a>
        <ul>
        <li>
            <asp:LinkButton ID="link_opdiag" runat="server" OnClick="link_opdiag_Click">Diagnosis</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_oppres" runat="server" OnClick="link_oppres_Click">Prescription</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_oplabres" runat="server" OnClick="link_oplabres_Click">Lab Results</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_opinj" runat="server" OnClick="link_opinj_Click">Injections</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_opvit" runat="server" OnClick="link_opvit_Click">Vitals</asp:LinkButton></li>
      
        </ul>
    </li>
    <li><a href="#">Patient History</a>
        <ul>
        <li>
            <asp:LinkButton ID="link_family" runat="server" OnClick="link_family_Click">Family History</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_personal" runat="server" OnClick="link_personal_Click">Personal History</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_past" runat="server" OnClick="link_past_Click">Past History</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_allergies" runat="server" OnClick="link_allergies_Click">Allergies</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_vaccinations" runat="server" OnClick="link_vaccinations_Click">Vaccinations</asp:LinkButton></li>
      
        </ul>
    </li>
      <li><a href="#">Take Patient History</a>
         <ul>
        <li>
            <asp:LinkButton ID="link_take_fam" runat="server" OnClick="link_take_fam_Click">Family History</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_take_pers" runat="server" OnClick="link_take_pers_Click">Personal History</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_take_past" runat="server" OnClick="link_take_past_Click">Past History</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_take_aller" runat="server" OnClick="link_take_aller_Click">Allergies</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_take_vacc" runat="server" OnClick="link_take_vacc_Click">Vaccinations</asp:LinkButton></li>
      
        </ul>
    </li>
    <li><a href="#">NCD Treatment History</a>
        
      <ul>

        <li>
            <asp:LinkButton ID="link_prediag" runat="server" OnClick="link_prediag_Click">Previous Diagnosis</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_prepres" runat="server" OnClick="link_prepres_Click">Previous Prescriptions</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_prelab" runat="server" OnClick="link_prelab_Click">Lab Results</asp:LinkButton></li>
          <li>
            <asp:LinkButton ID="link_vitals" runat="server" OnClick="link_vitals_Click">Vital Signs</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_rbs" runat="server" OnClick="link_rbs_Click">RBS Levels</asp:LinkButton></li>
        
      </ul> 
    </li>
    <li><a href="#">Casuality Treatment History</a>
         <ul>
        <li>
            <asp:LinkButton ID="link_casdiag" runat="server" OnClick="link_casdiag_Click">Diagnosis</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_caspres" runat="server" OnClick="link_caspres_Click">Prescription</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="link_labres" runat="server" OnClick="link_labres_Click">Lab Results</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_inj" runat="server" OnClick="link_inj_Click">Injections</asp:LinkButton></li>
      <li>
            <asp:LinkButton ID="link_vit" runat="server" OnClick="link_vit_Click">Vitals</asp:LinkButton></li>
      
        </ul>
    </li>
          
      <li>
            <asp:LinkButton ID="link_refer" runat="server" OnClick="link_refer_Click">Referrals</asp:LinkButton></li>
  </ul>
</div>

                            </td></tr>
            <tr><td></td><td></td></tr>
                            <tr>
                                <td colspan="2">
                <asp:MultiView ID="MultiView_Main" runat="server">
                <table class="auto-style2">
                <tr>
                <td>
               <asp:View ID="View_Diagnosis" runat="server">
               <table class="style1">
                   <tr>
                       <td class="auto-style22">&#160;</td>
                       <td>&#160;</td>

                   </tr>
                   <tr>
                       <td class="auto-style22">
                           &nbsp;</td>
                       <td>
                           &nbsp;</td>

                   </tr>
                   <tr>
                       <td class="auto-style22">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="auto-style22">
                           <asp:Label ID="lbl_prepblm" runat="server" Text="Presenting Problems:">

               </asp:Label>
                       </td>
                       <td>
                           <asp:TextBox ID="txt_present" runat="server" Height="47px" TextMode="MultiLine" Width="300px">

                           </asp:TextBox>
                       </td>
                   </tr>
                   <tr>
                       <td class="auto-style22">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="auto-style1">Provisional Diagnosis:&nbsp;&nbsp;&nbsp;&nbsp; </td>
                       <td>
                           <asp:TextBox ID="txt_diag" runat="server" ></asp:TextBox>
                   </td>

                   </tr>
                   <tr>
                       <td class="auto-style1">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="auto-style1">Remarks:</td>
                       <td>
                           <asp:TextBox ID="txt_remarkdiag" runat="server" Height="47px" TextMode="MultiLine" Width="300px">

                           </asp:TextBox>

                       </td>

                   </tr>
                   <tr>
                       <td class="auto-style22">&#160;</td>
                       <td>&#160;</td>

                   </tr>
                   <tr>
                       <td class="auto-style22">
                           <asp:Button ID="btn_adddiag" runat="server" OnClick="btn_adddiag_Click" CssClass="btn" Text="Add" />

                       </td>
                       <td>
                           <asp:Button ID="btn_refreshdiag" runat="server" CssClass="btn" OnClick="btn_refreshdiag_Click" Text="Refresh" />

                       </td>

                   </tr>
                   <tr>
                       <td class="auto-style22">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="auto-style22">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="auto-style22">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
               </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_prescription" runat="server">
                                                        
                   <table class="style1">
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style11">
                   &nbsp;</td>

                       </tr>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style11">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style11">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">Drug/Ointment Name:</td>
                           <td class="auto-style11">
                               <asp:DropDownList ID="ddl_pres" runat="server">
                               </asp:DropDownList>
                               &nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style11">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">Potency (in ml):</td>
                           <td class="auto-style10">
                               <asp:TextBox ID="txt_potency" runat="server" CssClass="twitter"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style10">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">Dosage (no of pills per intake):</td>
                           <td class="auto-style2">
                               <asp:TextBox ID="txt_dose" runat="server" CssClass="twitter"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style2">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1" width="400">Frequency (no of times a day):</td>
                           <td class="auto-style29">
                               <asp:TextBox ID="txt_freq" runat="server" CssClass="twitter"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td class="auto-style1" width="400">&nbsp;</td>
                           <td class="auto-style29">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">No of days:</td>
                           <td class="auto-style34">
                               <asp:TextBox ID="txt_days" runat="server" CssClass="twitter"></asp:TextBox>
                       </td>

                       </tr>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style34">&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style1">Remarks:</td>
                           <td class="auto-style13">
                               <asp:TextBox ID="txt_remarks" runat="server" Height="58px" TextMode="MultiLine" Width="203px" CssClass="twitter"></asp:TextBox>

                           </td>

                       </tr>
                       <tr>
                           <td class="auto-style1"></td>
                           <td></td>

                       </tr>
                       <tr>
                           <td class="auto-style32">
               <asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add"  CssClass="btn" />

                           </td>
                           <td>
                               <asp:Button ID="btn_ref" runat="server" OnClick="btn_ref_Click" Text="Refresh" CssClass="btn" />

                           </td>

                       </tr>
                       <tr>
                           <td class="auto-style32"></td>
                           <td></td>

                       </tr>
                       <tr>
                           <td class="auto-style32">&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style32">&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td class="auto-style32"></td>
                           <td></td>

                       </tr>

                   </table>
                   <table>
                       <tr>
                           <td>
                               <asp:GridView ID="gridview_pres" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="auto-style2" DataKeyNames="presid" Height="182px" 
                                   OnRowCancelingEdit="gridview_pres_RowCancelingEdit" 
                                   OnRowDeleting="gridview_pres_RowDeleting" 
                                   OnRowEditing="gridview_pres_RowEditing" 
                                   OnRowUpdating="gridview_pres_RowUpdating" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                   <Columns>
                                       <asp:BoundField DataField="presid" HeaderText="Prescription Id" />
                                       <asp:BoundField DataField="MedName" HeaderText="Medicine Name" />
                                       <asp:BoundField DataField="potency" HeaderText="Potency" />
                                       <asp:BoundField DataField="dosage" HeaderText="Dosage" />
                                       <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                       <asp:BoundField DataField="no_of_days" HeaderText="No of Days" />
                                       <asp:CommandField ShowDeleteButton="True" />
                                       <asp:CommandField ShowEditButton="True" />
               </Columns>
               <FooterStyle BackColor="White" ForeColor="#000066" />
               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"></HeaderStyle>
               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               <RowStyle ForeColor="#000066" />
               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
               <sortedascendingcellstyle backcolor="#F1F1F1" />
               <sortedascendingheaderstyle backcolor="#007DBB" />
               <sorteddescendingcellstyle backcolor="#CAC9C9" />
               <sorteddescendingheaderstyle backcolor="#00547E" />
               </asp:GridView></td></tr>
                       <tr>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                       </tr>
                                                        </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <asp:View ID="view_injections" runat="server">
                                                        <table>
                       <tr><td class="auto-style35"></td>
                           <td>&nbsp;</td>
                       </tr>
                        <tr><td class="auto-style1">Injection Name:</td>
                            <td>
                                <asp:TextBox ID="txt_inj" runat="server" CssClass="twitter"></asp:TextBox>
                            </td>
                       </tr>
                        <tr><td class="auto-style1">Dosage:</td>
                            <td>
                                <asp:TextBox ID="txt_injdose" runat="server" CssClass="twitter"></asp:TextBox>
                                <span class="auto-style33">&nbsp;<span class="auto-style1">mL</span></span></td>
                       </tr>
                        <tr><td class="auto-style1">Frequency:(Once or Specify Number of times)</td>
                            <td>
                                <asp:TextBox ID="txt_injfreq" runat="server" CssClass="twitter"></asp:TextBox>
                            </td>
                       </tr>
                        <tr><td class="auto-style1">Remarks:</td>
                            <td>
                                <asp:TextBox ID="txt_injrem" runat="server" Height="44px" TextMode="MultiLine" Width="213px" CssClass="twitter"></asp:TextBox>
                            </td>
                       </tr>
                       <tr><td class="auto-style35"></td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr><td class="auto-style35">
                           <asp:Button ID="btn_injadd" runat="server" Text="Add" OnClick="btn_injadd_Click" CssClass="btn" />
                           </td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr><td class="auto-style35"></td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr><td colspan="2">
                           <asp:GridView ID="GridView_Injection" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                               <Columns>
                                   <asp:BoundField DataField="injectionname" HeaderText="Medicine Name" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="type" HeaderText="Type" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="dosage" HeaderText="Dosage" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="frequency" HeaderText="Frequency" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="empname" HeaderText="Requested By" >
                                   <ItemStyle Width="25%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="remarks" HeaderText="Remarks" >
                                   <ItemStyle Width="35%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="dateofinj" HeaderText="Date Of Injection" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                               </Columns>
                               <FooterStyle BackColor="White" ForeColor="#000066" />
                               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                               <RowStyle ForeColor="#000066" />
                               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                               <SortedAscendingCellStyle BackColor="#F1F1F1" />
                               <SortedAscendingHeaderStyle BackColor="#007DBB" />
                               <SortedDescendingCellStyle BackColor="#CAC9C9" />
                               <SortedDescendingHeaderStyle BackColor="#00547E" />
                           </asp:GridView>
                           </td>
                       </tr>
                   </table>
               
                                                    </asp:View>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_labtest" runat="server">
                                                        <table>
                 <tr>
                       <td class="auto-style30">&nbsp;</td>
                       <td>&nbsp;</td>

                   </tr>
                                                            <tr>
                                                                <td class="auto-style30"></td>
                                                                <td></td>
                                                            </tr>
                   <tr>
                       <td class="auto-style1">&nbsp;</td>
                       <td class="auto-style4">
                           &nbsp;</td>

                   </tr>
                   <tr>
                       <td class="auto-style1">Test:</td>
                       <td>
                   <asp:DropDownList ID="ddl_test" runat="server" CssClass="twitter">
                   </asp:DropDownList>
                   </td>
                </tr>
                                                            <tr>
                                                                <td class="auto-style1">&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style1">Remarks:</td>
                   <td>
                       <asp:TextBox ID="txt_lab_remark" runat="server" Height="55px" TextMode="MultiLine" Width="243px" CssClass="twitter"></asp:TextBox>

                   </td>

               </tr>
                   <tr>
                       <td class="auto-style30">
                           <asp:Button ID="btn_lab_req" runat="server" OnClick="btn_lab_req_Click" Text="Add" CssClass="btn" />

                       </td>
                       <td>

                       </td>

                   </tr>
                   <tr>
                       <td class="auto-style30"></td>
                       <td></td>

                   </tr>
                   <tr>
                       <td colspan="2">
                           <asp:GridView ID="GridView_lab_request" runat="server" 
                               AutoGenerateColumns="False" CellPadding="3" DataKeyNames="labreqid" 
                               OnRowDeleting="GridView_lab_request_RowDeleting" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                               <Columns>
                                   <asp:BoundField DataField="labreqid" HeaderText="Lab request Id" />
                                   
                                   <asp:BoundField DataField="testname" HeaderText="Test Name" />
                                   <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                   <asp:CommandField ShowDeleteButton="True" />
               </Columns>
               <FooterStyle BackColor="White" ForeColor="#000066" />
               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               <RowStyle ForeColor="#000066" />
               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
               <sortedascendingcellstyle backcolor="#F1F1F1" />
               <sortedascendingheaderstyle backcolor="#007DBB" />
               <sorteddescendingcellstyle backcolor="#CAC9C9" />
               <sorteddescendingheaderstyle backcolor="#00547E" />
               </asp:GridView>

                       </td>

                   </tr>
                                                            <tr>
                                                                <td colspan="2">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">&nbsp;</td>
                                                            </tr>
                   <tr>
                       <td class="auto-style30"></td>
                       <td></td>

                   </tr>

               </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_prediag" runat="server">
                                                        <table>
                       <tr>
                           <td>
                               &nbsp;</td>

                       </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbl_response" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
                       <tr>
                           <td class="auto-style2">
                               &nbsp;</td>

                       </tr>
                                                            <tr>
                                                                <td class="auto-style2">
                                                                    <asp:GridView ID="GridView_Pre_Diag" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_Pre_Diag_PageIndexChanging" Width="1000px">
                                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                        <RowStyle ForeColor="#000066" />
                                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                        <sortedascendingcellstyle backcolor="#F1F1F1" />
                                                                        <sortedascendingheaderstyle backcolor="#007DBB" />
                                                                        <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                                                        <sorteddescendingheaderstyle backcolor="#00547E" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="symptoms" HeaderText="Symptoms" />
                                                                            <asp:BoundField DataField="diagnosis" HeaderText="Diagnosis" />
                                                                            <asp:BoundField DataField="empname" HeaderText="Diagnosed By" />
                                                                            <asp:BoundField DataField="complications" HeaderText="Complications" />
                                                                            <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                                                            <asp:BoundField DataField="dateofdiag" HeaderText="Date Of Diagnosis" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style2">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style2">&nbsp;</td>
                                                            </tr>
                       <tr><td></td></tr>

                   </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style20">
                                                    <asp:View ID="view_prepres" runat="server">
                                                        <table>
                   <tr>
                       <td class="auto-style28">
                           &nbsp;</td>
                   </tr>
                                                            <tr>
                                                                <td class="auto-style28">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style28">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style28">
                                                                    <asp:Label ID="lbl_response0" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
                   <tr>
                       <td class="auto-style14">
                           <asp:GridView ID="GridView_Pre_Pres" runat="server" AllowPaging="True" 
                               AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" 
                               BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                Width="1000px" >
                               <Columns>
                                   <asp:BoundField DataField="medname" HeaderText="Medicine Name" >
                                   <ItemStyle Width="20%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="potency" HeaderText="Potency" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="dosage" HeaderText="Dosage" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="frequency" HeaderText="Frequency" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="no_of_days" HeaderText="No of Days" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="empname" HeaderText="Prescribed By" >
                                   <ItemStyle Width="15%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="remarks" HeaderText="Remarks" >
                                   <ItemStyle Width="30%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="dateofpres" HeaderText="Date Of Prescription" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                               </Columns>
                               <FooterStyle BackColor="White" ForeColor="#000066" />
                               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                               <PagerSettings LastPageText="Last" />
                               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                               <RowStyle ForeColor="#000066" />
                               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                               <sortedascendingcellstyle backcolor="#F1F1F1" />
                               <sortedascendingheaderstyle backcolor="#007DBB" />
                               <sorteddescendingcellstyle backcolor="#CAC9C9" />
                               <sorteddescendingheaderstyle backcolor="#00547E" />
                           </asp:GridView>
                       </td>
                   </tr>
                   <tr>
                       <td class="auto-style23"></td>
                   </tr>
                                                            <tr>
                                                                <td class="auto-style23">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style23">j</td>
                                                            </tr>
                   <tr>
                       <td class="auto-style26"></td>
                   </tr>
               </table>
           
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_prelabres" runat="server">
                                                        <table>
                       <tr>
                           <td>
                               &nbsp;</td>

                       </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbl_response1" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
                       <tr>
                           <td>
                               <asp:GridView ID="GridView_Lab_Result" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_Lab_Result_PageIndexChanging" Width="1000px">
                                   <FooterStyle BackColor="White" ForeColor="#000066" />
                                   <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                   <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                   <RowStyle ForeColor="#000066" />
                                   <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                   <sortedascendingcellstyle backcolor="#F1F1F1" />
                                   <sortedascendingheaderstyle backcolor="#007DBB" />
                                   <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                   <sorteddescendingheaderstyle backcolor="#00547E" />
                                   <Columns>
                                       <asp:BoundField DataField="testname" HeaderText="Test" />
                                       <asp:BoundField DataField="result" HeaderText="Result" />
                                       <asp:BoundField DataField="dateofres" HeaderText="Date Of Test" />
                                       <asp:BoundField DataField="empname" HeaderText="Requested By" />
                                   </Columns>
                               </asp:GridView>

                           </td>

                       </tr>
                      

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                      

                   </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_vitals" runat="server">
                                                         <table>
                       <tr><td>
                           &nbsp;</td></tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>
                                                                     <asp:Label ID="lbl_response2" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                 </td>
                                                             </tr>
                         <tr><td>
                             &nbsp;</td></tr>
                                                             <tr>
                                                                 <td>
                                                                     <asp:GridView ID="GridView_vitals" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_vitals_PageIndexChanging" Width="1000px">
                                                                         <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                         <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                                         <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                         <RowStyle ForeColor="#000066" />
                                                                         <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                         <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                         <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                                         <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                         <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                                         <Columns>
                                                                             <asp:BoundField DataField="vital" HeaderText="Particulars " />
                                                                             <asp:BoundField DataField="value" HeaderText="Value" />
                                                                             <asp:BoundField DataField="normal" HeaderText="Normal Value" />
                                                                             <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                                                             <asp:BoundField DataField="dateofvital" HeaderText="Date" />
                                                                             <asp:BoundField DataField="empname" HeaderText="Checked By" />
                                                                         </Columns>
                                                                     </asp:GridView>
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                             <tr><td></td></tr>
                   </table>
               
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_famhis" runat="server">
                                                        <table>
                       <tr>
                           <td>&nbsp;</td>

                       </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                       <tr>
                           <td class="auto-style1">Illnesses/conditions which you know have occurred in your blood relatives (Yes/No)</td>

                       </tr>
                       <tr>
                           <td>
                               <asp:Label ID="lbl_response3" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                           </td>

                       </tr>
                       <tr>
                           <td>
                               <asp:GridView ID="GridView_Family" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                   <FooterStyle BackColor="White" ForeColor="#000066" />
                                   <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                   <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                   <RowStyle ForeColor="#000066" />
                                   <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                   <sortedascendingcellstyle backcolor="#F1F1F1" />
                                   <sortedascendingheaderstyle backcolor="#007DBB" />
                                   <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                   <sorteddescendingheaderstyle backcolor="#00547E" />
                                   <Columns>
                                       <asp:BoundField DataField="famillness" HeaderText="Particulars" />
                                       <asp:BoundField DataField="grandparents" HeaderText="Grand Parents" />
                                       <asp:BoundField DataField="father" HeaderText="Father" />
                                       <asp:BoundField DataField="mother" HeaderText="Mother" />
                                       <asp:BoundField DataField="brothers" HeaderText="Brothers" />
                                       <asp:BoundField DataField="sisters" HeaderText="Sisters" />
                                       <asp:BoundField DataField="sons" HeaderText="Sons" />
                                       <asp:BoundField DataField="daughters" HeaderText="Daughters" />
                                       <asp:BoundField DataField="others" HeaderText="None" />
                                       <asp:BoundField DataField="describe" HeaderText="Description" />
               </Columns>
               </asp:GridView>
                  </td>
               </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style2"></td>
                   </tr>
             </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_pasthis" runat="server">
                                                        <table>
                   <tr>
               <td>
                   &nbsp;</td>
               </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbl_response4" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="gridview_past" runat="server" AutoGenerateColumns="False" CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="history" HeaderText="Particulars" />
                                                                            <asp:BoundField DataField="value" HeaderText="Yes/No" />
                                                                            <asp:BoundField DataField="description" HeaderText="Description" />
                                                                        </Columns>
                                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                        <RowStyle ForeColor="#000066" />
                                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                   <tr><td class="auto-style1">&nbsp;</td></tr>
                                                            <tr>
                                                                <td class="auto-style1">Have you ever had any of the following?</td>
                                                            </tr>
                   <tr><td>&#160;</td></tr>
               <tr><td class="auto-style1">Have you had a medical problem and/or surgery related to:</td></tr>
               <tr><td>&#160;</td></tr>
               <tr><td>
                   <asp:GridView ID="gridview_surgery" runat="server" AutoGenerateColumns="False" 
                       CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                       <FooterStyle BackColor="White" ForeColor="#000066" />
                       <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                       <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                       <RowStyle ForeColor="#000066" />
                       <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                       <sortedascendingcellstyle backcolor="#F1F1F1" />
                       <sortedascendingheaderstyle backcolor="#007DBB" />
                       <sorteddescendingcellstyle backcolor="#CAC9C9" />
                       <sorteddescendingheaderstyle backcolor="#00547E" />
                       <Columns>
                           <asp:BoundField DataField="MedicalProblem" HeaderText="Particulars" />
                           <asp:BoundField DataField="value" HeaderText="Surgery/Yes/No" />
                           <asp:BoundField DataField="describe" HeaderText="Description" />
               </Columns>
               </asp:GridView></td></tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_prehis" runat="server">
                                                        <table>
    <tr>
        <td>
            &nbsp;</td>

    </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbl_response5" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
    <tr><td>
<asp:GridView ID="GridView_Social" runat="server" AutoGenerateColumns="False" CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
    <FooterStyle BackColor="White" ForeColor="#000066" />
    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
    <RowStyle ForeColor="#000066" />
    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
    <sortedascendingcellstyle backcolor="#F1F1F1" />
    <sortedascendingheaderstyle backcolor="#007DBB" />
    <sorteddescendingcellstyle backcolor="#CAC9C9" />
    <sorteddescendingheaderstyle backcolor="#00547E" />
    <Columns>
        <asp:BoundField DataField="Substance" HeaderText="Particulars" />
        <asp:BoundField DataField="currentusage" HeaderText="Currently Using? Yes/No" />
        <asp:BoundField DataField="previoususage" HeaderText="Previously Used? Yes/No" />
        <asp:BoundField DataField="frequency" HeaderText="Frequency/Type/Amount" />
        <asp:BoundField DataField="howlong" HeaderText="How Long?" />
        <asp:BoundField DataField="stoppedwhen" HeaderText="If stopped, When?" />
               </Columns>
               </asp:GridView>
                                                                                                                                                                   
    </td>

    </tr>

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>

</table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_allergies" runat="server">
                                                         <table>
                       <tr>
                           <td>&nbsp;</td>

                       </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td></td>
                                                             </tr>
                       <tr>
                           <td>
                               <asp:Label ID="lbl_response6" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                           </td>

                       </tr>
                       <tr>
                           <td>
                               <asp:GridView ID="GridView_Allergy" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3"
                                    Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                   <FooterStyle BackColor="White" ForeColor="#000066" />
                                   <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                   <PagerSettings LastPageText="Last" />
                                   <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                   <RowStyle ForeColor="#000066" />
                                   <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                   <sortedascendingcellstyle backcolor="#F1F1F1" />
                                   <sortedascendingheaderstyle backcolor="#007DBB" />
                                   <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                   <sorteddescendingheaderstyle backcolor="#00547E" />
                                   <Columns>
                                       <asp:BoundField DataField="allergent" HeaderText="Particulars" />
                                       <asp:BoundField DataField="value" HeaderText="Yes/No" />
                                       <asp:BoundField DataField="describe" HeaderText="Describe" />
               </Columns>
               </asp:GridView>

                           </td>

                       </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                       <tr>
                           <td></td>

                       </tr>
                       </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_vaccinations" runat="server">
                                                        <table>
    <tr>
        <td>
            &nbsp;</td>

    </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbl_response7" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
    <tr>
        <td class="auto-style1">Have you take immunizations for:</td>

    </tr>
    <tr>
        <td></td>

    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView_Vaccination" runat="server" AutoGenerateColumns="False"
                 CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <sortedascendingcellstyle backcolor="#F1F1F1" />
                <sortedascendingheaderstyle backcolor="#007DBB" />
                <sorteddescendingcellstyle backcolor="#CAC9C9" />
                <sorteddescendingheaderstyle backcolor="#00547E" />
                <Columns>
                    <asp:BoundField DataField="Immunization" HeaderText="Particulars" />
                    <asp:BoundField DataField="Value" HeaderText="Taken? Yes/No" />
                    <asp:BoundField DataField="Year" HeaderText="If taken, Year" />
               </Columns>
               </asp:GridView>
               </td>
               </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                  </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_takefam" runat="server">
                                                            <table>
                       <tr>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style1">&nbsp;</td>
                           <td class="auto-style1">&nbsp;</td>

                       </tr>
                                                                <tr>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="auto-style1">Illnesses/conditions which you know have occurred in your blood relatives.</td>
                                                                    <td class="auto-style1"></td>
                                                                    <td class="auto-style1">&nbsp;</td>
                                                                </tr>
                       <tr>
                           <td></td>
                           <td></td>
                           <td>&nbsp;</td>

                       </tr>
                       <tr>
                           <td class="auto-style1">
                               <asp:Label ID="lbfamily1" runat="server" Text="Cancer (describe the type of cancer for each person)" style="font-size: small"></asp:Label>

                           </td>
                           <td class="auto-style6">
 <asp:CheckBoxList ID="chkfam" runat="server" RepeatDirection="Horizontal" CssClass="twitter" >
<asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td class="auto-style6"><asp:TextBox ID="txtfam1" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td>
  <asp:Label ID="lbfamily2" runat="server" Text="Heart Disease" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam0" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam2" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td><asp:Label ID="lbfamily3" runat="server" Text="Diabetes" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam1" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem Value="None">None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam3" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr>
                                                                <td class="auto-style1">
               <asp:Label ID="lbfamily4" runat="server" Text="Stroke/TIA" CssClass="auto-style43"></asp:Label></td><td class="auto-style1"><asp:CheckBoxList ID="chkfam2" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td class="auto-style1"><asp:TextBox ID="txtfam4" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td><asp:Label ID="lbfamily5" runat="server" Text="High Blood Pressure" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam3" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam5" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td>
               <asp:Label ID="lbfamily6" runat="server" Text="High Cholesterol or Triglycerides" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam4" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam6" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td><asp:Label ID="lbfamily7" runat="server" Text="Liver Disease" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam5" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam7" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td>
               <asp:Label ID="lbfamily8" runat="server" Text="Alcohol or Drug Abuse" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam6" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam8" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td><asp:Label ID="lbfamily9" runat="server" Text="Anxiety, Depression or Psychiatric Illness" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam7" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam9" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td>
               <asp:Label ID="lbfamily10" runat="server" Text="Tuberculosis" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam8" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam10" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td><asp:Label ID="lbfamily11" runat="server" Text="Anesthesia Complications" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam9" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam11" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td>
               <asp:Label ID="lbfamily12" runat="server" Text="Genetic Disorder" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="chkfam10" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Grandparents</asp:ListItem><asp:ListItem>Father</asp:ListItem><asp:ListItem>Mother</asp:ListItem><asp:ListItem>Brothers</asp:ListItem><asp:ListItem>Sisters</asp:ListItem><asp:ListItem>Sons</asp:ListItem><asp:ListItem>Daughter</asp:ListItem><asp:ListItem>None</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="txtfam12" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td class="auto-style7"><asp:Button ID="btn_addfam" runat="server" OnClick="btn_addfam_Click" Text="Add" CssClass="btn" /></td><td class="auto-style7"></td><td class="auto-style7">&nbsp;</td></tr>
                                                                <tr>
                                                                    <td class="auto-style7">&nbsp;</td>
                                                                    <td class="auto-style7">&nbsp;</td>
                                                                    <td class="auto-style7">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="auto-style7">&nbsp;</td>
                                                                    <td class="auto-style7">&nbsp;</td>
                                                                    <td class="auto-style7">&nbsp;</td>
                                                                </tr>
                                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>

              
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_takepast" runat="server">
                                                        <table><tr><td>
                                                            &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label1" runat="server" Text="Have you ever been hospitalized?"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" CssClass="twitter" Width="56px">
                                                                        <asp:ListItem Selected="True">No</asp:ListItem>
                                                                        <asp:ListItem>Yes</asp:ListItem>
                                                                    </asp:CheckBoxList>
                                                                </td>
                                                                <td><span class="auto-style1">Describe</span>:<asp:TextBox ID="textBox1" runat="server" CssClass="twitter" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr><td><asp:Label ID="Label2" runat="server" Text="Have you had any serious injuries and/or broken bones?" ></asp:Label></td><td><asp:CheckBoxList ID="CheckBoxList2" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><span class="auto-style1">Describe</span>:<asp:TextBox ID="textBox2" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style5">
           <asp:Label ID="Label3" runat="server" Text="Have you ever received a blood transfusion?" ></asp:Label></td><td class="auto-style5"><asp:CheckBoxList ID="CheckBoxList3" runat="server" Width="56px" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td class="auto-style5"><span class="auto-style2"><span class="auto-style1">Describe</span>:</span><asp:TextBox ID="textBox3" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr class="auto-style43"><td>Have you ever had any of the following?</td><td></td><td>Describe Details:</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label4" runat="server" Text="Abnormal chest x-ray" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList4" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td>
                                                            <asp:TextBox ID="textBox4" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label5" runat="server" Text="Anesthesia complications" ></asp:Label>&#160;&#160;</td><td><asp:CheckBoxList ID="CheckBoxList5" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox5" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label6" runat="server" Text="Anxiety, depression or mental illness" CssClass="auto-style43"></asp:Label>&#160;&#160;</td><td><asp:CheckBoxList ID="CheckBoxList6" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox6" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label7" runat="server" Text="Blood problems (abnormal bleeding, anemia, high or low white count)" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList7" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox7" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label8" runat="server" Text="Diabetes&nbsp;" CssClass="auto-style43"></asp:Label></td><td><asp:CheckBoxList ID="CheckBoxList8" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox8" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label9" runat="server" Text="Growth removed from the colon or rectum (polyp or tumor)" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList9" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox9" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label10" runat="server" Text="High blood pressure" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList10" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox10" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label11" runat="server" Text="High cholesterol or triglycerides" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList11" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox11" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label12" runat="server" Text="Stroke or TIA" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList12" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox12" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label13" runat="server" Text="Treatment for alcohol and/or drug abuse" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList13" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox13" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label14" runat="server" Text="Tuberculosis or positive tuberculin skin test" CssClass="auto-style43"></asp:Label>&#160;</td><td><asp:CheckBoxList ID="CheckBoxList14" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox14" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="Label15" runat="server" Text="Cosmetic or plastic surgery" CssClass="auto-style43"></asp:Label></td><td>
           <asp:CheckBoxList ID="CheckBoxList15" runat="server" Width="56px" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:CheckBoxList></td><td><asp:TextBox ID="textBox15" runat="server" TextMode="MultiLine" CssClass="twitter"></asp:TextBox></td></tr><tr class="auto-style43"><td class="auto-style45">Have you had a medical problem and/or surgery related to:</td><td class="auto-style45"></td><td class="auto-style45">Describe:(If surgery, specify Year of surgery)</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="lbl1" runat="server" Text="Eyes (cataracts, glaucoma)"></asp:Label></td><td><asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txt1" runat="server" TextMode="MultiLine" Height="25px" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl2" runat="server" Text="Ears, nose, sinuses, or tonsils" ></asp:Label></td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList2" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td ><asp:TextBox ID="txt2" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl3" runat="server" Text="Thyroid or parathyroid glands" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList3" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt3" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl4" runat="server" Text="Heart valves or abnormal heart rhythm" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList4" runat="server" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt4" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl5" runat="server" Text="Coronary (heart) arteries (angina)" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList5" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt5" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl6" runat="server" Text="Arteries (aorta, arteries to head, arms, legs)" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList6" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt6" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl7" runat="server" Text="Veins or blood clots in the veins" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList7" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt7" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl8" runat="server" Text="Lungs" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList8" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt8" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl9" runat="server" Text="Esophagus or stomach (ulcer)" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList9" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt9" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl10" runat="server" Text="Bowel (small &amp; large intestine)" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList10" runat="server" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt10" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl11" runat="server" Text="Appendix" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList11" runat="server" CssClass="twitter"><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt11" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl12" runat="server" Text="Liver or gallbladder (including hepatitis)" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList12" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt12" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl13" runat="server" Text="Hernia" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList13" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt13" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl14" runat="server" Text="Kidneys or bladder" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList14" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt14" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl15" runat="server" Text="Bones, joints or muscles" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList15" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt15" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl16" runat="server" Text="Back, neck or spine" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList16" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt16" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl17" runat="server" Text="Brain" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList17" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6">
                                                            <asp:TextBox ID="txt17" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl18" runat="server" Text="Skin" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList18" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt18" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl19" runat="server" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList19" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt19" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td class="auto-style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl20" runat="server" ></asp:Label>&#160;</td><td class="auto-style6"><asp:RadioButtonList ID="RadioButtonList20" runat="server" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem>Surgery</asp:ListItem></asp:RadioButtonList></td><td class="auto-style6"><asp:TextBox ID="txt20" runat="server" Height="25px" TextMode="MultiLine" Width="218px" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><asp:Button ID="btn_past" runat="server" OnClick="btn_past_Click" Text="Add" CssClass="btn" /></td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
               
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_takepre" runat="server">
                                                         <table><tr><td class="auto-style41">&nbsp;</td><td class="auto-style1">&nbsp;</td><td class="auto-style1">&nbsp;</td><td class="auto-style1">&nbsp;</td><td class="auto-style1">&nbsp;</td>
           <td class="auto-style1">&nbsp;</td></tr>
                                                             <tr>
                                                                 <td class="auto-style41">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td class="auto-style41">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                                 <td class="auto-style1">&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td class="auto-style41"><span class="auto-style33">Substance</span></td>
                                                                 <td class="auto-style1">Previous Usage?</span></td>
                                                                 <td class="auto-style1">Current Usage?</td>
                                                                 <td class="auto-style1">Frequency/Type/Amount</td>
                                                                 <td class="auto-style1">How Long?(years)</td>
                                                                 <td class="auto-style1">If Stopped, When?</td>
                                                             </tr>
                                                             <tr><td></td><td class="auto-style8"></td><td class="auto-style9"></td><td class="auto-style10"></td><td class="auto-style11"></td><td></td></tr><tr><td><asp:Label ID="lblsub1" runat="server" Text="Caffeine: coffee, tea, soda" ></asp:Label></td><td class="auto-style8"><asp:RadioButtonList ID="rbl_pre_sub1" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style9"><asp:RadioButtonList ID="rbl_cur_sub1" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style10"><asp:TextBox ID="txt_freq_sub1" runat="server" CssClass="twitter"></asp:TextBox></td><td class="auto-style11"><asp:TextBox ID="txt_long_sub1" runat="server" CssClass="twitter"></asp:TextBox></td><td>
                                                             <asp:TextBox ID="txt_stop_sub1" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="lblsub2" runat="server" Text="Tobacco" ></asp:Label></td><td class="auto-style8"><asp:RadioButtonList ID="rbl_pre_sub2" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style9"><asp:RadioButtonList ID="rbl_cur_sub2" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style10"><asp:TextBox ID="txt_freq_sub2" runat="server" CssClass="twitter"></asp:TextBox></td><td class="auto-style11"><asp:TextBox ID="txt_long_sub2" runat="server" CssClass="twitter"></asp:TextBox></td><td><asp:TextBox ID="txt_stop_sub2" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="lblsub3" runat="server" Text="Alcohol &#8211; beer, wine, liquor" ></asp:Label></td><td class="auto-style8">
           <asp:RadioButtonList ID="rbl_pre_sub3" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style9"><asp:RadioButtonList ID="rbl_cur_sub3" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style10"><asp:TextBox ID="txt_freq_sub3" runat="server" CssClass="twitter"></asp:TextBox></td><td class="auto-style11"><asp:TextBox ID="txt_long_sub3" runat="server" CssClass="twitter"></asp:TextBox></td><td><asp:TextBox ID="txt_stop_sub3" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="lblsub4" runat="server" Text="Recreational/Street drugs" ></asp:Label></td><td class="auto-style8"><asp:RadioButtonList ID="rbl_pre_sub4" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style9">
           <asp:RadioButtonList ID="rbl_cur_sub4" runat="server" RepeatDirection="Horizontal" CssClass="twitter"><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style10"><asp:TextBox ID="txt_freq_sub4" runat="server" CssClass="twitter"></asp:TextBox></td><td class="auto-style11"><asp:TextBox ID="txt_long_sub4" runat="server" CssClass="twitter"></asp:TextBox></td><td><asp:TextBox ID="txt_stop_sub4" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;</td><td class="auto-style8">&nbsp;</td><td class="auto-style9">&nbsp;</td><td class="auto-style10">&nbsp;</td><td class="auto-style11">&nbsp;</td><td>&nbsp;</td></tr><tr><td><asp:Button ID="btn_add_sub" runat="server" OnClick="btn_add_sub_Click" Text="Add" CssClass="btn" /></td><td class="auto-style8">&nbsp;</td><td class="auto-style9">&nbsp;</td><td class="auto-style10">&nbsp;</td><td class="auto-style11">&nbsp;</td><td>&nbsp;</td></tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                                 <td class="auto-style8">&nbsp;</td>
                                                                 <td class="auto-style9">&nbsp;</td>
                                                                 <td class="auto-style10">&nbsp;</td>
                                                                 <td class="auto-style11">&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                                 <td class="auto-style8">&nbsp;</td>
                                                                 <td class="auto-style9">&nbsp;</td>
                                                                 <td class="auto-style10">&nbsp;</td>
                                                                 <td class="auto-style11">&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr><td>&nbsp;</td><td class="auto-style8">&nbsp;</td><td class="auto-style9">&nbsp;</td><td class="auto-style10">&nbsp;</td><td class="auto-style11">&nbsp;</td><td>&nbsp;</td></tr>

               </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_takevacc" runat="server">
                                                        <table>
           <tr class="auto-style33"><td class="auto-style1">&nbsp;</td><td>&#160;</td><td>&#160;</td></tr>
                                                            <tr class="auto-style33">
                                                                <td class="auto-style1">&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr class="auto-style33">
                                                                <td class="auto-style1">&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr class="auto-style33">
                                                                <td class="auto-style1">Have you had these Vaccinations?</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr class="auto-style33"><td>&nbsp;</td><td>&nbsp;</td><td class="auto-style1">If Yes, Year when Vaccinated</td></tr><tr><td><asp:Label ID="labvac1" runat="server" Text="BCG (Bacillus Calmette Guerin) Vaccine" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac1" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac1" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac2" runat="server" Text="DPT Vaccine"></asp:Label></td><td><asp:RadioButtonList ID="rblvac2" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td>
                                                            <asp:TextBox ID="txtvac2" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="labvac3" runat="server" Text="OPV (Oral Polio) Vaccine" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac3" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac3" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac4" runat="server" Text="MMR (Measles Mumps Rubella) Vaccine " ></asp:Label></td><td><asp:RadioButtonList ID="rblvac4" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac4" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac5" runat="server" Text="Hepatitis B Vaccine " ></asp:Label></td><td>
           <asp:RadioButtonList ID="rblvac5" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac5" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac6" runat="server" Text="Tetanus Toxoid (TT)" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac6" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac6" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac7" runat="server" Text="Chicken Pox (Varicella) vaccine" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac7" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td>
                                                            <asp:TextBox ID="txtvac7" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="labvac8" runat="server" Text="HPV (Human Papilloma Virus)" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac8" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac8" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac9" runat="server" Text="HiB (Haemophilus Influenzae Type B) " ></asp:Label></td><td><asp:RadioButtonList ID="rblvac9" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac9" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac10" runat="server" Text="Pneumococcal" ></asp:Label></td><td>
           <asp:RadioButtonList ID="rblvac10" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac10" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac11" runat="server" Text="IPV (Inactivated Polio Vaccine)" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac11" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac11" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac12" runat="server" Text="Rotavirus" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac12" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td>
                                                            <asp:TextBox ID="txtvac12" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="labvac13" runat="server" Text="Influenza" ></asp:Label></td>
               <td><asp:RadioButtonList ID="rblvac13" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac13" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac14" runat="server" Text="Pneumococcal Booster" ></asp:Label></td><td><asp:RadioButtonList ID="rblvac14" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac14" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="labvac15" runat="server" Text="Hepatitis A" ></asp:Label></td><td>
           <asp:RadioButtonList ID="rblvac15" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem>Unknown</asp:ListItem><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txtvac15" runat="server" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><asp:Button ID="btn_addvac" runat="server" OnClick="btn_addvac_Click" Text="Add" CssClass="btn" /></td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_takealler" runat="server">
                                                                        <table>
           <tr class="auto-style33"><td class="auto-style41">&nbsp;</td>
               <td>&nbsp;</td><td class="auto-style42">&nbsp;</td></tr>
                                                                            <tr class="auto-style33">
                                                                                <td class="auto-style41">&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td class="auto-style42">&nbsp;</td>
                                                                            </tr>
                                                                            <tr class="auto-style33">
                                                                                <td class="auto-style41">&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td class="auto-style42">&nbsp;</td>
                                                                            </tr>
                                                                            <tr class="auto-style33">
                                                                                <td class="auto-style41">Allergent</td>
                                                                                <td></td>
                                                                                <td class="auto-style42">Describe Allergic Reaction and name of allergent where applicable </td>
                                                                            </tr>
                                                                            <tr><td>&nbsp;</td><td>&nbsp;</td><td class="auto-style13">&nbsp;</td></tr><tr><td><asp:Label ID="lblallergy1" runat="server" Text="Iodine or X-ray contrast dye" ></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent1" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent1" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="lblallergy2" runat="server" Text="Latex or Rubber" ></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent2" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13">
                                                                            <asp:TextBox ID="txt_allergent2" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="lblallergy3" runat="server" Text="Bee or wasp stings" ></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent3" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent3" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="lblallergy4" runat="server" Text="Adhesive tape" ></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent4" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent4" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="lblallergy5" runat="server" Text="Sunlight" ></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent5" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent5" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="lblallergy6" runat="server" Text="Dust"></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent6" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent6" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td>
           <asp:Label ID="lblallergy7" runat="server" Text="Medical Allergents" ></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent7" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent7" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td><asp:Label ID="lblallergy8" runat="server" Text="Food Allergents"></asp:Label></td><td><asp:RadioButtonList ID="rbl_allergent8" runat="server" RepeatDirection="Horizontal" CssClass="twitter" ><asp:ListItem Selected="True">No</asp:ListItem><asp:ListItem>Yes</asp:ListItem></asp:RadioButtonList></td><td class="auto-style13"><asp:TextBox ID="txt_allergent8" runat="server" Height="28px" TextMode="MultiLine" Width="358px" CssClass="twitter"></asp:TextBox></td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td class="auto-style13">&nbsp;</td></tr><tr><td><asp:Button ID="btn_add_allergent" CssClass="btn" runat="server" OnClick="btn_add_allergent_Click" Text="Add" /></td><td>&nbsp;</td><td class="auto-style13">&nbsp;</td></tr>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td class="auto-style13">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td class="auto-style13">&nbsp;</td>
                                                                            </tr>
                                                                            <tr><td>&nbsp;</td><td>&nbsp;</td><td class="auto-style13">&nbsp;</td></tr></table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                <asp:View ID="view_referrals" runat="server">
                                                        <table>
               <tr>
                   <td class="auto-style1" colspan="2">&nbsp;</td>
               </tr>
                                                            <tr>
                                                                <td class="auto-style1" colspan="2">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style1" colspan="2">&nbsp;</td>
                                                            </tr>
               <tr>
                   <td colspan="2" class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Referrals to Other Departments</td>
               </tr>
               <tr>
                   <td class="auto-style17">&nbsp;</td>
                   <td>&nbsp;</td>
               </tr>
               <tr>
                   <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refer To</td>
                   <td class="auto-style1"></td>
               </tr>
                                                            <tr>
                                                                <td class="auto-style1">&nbsp;</td>
                                                                <td class="auto-style1">&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style18">&nbsp;</td>
                   <td class="auto-style1">
                       &nbsp;</td>
               </tr>
                                                            <tr>
                                                                <td class="auto-style18"><span class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Department</span>:</td>
                                                                <td class="auto-style1">
                                                                    <asp:DropDownList ID="ddl_depts" runat="server" AutoPostBack="True" CssClass="twitter" OnSelectedIndexChanged="ddl_depts_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style18">&nbsp;</td>
                                                                <td class="auto-style1">&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Doctor:</td>
                   <td class="auto-style1">
                       <asp:DropDownList ID="ddl_docs" CssClass="twitter" runat="server" AutoPostBack="True">
                       </asp:DropDownList>
                   </td>
               </tr>
                                                            <tr>
                                                                <td class="auto-style1">&nbsp;</td>
                                                                <td class="auto-style1">&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remarks:</td>
                   <td class="auto-style16">
                       <asp:TextBox ID="txt_remarkref" CssClass="twitter" runat="server" Height="47px" TextMode="MultiLine" Width="427px"></asp:TextBox>
                   </td>
               </tr>
                                                            <tr>
                                                                <td class="auto-style1">&nbsp;</td>
                                                                <td class="auto-style16">&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style17">
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <asp:Button ID="btn_addref" runat="server" CssClass="btn" OnClick="btn_addref_Click" Text="Add" />
                   </td>
                   <td>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <asp:Button ID="btn_refreshref" CssClass="btn" runat="server" OnClick="btn_refreshref_Click" Text="Refresh" />
                   </td>
               </tr>
                                                            <tr>
                                                                <td class="auto-style17">&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style17">&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
               <tr>
                   <td class="auto-style17">&nbsp;</td>
                   <td>&nbsp;</td>
               </tr>
              
                                                        </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                      <tr><td>
                        <asp:View ID="View_casdiag" runat="server">
                            <table class="auto-style46">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_casdiag" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridView_casdiag" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_casdiag_PageIndexChanging" Width="1000px">
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <sortedascendingcellstyle backcolor="#F1F1F1" />
                                            <sortedascendingheaderstyle backcolor="#007DBB" />
                                            <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                            <sorteddescendingheaderstyle backcolor="#00547E" />
                                            <Columns>
                                                <asp:BoundField DataField="symptoms" HeaderText="Symptoms" />
                                                <asp:BoundField DataField="prodiagnosis" HeaderText="Diagnosis" />
                                                <asp:BoundField DataField="empname" HeaderText="Diagnosed By" />
                                                <asp:BoundField DataField="deptname" HeaderText="Department" />
                                                <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                                <asp:BoundField DataField="dateofdiag" HeaderText="Date Of Diagnosis" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                    </td></tr>
                    <tr><td>
                        <asp:View ID="View_caspres" runat="server">
                            <table class="auto-style46">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_caspres" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridView_caspre" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="1000px" OnPageIndexChanging="GridView_caspre_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="medname" HeaderText="Medicine Name">
                                                <ItemStyle Width="20%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="potency" HeaderText="Potency">
                                                <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="dosage" HeaderText="Dosage">
                                                <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="frequency" HeaderText="Frequency">
                                                <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="no_of_days" HeaderText="No of Days">
                                                <ItemStyle Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="empname" HeaderText="Prescribed By">
                                                <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="remarks" HeaderText="Remarks">
                                                <ItemStyle Width="30%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="dateofpres" HeaderText="Date Of Prescription">
                                                <ItemStyle Width="10%" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                            <PagerSettings LastPageText="Last" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <sortedascendingcellstyle backcolor="#F1F1F1" />
                                            <sortedascendingheaderstyle backcolor="#007DBB" />
                                            <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                            <sorteddescendingheaderstyle backcolor="#00547E" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        </td></tr>
                                        <tr>
                                            <td>
                                                <asp:View ID="View_caslab" runat="server">
                                                    <table class="auto-style46">
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbl_caslab" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridView_CasLab_Result" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_CasLab_Result_PageIndexChanging" Width="1000px">
                                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                    <RowStyle ForeColor="#000066" />
                                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                    <sortedascendingcellstyle backcolor="#F1F1F1" />
                                                                    <sortedascendingheaderstyle backcolor="#007DBB" />
                                                                    <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                                                    <sorteddescendingheaderstyle backcolor="#00547E" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="testname" HeaderText="Test" />
                                                                        <asp:BoundField DataField="result" HeaderText="Result" />
                                                                        <asp:BoundField DataField="dateofres" HeaderText="Date Of Test" />
                                                                        <asp:BoundField DataField="empname" HeaderText="Requested By" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:View ID="View_casinj" runat="server">
                                <table class="auto-style46">
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_casinj" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GridView_CasInjection" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" OnPageIndexChanging="GridView_CasInjection_PageIndexChanging" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                <Columns>
                                                    <asp:BoundField DataField="injectionname" HeaderText="Medicine Name">
                                                    <ItemStyle Width="10%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="type" HeaderText="Type">
                                                    <ItemStyle Width="10%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dosage" HeaderText="Dosage">
                                                    <ItemStyle Width="5%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="frequency" HeaderText="Frequency">
                                                    <ItemStyle Width="5%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="empname" HeaderText="Requested By">
                                                    <ItemStyle Width="25%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="remarks" HeaderText="Remarks">
                                                    <ItemStyle Width="35%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dateofinj" HeaderText="Date Of Injection">
                                                    <ItemStyle Width="10%" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </asp:View>
                        </td>
                    </tr>
                                        <tr>
                                            <td>
                                                <asp:View ID="View_casvit" runat="server">
                                                    <table class="auto-style46">
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbl_casvit" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridView_Casvitals" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" OnPageIndexChanging="GridView_Casvitals_PageIndexChanging" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                    <RowStyle ForeColor="#000066" />
                                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="vital" HeaderText="Particulars " />
                                                                        <asp:BoundField DataField="value" HeaderText="Value" />
                                                                        <asp:BoundField DataField="normal" HeaderText="Normal Value" />
                                                                        <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                                                        <asp:BoundField DataField="dateofvital" HeaderText="Date" />
                                                                        <asp:BoundField DataField="empname" HeaderText="Checked By" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </td>
                    </tr>
                  


<tr>
                                                <td>
                                                    <asp:View ID="view_opprediag" runat="server">
                                                        <table>
                       <tr>
                           <td>
                               &nbsp;</td>

                       </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbl_response26" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
                       <tr>
                           <td class="auto-style2">
                               <asp:GridView ID="GridView_opPre_Diag" runat="server" AllowPaging="True" 
                                   AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC"
                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_opPre_Diag_PageIndexChanging" Width="1000px">
                                   <FooterStyle BackColor="White" ForeColor="#000066" />
                                   <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                   <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                   <RowStyle ForeColor="#000066" />
                                   <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                   <sortedascendingcellstyle backcolor="#F1F1F1" />
                                   <sortedascendingheaderstyle backcolor="#007DBB" />
                                   <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                   <sorteddescendingheaderstyle backcolor="#00547E" />
                                   <Columns>
                                       <asp:BoundField DataField="symptoms" HeaderText="Symptoms" />
                                       <asp:BoundField DataField="prodiagnosis" HeaderText="Diagnosis" />
                                       <asp:BoundField DataField="empname" HeaderText="Diagnosed By" />
                                       <asp:BoundField DataField="deptname" HeaderText="Department" />
                                       <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                       <asp:BoundField DataField="dateofdiag" HeaderText="Date Of Diagnosis" />
               </Columns>
               </asp:GridView>

                           </td>

                       </tr>
                                                            <tr>
                                                                <td class="auto-style2">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style2">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style2">&nbsp;</td>
                                                            </tr>
                       <tr><td></td></tr>

                   </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style20">
                                                    <asp:View ID="view_opprepres" runat="server">
                                                        <table>
                   <tr>
                       <td class="auto-style28">
                           &nbsp;</td>
                   </tr>
                                                            <tr>
                                                                <td class="auto-style28">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style28">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style28">
                                                                    <asp:Label ID="lbl_response24" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                </td>
                                                            </tr>
                   <tr>
                       <td class="auto-style14">
                           <asp:GridView ID="GridView_opPre_Pres" runat="server" AllowPaging="True" 
                               AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" 
                               BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                Width="1000px" OnPageIndexChanging="GridView_opPre_Pres_PageIndexChanging1" >
                               <Columns>
                                   <asp:BoundField DataField="medname" HeaderText="Medicine Name" >
                                   <ItemStyle Width="20%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="potency" HeaderText="Potency" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="dosage" HeaderText="Dosage" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="frequency" HeaderText="Frequency" >
                                   <ItemStyle Width="5%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="no_of_days" HeaderText="No of Days" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="empname" HeaderText="Prescribed By" >
                                   <ItemStyle Width="15%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="remarks" HeaderText="Remarks" >
                                   <ItemStyle Width="30%" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="dateofpres" HeaderText="Date Of Prescription" >
                                   <ItemStyle Width="10%" />
                                   </asp:BoundField>
                               </Columns>
                               <FooterStyle BackColor="White" ForeColor="#000066" />
                               <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                               <PagerSettings LastPageText="Last" />
                               <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                               <RowStyle ForeColor="#000066" />
                               <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                               <sortedascendingcellstyle backcolor="#F1F1F1" />
                               <sortedascendingheaderstyle backcolor="#007DBB" />
                               <sorteddescendingcellstyle backcolor="#CAC9C9" />
                               <sorteddescendingheaderstyle backcolor="#00547E" />
                           </asp:GridView>
                       </td>
                   </tr>
                   <tr>
                       <td class="auto-style23"></td>
                   </tr>
                                                            <tr>
                                                                <td class="auto-style23">&nbsp;</td>
                                                            </tr>
                   <tr>
                       <td class="auto-style26"></td>
                   </tr>
               </table>
           
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_oplabres" runat="server">
                                                        <table>
                       <tr>
                           <td>
                               <asp:Label ID="lbl_response25" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                           </td>

                       </tr>
                       <tr>
                           <td>
                               &nbsp;</td>

                       </tr>
                      

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="GridView_opLab_Result" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_opLab_Result_PageIndexChanging" Width="1000px">
                                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                        <RowStyle ForeColor="#000066" />
                                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                        <sortedascendingcellstyle backcolor="#F1F1F1" />
                                                                        <sortedascendingheaderstyle backcolor="#007DBB" />
                                                                        <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                                                        <sorteddescendingheaderstyle backcolor="#00547E" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="testname" HeaderText="Test" />
                                                                            <asp:BoundField DataField="result" HeaderText="Result" />
                                                                            <asp:BoundField DataField="dateofres" HeaderText="Date Of Test" />
                                                                            <asp:BoundField DataField="empname" HeaderText="Requested By" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                      

                   </table>
                                                    </asp:View>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:View ID="view_opvitals" runat="server">
                                                         <table>
                       <tr><td>
                           &nbsp;</td></tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>
                                                                     <asp:Label ID="lbl_response23" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found"></asp:Label>
                                                                 </td>
                                                             </tr>
                         <tr><td>
                             <asp:GridView ID="GridView_opvitals" AutoGenerateColumns="False" runat="server" CellPadding="3" AllowPaging="True" OnPageIndexChanging="GridView_opvitals_PageIndexChanging" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" >
                                 <FooterStyle BackColor="White" ForeColor="#000066" />
                                 <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                 <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                 <RowStyle ForeColor="#000066" />
                                 <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                 <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                 <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                 <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                 <SortedDescendingHeaderStyle BackColor="#00547E" />
                             <Columns><asp:BoundField DataField="vital" HeaderText="Particulars "/>
                                 <asp:BoundField DataField="value" HeaderText="Value" />
                                 <asp:BoundField DataField="normal" HeaderText="Normal Value" />
                                 <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                 <asp:BoundField DataField="dateofvital" HeaderText="Date" />
                                 <asp:BoundField DataField="empname" HeaderText="Checked By" />
                                
                               </Columns>
                             </asp:GridView>
                             </td></tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>&nbsp;</td>
                                                             </tr>
                             <tr><td></td></tr>
                   </table>
               
                                                    </asp:View>
                                                </td>
                                            </tr>
                    <tr>
                        <td>
                            <asp:View ID="view_opinj" runat="server">
                                <table class="auto-style46">
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_response27" runat="server" Font-Size="Large" ForeColor="#FF99CC" Text="No Records Found" Visible="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GridView_opInj" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" OnPageIndexChanging="GridView_opInj_PageIndexChanging" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                <Columns>
                                                    <asp:BoundField DataField="injectionname" HeaderText="Medicine Name">
                                                    <ItemStyle Width="10%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="type" HeaderText="Type">
                                                    <ItemStyle Width="10%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dosage" HeaderText="Dosage">
                                                    <ItemStyle Width="5%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="frequency" HeaderText="Frequency">
                                                    <ItemStyle Width="5%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="empname" HeaderText="Requested By">
                                                    <ItemStyle Width="25%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="remarks" HeaderText="Remarks">
                                                    <ItemStyle Width="35%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dateofinj" HeaderText="Date Of Injection">
                                                    <ItemStyle Width="10%" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    </table></asp:View>
                                </td></tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td>
                        <asp:View ID="View_RBS" runat="server"><table>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>
                                <asp:Label ID="lbl_resp30" runat="server" Text="No Records Found" Font-Size="Large" ForeColor="#FF0066"></asp:Label></td></tr>
                            <tr>
                            <td>

                                <asp:GridView ID="GridView_RBS" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_RBS_PageIndexChanging" Width="800px">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <Columns>
                                        <asp:BoundField DataField="value" HeaderStyle-HorizontalAlign="Center" HeaderText="Value" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="remarks" HeaderStyle-HorizontalAlign="Center" HeaderText="Remarks" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="date" HeaderStyle-HorizontalAlign="Center" HeaderText="Date" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="empname" HeaderStyle-HorizontalAlign="Center" HeaderText="Taken By" ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                </asp:GridView>

                            </td>
                                                                      </tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table></asp:View>
                    </td></tr>
                    <tr><td>
                        <asp:View ID="view_opin" runat="server">
                            <table class="nav-justified">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Enter Injection Name</td>
                                    <td>
                                        <asp:DropDownList ID="ddl_injname" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Enter Dosage</td>
                                    <td>
                                        <asp:TextBox ID="txt_injdos" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Enter Number of Doses</td>
                                    <td>
                                        <asp:TextBox ID="txt_injnodos" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Enter Instructions</td>
                                    <td>
                                        <asp:TextBox ID="txt_injins" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Button ID="btn_injopadd" runat="server" CssClass="btn" OnClick="btn_injopadd_Click" Text="Add" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="grid_inj" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="1000px" OnPageIndexChanging="grid_inj_PageIndexChanging1">
                                            <Columns>
                                                <asp:BoundField DataField="injectionname" HeaderText="Medicine Name">
                                                <ItemStyle Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="type" HeaderText="Type">
                                                <ItemStyle Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="dosage" HeaderText="Dosage">
                                                <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="frequency" HeaderText="Frequency">
                                                <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="empname" HeaderText="Requested By">
                                                <ItemStyle Width="25%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="remarks" HeaderText="Remarks">
                                                <ItemStyle Width="35%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="dateofinj" HeaderText="Date Of Injection">
                                                <ItemStyle Width="10%" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        </td></tr>
                                        </table>
                                    </asp:MultiView>
                                </td>
                            </tr>
                           
                        </table>
                    
                
    </div>  
   
                    
                </td>
                
            </tr>
        </table></center>
    </form>
</asp:Content>


