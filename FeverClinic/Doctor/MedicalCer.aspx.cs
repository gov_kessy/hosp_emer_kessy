﻿//Leave medical certificate
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class FeverClinic_Doctor_MedicalCer : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/FeverClinic/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getCon();
            SqlCommand cmd_pat = new SqlCommand("select empname from EmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);


            int k_pat = cmd_pat.ExecuteNonQuery();

            if (dt_pat.Rows.Count > 0)
            {
                DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

                string docname = Convert.ToString(row_pat[0]);
                txt_docname.Text = docname;

                c.Con.Close();
            }
        }

        if (!IsPostBack)
        {
            lbl_gen.Visible = false;
            txt_pno.Visible = false;
            lbl_pno.Visible = false;
            btn_search.Visible = false;
            binddoc();
        }
    }
    public void binddoc()
    {

        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select empname from EmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            string docname = Convert.ToString(row_pat[0]);
            txt_docname.Text = docname;

            c.Con.Close();
        }
    }

    protected void GenerateReport(object sender, EventArgs e)
    {

        c.getCon();
        SqlCommand cmdpat = new SqlCommand("select * from MedCerMaster where medsertype like '%Leave Certificate%' ", c.Con);
        SqlDataAdapter sdapat = new SqlDataAdapter(cmdpat);
        DataTable dtpat = new DataTable();
        sdapat.Fill(dtpat);

        int k = cmdpat.ExecuteNonQuery();

        if (dtpat.Rows.Count > 0)
        {
            int id = Convert.ToInt32(dtpat.Rows[0][0]);
            String s = "insert into MedicalCertificate values('" + id + "','" + txt_pno.Text + "','" + Session["uid"] + "','" + DateTime.Today + "')";
            SqlCommand cmd2 = new SqlCommand(s, c.Con);
            cmd2.ExecuteNonQuery();
        }
        c.Con.Close();
        //c.getCon();
        //SqlCommand cmdpat = new SqlCommand("select * from patientdetails where patientid= '" + ddlEmployees.SelectedItem.Value + "' ", c.Con);
        //SqlDataAdapter sdapat = new SqlDataAdapter(cmdpat);
        //DataTable dtpat = new DataTable();
        //sdapat.Fill(dtpat);

        //int k = cmdpat.ExecuteNonQuery();

        //if (dtpat.Rows.Count > 0)
        //{
        //    DataRow dr = dtpat.Rows[dtpat.Rows.Count - 1];
        //    DateTime todaydate = DateTime.Now;
        //    string pname = Convert.ToString(dr[1]);
        //    string gen = Convert.ToString(dr[14]);
        //    DateTime dob = Convert.ToDateTime(dr[7]);
        //    int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;



        Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(1);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            // table.SetWidths(new float[] { 1 });

            //Company Logo
            cell = ImageCell("~/Casuality/Doctor/images/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("\nGovernment General Hospital\n", FontFactory.GetFont("Verdana", 18, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("Changanacherry,\n", FontFactory.GetFont("Verdana", 16, Font.NORMAL, Color.BLACK)));
            phrase.Add(new Chunk("Kottayam,\n", FontFactory.GetFont("Verdana", 14, Font.NORMAL, Color.BLACK)));
            phrase.Add(new Chunk("Kerala - 686101", FontFactory.GetFont("Verdana", 14, Font.NORMAL, Color.BLACK)));
            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#000000"));
            //DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            //DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(1);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            //    table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            //Employee Details
            cell = PhraseCell(new Phrase("\n\nMEDICAL CERTIFICATE", FontFactory.GetFont("Verdana", 16, Font.UNDERLINE, Color.BLACK)), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 2;
            cell.PaddingBottom = 30f;
            table.AddCell(cell);

            //Photo
            //  cell = ImageCell(string.Format("~/photos/{0}.jpg", dr["EmployeeId"]), 25f, PdfPCell.ALIGN_CENTER);
            //  table.AddCell(cell);

            //Name



            //phrase = new Phrase();
            //phrase.Add(new Chunk(dr["pname"] + " " + dr["patientid"] + " " + dr["gender"] + "\n", FontFactory.GetFont("Arial", 10, Font.BOLD, Color.BLACK)));
            //phrase.Add(new Chunk("(" + dr["contact"].ToString() + ")", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)));
            //cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            //cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            //table.AddCell(cell);
            //document.Add(table);



            //  DrawLine(writer, 160f, 80f, 160f, 690f, Color.BLACK);
            //DrawLine(writer, 115f, document.Top - 200f, document.PageSize.Width - 100f, document.Top - 200f, Color.BLACK);

            table = new PdfPTable(1);
            // table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_MIDDLE;
            if (lbl_gen.Text == "Female")
                table.AddCell(PhraseCell(new Phrase("\n\nSignature of the Applicant …………………………………….\n\n I, Dr. " + txt_docname.Text + " after careful personal examination of the case hereby certify that " + txt_patname.Text + " whose signature is given above, is suffering from " + txt_diag.Text + " that I consider that a period of absence from duty for " + txt_days.Text + " days with effect from " + txt_datefrom.Text + " to " + txt_dateto.Text + "  is absolutely necessary for the restoration of her health.", FontFactory.GetFont("Verdana", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_MIDDLE));
            else if (lbl_gen.Text == "Male")
                table.AddCell(PhraseCell(new Phrase("\n\nSignature of the Applicant …………………………………….\n\n I, Dr. " + txt_docname.Text + " after careful personal examination of the case hereby certify that " + txt_patname.Text + " whose signature is given above, is suffering from " + txt_diag.Text + " that I consider that a period of absence from duty for " + txt_days.Text + " days with effect from " + txt_datefrom.Text + " to " + txt_dateto.Text + "  is absolutely necessary for the restoration of his health.", FontFactory.GetFont("Verdana", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_MIDDLE));

            else
                table.AddCell(PhraseCell(new Phrase("\n\nSignature of the Applicant …………………………………….\n\n I, Dr. " + txt_docname.Text + " after careful personal examination of the case hereby certify that " + txt_patname.Text + " whose signature is given above, is suffering from " + txt_diag.Text + " that I consider that a period of absence from duty for " + txt_days.Text + " days with effect from " + txt_datefrom.Text + " to " + txt_dateto.Text + "  is absolutely necessary for the restoration of his/her health.", FontFactory.GetFont("Verdana", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_MIDDLE));
            //  table.AddCell(PhraseCell(new Phrase(TextBox1.Text, FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_LEFT);
            cell.Colspan = 2;
            cell.PaddingBottom = 20f;
            table.AddCell(cell);


            //Address
            // table.AddCell(PhraseCell(new Phrase(" after careful personal examination of the case hereby certify that ", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //  phrase = new Phrase(new Chunk(dr["pname"] + "\n", FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)));
            // phrase.Add(new Chunk(dr["addline2"] + "\n", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)));
            // phrase.Add(new Chunk(dr["place"] + " " + dr["contact"] + " " + dr["pin"], FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)));
            //  table.AddCell(PhraseCell(phrase, PdfPCell.ALIGN_LEFT));
            //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            //cell.Colspan = 2;
            //cell.PaddingBottom = 10f;
            //table.AddCell(cell);

            //Date of Birth
            //table.AddCell(PhraseCell(new Phrase(" whose signature is given above, is suffering from", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table.AddCell(PhraseCell(new Phrase(TextBox2.Text, FontFactory.GetFont("Arial", 12, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            //cell.Colspan = 2;
            //cell.PaddingBottom = 10f;
            //table.AddCell(cell);

            //Phone
            //table.AddCell(PhraseCell(new Phrase("Phone Number:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table.AddCell(PhraseCell(new Phrase(dr["contact"] + " Ext: " + dr["contact"], FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            //cell.Colspan = 2;
            //cell.PaddingBottom = 10f;
            //table.AddCell(cell);

            //Addtional Information
            table.AddCell(PhraseCell(new Phrase("MEDICAL OFFICER", FontFactory.GetFont("Arial", 14, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_RIGHT));
            table.AddCell(PhraseCell(new Phrase("Station:", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table.AddCell(PhraseCell(new Phrase("Date:", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));

            document.Add(table);
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=MedicalCertificate.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
        //}
        //c.Con.Close();

    }
    private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2, Color color)
    {
        PdfContentByte contentByte = writer.DirectContent;
        contentByte.SetColorStroke(color);
        contentByte.MoveTo(x1, y1);
        contentByte.LineTo(x2, y2);
        contentByte.Stroke();
    }
    private static PdfPCell PhraseCell(Phrase phrase, int align)
    {
        PdfPCell cell = new PdfPCell(phrase);
        cell.BorderColor = Color.WHITE;
        cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 4f;
        cell.PaddingTop = 2f;
        return cell;
    }
    private static PdfPCell ImageCell(string path, float scale, int align)
    {
        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path));
        image.ScalePercent(scale);
        PdfPCell cell = new PdfPCell(image);
        cell.BorderColor = Color.WHITE;
        cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 0f;
        cell.PaddingTop = 0f;
        return cell;
    }


    protected void ddl_reg_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_reg.SelectedItem.Text == "Yes")
        {
            txt_pno.Visible = true;
            lbl_pno.Visible = true;
            btn_search.Visible = true;

        }

    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        binddoc();
        c.getCon();
        SqlCommand cmdbk = new SqlCommand("select * from patientdetails where patientid= '" + txt_pno.Text + "' ", c.Con);
        SqlDataAdapter sdabk = new SqlDataAdapter(cmdbk);
        DataTable dtbk = new DataTable();
        sdabk.Fill(dtbk);

        int k = cmdbk.ExecuteNonQuery();

        if (dtbk.Rows.Count > 0)
        {
            DataRow rowbk = dtbk.Rows[dtbk.Rows.Count - 1];
            DateTime todaydate = DateTime.Now;
            string pname = Convert.ToString(rowbk[1]);
            string gen = Convert.ToString(rowbk[14]);
            lbl_gen.Text = gen;
            txt_patname.Text = pname;
            txt_patname.Enabled = false;
        }
        else
        {
            Response.Write("<script>alert('No Patient Found With That Card Number');</script>");

        }
        c.Con.Close();
    }
}