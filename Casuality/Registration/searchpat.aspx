﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Casuality/Registration/Regmaster.master" AutoEventWireup="true" CodeFile="searchpat.aspx.cs" Inherits="Casuality_Registration_searchpat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <title></title>
   
<link href="style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Enter Patient Name:</td>
                <td>
                    <asp:TextBox ID="txt_name" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="*Enter Patient Name" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_search" runat="server" Text="Search" Width="118px" OnClick="btn_search_Click" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                   <asp:GridView ID="gridview_pat" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" runat="server" AutoGenerateColumns="False" OnRowCommand="gridview_pat_RowCommand" Width="498px" CellPadding="3" CssClass="auto-style2" DataKeyNames="patientid" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" HorizontalAlign="Center"  >  
                    <Columns>  
                        <asp:BoundField DataField="patientid" HeaderText="Patient Id" />  
                        <asp:BoundField DataField="pname" HeaderText="Patient Name" />  
                        <asp:BoundField DataField="dob" HeaderText="Date Of Birth" />  
                     <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >

                        
                             
<ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>

                        
                             
                    </Columns>  
                       <FooterStyle BackColor="White" ForeColor="#000066" />

<HeaderStyle BackColor="#006699" ForeColor="White" Font-Bold="True"></HeaderStyle>
                       <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                       <RowStyle ForeColor="#000066" />
                       <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                       <SortedAscendingCellStyle BackColor="#F1F1F1" />
                       <SortedAscendingHeaderStyle BackColor="#007DBB" />
                       <SortedDescendingCellStyle BackColor="#CAC9C9" />
                       <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>

                <td class="auto-style2">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</asp:Content>

